# Webx

Game created using cocos2d in ios

Updates are as below, created in branches as the code was developed.


WebX v1.0 smooth working acceleration and velocity


WebX v1.1 introduced static and dynamic friction

v1.1-introduced-static-and-dynamic-friction


WebX v1.2 Menus and labels created.zip

v1.2-menus-and-labels-created


WebX v1.3 Vertical and Horizontal gameplays created

v1.3-vertical-and-horizontal-gameplays-created


WebX v1.4 Forces Fuel acc and dec created

v1.4-forces-fuel-acc-and-dec-created


WebX v1.5.0 player vertical movement with max velocity

v1.5.0-player-vertical-movement-with-max-velocity


WebX v1.6.0 Enemy Weapons Ready but in linear path.zip

v1.6.0-enemy-weapons-ready-but-in-linear-path


WebX v1.6.1 Enemy Weapons linear path folowing the player

v1.6.1-enemy-weapons-linear-path-folowing-the-player


WebX v1.6.2 Enemy Weapons linear path not folowing the player

v1.6.2-enemy-weapons-linear-path-not-folowing-the-player


WebX v1.6.3 Enemy Weapons with Player Influence

v1.6.3-enemy-weapons-with-player-influence


WebX v1.6.4 Enemy Weapons with Enemy Influence

v1.6.4-enemy-weapons-with-enemy-influence


WebX v1.6.4.1 Enemy Weaponbelow screen enemy resets

v1.6.4.1-enemy-weapon-below-screen-enemy-resets


WebX v1.6.4.2 Enemy weapon throwing finalized

v1.6.4.2-enemy-weapon-throwing-finalized


WebX v1.6.5 Level Select Scene created

v1.6.5-level-select-scene-created


WebX v1.6.5.1 Flying enemies in scrolling layers

v1.6.5.1-flying-enemies-in-scrolling-layers


WebX v1.6.6 Six levels with Three diffrent enemies ready

v1.6.6-six-levels-with-three-diffrent-enemies-ready


WebX v1.6.7 Six levels with blocks ready

v1.6.7-six-levels-with-blocks-ready


WebX v1.6.8 seven levels with killing and blocking blocks ready.zip

v1.6.8-7-lvls-with-killing-and-blocking-blocks


WebX v1.6.9.1 Generalizing code blocks code messed up.zip

v1.6.9.1-generalizing-code-blocks-code-messed-up


WebX v1.6.9.2 Generalizing code blocks code minor error at block edge

v1.6.9.2-generalizing-code-blocks


WebX v1.6.9.3 Generalizing code, Rotation of collected items.zip

v1.6.9.3-rotation-of-collected-items


WebX v1.6.9.4 Generalizing code, Rotation completed

v1.6.9.4-rotation-completed


WebX v1.6.9.5 Shooting rotating enemies incomplete

v1.6.9.5-shooting-rotating-enemies-incomplete


WebX v1.6.9.6 Shooting rotating enemies complete

v1.6.9.6-shooting-rotating-enemies-complete


WebX v1.6.9.7 Shooting enemies without time slow

v1.6.9.7-shooting-enemies-without-time-slow


WebX v1.6.9.8 Shooting enemies with time slow.zip

v1.6.9.8-shooting-enemies-with-time-slow


WebX v1.6.9.9 Shooting green and blue enemies complete

v1.6.9.9-shooting-green-blue-enemies


WebX v1.7.0 Shooting and switching weapons complete

v1.7.0-shooting-switching-weapons


WebX v1.7.0.1 Weapons rotation broken and fixed

v1.7.0.1-weapons-rotation-fixed


WebX v1.7.1 Weapons block collisions

v1.7.1-weapons-block-collisions


WebX v1.7.2 Chasing enemies complete

v1.7.2-chasing-enemies


WebX v1.7.3 Chasing enemies complete with other items

v1.7.3-chasing-enemies-with-other-items


WebX v1.7.4 Chasing enemies x comp, vel change in chasing

v1.7.4-chasing-x-comp-vel-change


WebX v1.7.5 Chasing enemies symetrically distibuted

v1.7.5-chasing-enemies-symetrically-distibuted


WebX v1.7.6.1 Fuel items placement symetrically and randomly

v1.7.6.1-fuel-items-sym-random


WebX v1.7.6.2 Main and short gameplays merged

v1.7.6.2-main-short-gameplays-merged


WebX v1.7.7 Scores added for the first time

v1.7.7-scores-added-for-the-first-time


WebX v1.8.0 Code refactored Only two gameplays

v1.8.0-code-refactored-only-two-gameplays


WebX v1.8.1 Code refactor complete Only two gameplays

v1.8.1-code-refactor-done


WebX v1.8.2 Singleton shared across the scenes

v1.8.2-singleton-shared-across-the-scenes


WebX v1.8.3 Singleton crashes rectified

v1.8.3-singleton-crashes-rectified


WebX v1.8.4 Adhoc distribution fixed

v1.8.4-adhoc-distribution-fixed


WebX v1.8.5 Initial experiments with gamecenter

v1.8.5-initial-experiments-with-gamecenter


WebX v1.9.0 Facebook done, Game center working

v1.9.0-facebook-game-center-working


WebX v1.9.1 Facebook Game center internet connection alerts

v1.9.1-fb-gc-internet-connection-alerts


WebX v1.9.2 started with FQLs

v1.9.2-started-with-fqls


WebX v1.9.3 Creating a seperate ViewController for Facebook

v1.9.3-creating-seperate-ViewController-for-fb


WebX v1.9.4 Working on ViewController for Facebook

v1.9.4-working-on-viewcontroller-for-fb


WebX v1.9.5 Retrieved Profile picture and Name from Facebook

v1.9.5-retrieved-dp-and-name-from-fb


WebX v1.9.6 Retrieved Personal and Friends from Facebook

v1.9.6-retrieved-personal-and-friends-from-fb


WebX v1.9.7 Custom Loading added with internet access delay

v1.9.7-custom-loading-with-internet-access-delay


WebX v1.9.8 Facebook Publish working good

v1.9.8-fb-publish-working-good


WebX v1.9.9 added code data

v1.9.9-added-code-data


WebX v1.9.9 backup

v1.9.9-backup


WebX v1.9.9 code fixed back to ios 4.3

v1.9.9-code-fixed-back-to-ios4.3


WebX v1.9.9 Facebook publish with array of dictionaries

v1.9.9-fb-publish-with-array-of-dictionaries


WebX v1.9.9.1 Core Data used to save facebook info

v1.9.9.1-core-data-used-to-save-fb-info


WebX v1.9.9.2 Core Data used to fetch facebook info

v1.9.9.2-core-data-used-to-fetch-fb-info


WebX v1.9.9.3 Core Data used to fetch personal _ freinds facebook info

v1.9.9.3-core-data-fetch-freinds-fb


WebX v1.9.9.4.1 UITabBarController Buggy

v1.9.9.4.1-UITabBarController-buggy


WebX v1.9.9.4.2 UITabBarController completed with 2 Views

v1.9.9.4.2-UITabBarController-completed-with-2-views


WebX v1.9.9.4.3 UITabBarController titles and timers added

v1.9.9.4.3-UITabBarController-titles-and-timers


WebX v1.9.9.4.4 About to change Game Center code

v1.9.9.4.4-about-to-change-gc-code


WebX v1.9.9.4.4 Game Center details problem updating in core data

v1.9.9.4.4-gc-details-problem-updating-in-core-data


WebX v1.9.9.4.4 Initial attempt to store Game Center details in Core Data

v1.9.9.4.4-store-gc-details-in-core-data


WebX v1.9.9.4.5 Game Center details in core data

v1.9.9.4.5-gc-details-in-core-data


WebX v1.9.9.4.6 Facebook removed only core data in 3rd view controller

v1.9.9.4.6-fb-removed-only-core-data-in-3rd-view-controller


WebX v1.9.9.4.7 Custom table view created

v1.9.9.4.7-custom-table-view-created


WebX v1.9.9.4.8 Game Center Facebook connections backup

v1.9.9.4.8-gc-fb-connections-backup


WebX v2.0 attempting graphics

v2.0-attempting-graphics


WebX v2.0.1 Burning rocks animated

v2.0.1-burning-rocks-animated


WebX v2.0.3 Normal and Burning rocks animated

v2.0.3-normal-and-burning-rocks-animated


WebX v2.0.4 Backup before main menu animation

v2.0.4-backup-before-main-menu-animation


WebX v2.0.5 Terrain getting ready

v2.0.5-terrain-getting-ready


WebX v2.0.6 replacing all rocks to one

v2.0.6-replacing-all-rocks-to-one


WebX v2.0.7 creating moving rocks

v2.0.7-creating-moving-rocks


WebX v2.0.8 Atmosphere 2 layers rocks, ash

v2.0.8-atmosphere-2-layers-rocks-ash
