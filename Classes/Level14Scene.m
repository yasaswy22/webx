//
//  Level14Scene.m
//  WebX
//
//  Created by Sunny's Mac on 2/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "level14Scene.h"
#import "MainMenuScene.h"
#import "ChaserItem.h"

@implementation Level14Scene

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* level14Scene = [CCScene node];
	Level14Scene* level14Layer = [Level14Scene node];
	[level14Scene addChild:level14Layer];
	return level14Scene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		screenSize = [[CCDirector sharedDirector] winSize];
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		self.isAccelerometerEnabled=YES;
		
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:15];
		
		CCMenuItemFont *exitButton = [CCMenuItemFont itemFromString:@"exit" target:self selector:@selector(returnMainMenu:)];
		exitButton.color = ccc3(TEXT_COLOR);
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5, 
								  screenSize.height - exitButton.contentSize.height * 0.5);
		
		pauseButton = [CCMenuItemFont itemFromString:@"  pause" target:self selector:@selector(pauseGame:)];
		pauseButton.color = ccc3(TEXT_COLOR);
		pauseButton.position = ccp(screenSize.width - pauseButton.contentSize.width * 0.5, 
								   pauseButton.contentSize.height * 0.5);
		
		CCMenu* menu = [CCMenu menuWithItems:exitButton, pauseButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu z:3];
		
		player = [PlayerItem spriteWithFile:@"Volleyball_Ball.png"];
		player.position = ccp(screenSize.width*0.5, MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		player.velocity = CGPointZero;
		player.numNonStaticCollectables = 0;
		player.acceleration = CGPointZero;
		player.decelaration = CGPointZero;
		player.playerBoost = NO;
		player.velocity = ccp(0, PLAYER_MAX_VELOCITY);
		player.acceleration = ccp(0, PLAYER_INITIAL_ACCELERATION);
		[self addChild:player z:2];
		
		fuelTank = [FuelItem spriteWithFile:@"fuel_tank.png"];
		int i = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
		int j = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
		fuelTank.position = ccp(i, j);
		[self addChild:fuelTank z:2];
		
		chasingEnemies = [[NSMutableArray alloc]init];
		CCTexture2D *chasingEnemyTexture = [[CCTextureCache sharedTextureCache] addImage: @"Soccer_Ball.png"];
		for (int i=0; i<NUMBER_OF_CHASING_ENEMIES; i++) {
			ChaserItem *chasingEnemy = [ChaserItem spriteWithTexture:chasingEnemyTexture];
			
			CGFloat j = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
			CGFloat k =  arc4random() % (int)(screenSize.height*0.5);
			
			chasingEnemy.position = ccp(j, MIN_CHASING_ENEMY_VERTICAL_MOVE_WHEN_BOOST_HEIGHT-k);
			chasingEnemy.direction = YES;
			chasingEnemy.maxAllowedHeightFromBottom = MIN_CHASING_ENEMY_VERTICAL_MOVE_WHEN_BOOST_HEIGHT;
			[chasingEnemies addObject:chasingEnemy];
			[self addChild:chasingEnemy z:2];
		}
		
		[self scheduleUpdate];
		playerTouched = NO;
		screenTouched = NO;
		accumulatedTime = 0;
		totalTime = 0;
	}
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	deltaTime = delta;
	
	previousPlayerPosition = player.position;
	
	[player powerUpForces];
	[player playerFrictionalForces];
	[player limitPlayerBoundaries];
	[player playerFrictionalForcesEnemyCollisionMovement];
	[player checkPlayerTakesFuel: fuelTank];
	[fuelTank updateFuelTankPosition: player.velocity];
	[player update];
	[player updateAccelerometerInput: acceX];	
	[player maintainPlayerAtMinHeight: deltaTime];  // should be before player block collision to bring back to min height
	
	for (int i=0; i<chasingEnemies.count; i++) {
		ChaserItem *chasingEnemy = [chasingEnemies objectAtIndex:i];
		
		[chasingEnemy updateChasingEnemyMovement: player.velocity];
		[chasingEnemy chasingEnemyBackingMovement: player :deltaTime];
	}
	
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint unconvertedLocation = [touch locationInView: [touch view]];
	CGPoint location = [[CCDirector sharedDirector] convertToGL: unconvertedLocation];
	
	touchedLocation = location;
	
	screenTouched = YES;
	return YES;
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint unconvertedLocation = [touch locationInView: [touch view]];
	CGPoint location = [[CCDirector sharedDirector] convertToGL: unconvertedLocation];
	
	touchedLocation = location;
	
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	
	screenTouched = NO;
	
	CGPoint unconvertedLocation = [touch locationInView: [touch view]];
	CGPoint location = [[CCDirector sharedDirector] convertToGL: unconvertedLocation];
	
}

-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	acceX = 0;
	if(acceleration.x > ACCELEROMETER_MIN_VALUE)
		acceX = 1;
	else if(acceleration.x < -ACCELEROMETER_MIN_VALUE)
		acceX = -1;
}

-(void) pauseGame: (id) sender {
	if ([[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] resume];
		[pauseButton setString:@"  pause"];
	}
	else if (![[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] pause];
		[pauseButton setString:@"resume"];
	}
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) draw {
	[super draw];
	
	glEnable(GL_LINE_SMOOTH);
	glColor4ub(125, 100, 100, 255);  
    glLineWidth(1.0f);
	
	if (screenTouched) {
		//		CCLOG(@"player.position: (%f, %f), touchedLocation: (%f, %f)", 
		//			  player.position.x, player.position.y, touchedLocation.x, touchedLocation.y);
		//		ccDrawLine(player.position, touchedLocation);
	}
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[chasingEnemies release];
	[super dealloc];
}

@end


@end
