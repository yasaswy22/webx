//
//  CollectedWeaponItem.h
//  WebX
//
//  Created by Sunny's Mac on 1/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CollectedWeaponItem : CCSprite {

@private
	CGPoint velocity;
	CGPoint acceleration;
	CGPoint collisionForce;
	CGPoint frictionalForce;
	BOOL nonStatic;
	BOOL flying;
	BOOL active;
}

@property CGPoint velocity;
@property CGPoint acceleration;
@property CGPoint collisionForce;
@property CGPoint frictionalForce;
@property BOOL nonStatic;
@property BOOL flying;
@property BOOL active;

-(void) update;
-(void) holdUnactivatedCollectedWeapon: (CGPoint) playerPosition;
-(void) activateCollectedWeapon: (CGPoint) clickedLocation;
-(void) enemyMovementCollectableWeaponCollision: (NSMutableArray *) enemies;
-(void) checkCollectedWeaponBoundaries;

@end
