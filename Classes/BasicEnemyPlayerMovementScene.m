//
//  BasicEnemyPlayerMovementScene.m
//  WebX
//
//  Created by Sunny's Mac on 1/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BasicEnemyPlayerMovementScene.h"
#import "MainMenuScene.h"
#import "GlobalVars.h"
#import "EnemyItem.h"

@implementation BasicEnemyPlayerMovementScene

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* basicEnemyPlayerMovementScene = [CCScene node];
	BasicEnemyPlayerMovementScene* basicEnemyPlayerMovementLayer = [BasicEnemyPlayerMovementScene node];
	[basicEnemyPlayerMovementScene addChild:basicEnemyPlayerMovementLayer];
	return basicEnemyPlayerMovementScene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		screenSize = [[CCDirector sharedDirector] winSize];
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		self.isAccelerometerEnabled=YES;
		
		CCMenuItemImage* exitButton = [CCMenuItemImage itemFromNormalImage:@"exit_brown.png" 
															 selectedImage:@"exit_brown.png"
																	target:self
																  selector:@selector(returnMainMenu:)];
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5,
								  screenSize.height - exitButton.contentSize.height * 0.5);
		CCMenu* menu = [CCMenu menuWithItems:exitButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu];
		
		player = [PlayerItem spriteWithFile:@"hero.png"];
		player.position = ccp(screenSize.width*0.5, MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		player.velocity = CGPointZero;
		player.acceleration = CGPointZero;
		player.decelaration = CGPointZero;
		//		player.velocity = ccp(0, PLAYER_INITIAL_VELOCITY);
		//		player.acceleration = ccp(0, PLAYER_INITIAL_ACCELERATION);
		[self addChild:player z:2];
		
		staticEnemies = [[NSMutableArray alloc]init];
		CCTexture2D *enemyTexture = [[CCTextureCache sharedTextureCache] addImage: @"solder_small_2.png"];
		for (int i=0; i<NUMBER_OF_STATIC_ENEMIES; i++) {
			EnemyItem *enemy = [EnemyItem spriteWithTexture:enemyTexture];
			
			int j = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
			int k = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
			
			enemy.position = ccp(j, k);
			
			[staticEnemies addObject:enemy];
			[self addChild:enemy z:1];
		}
		
		[self scheduleUpdate];
		screenTouched = NO;
	}
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	[self powerUpForces];
	[self playerBoostMovement];
	[self frictionalForces];
	[self updateEnemiesPosition];
	
	deltaTime = delta;
	
	[player update];
	
	if(!CGPointEqualToPoint(player.velocity, CGPointZero)) {
		CGPoint pos = player.position;
		pos.x += lor*2 ;
		player.position = pos;
	}
}

-(void) powerUpForces {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	if (player.fuel > 0) {
		// Acceleration due to player pressing the screen..
		player.acceleration = ccp(player.acceleration.x, player.acceleration.y + TOUCH_ACCELERATION);
	}
}


	-(void) playerBoostMovement {
		
		if (!CGPointEqualToPoint(player.velocity, CGPointZero)) {	
			if (playerBoost) {
				if (player.position.y < MAX_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT) {
					player.position = ccp(player.position.x, player.position.y+(TOUCH_PLAYER_VER_BOOST_ACCELERATION*deltaTime));
				}
				else if(player.position.y >= MAX_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT){
					playerBoost = NO;
				}
			}
			else if(!playerBoost) {
				if (player.position.y > MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT) {
					player.position = ccp(player.position.x, player.position.y-(TOUCH_PLAYER_VER_BOOST_ACCELERATION*0.25*deltaTime));
				}
			}
		}
	}
	
-(void) frictionalForces {
	if (!CGPointEqualToPoint(player.velocity, CGPointZero)) {
	player.decelaration = ccp(player.decelaration.x, player.decelaration.y+DYNAMIC_FRICTIONAL_DECELERATION);
	}
}

-(void) updateEnemiesPosition {
	
	for (int i=0; i<staticEnemies.count; i++) {
		EnemyItem *enemy = [staticEnemies objectAtIndex:i];
		enemy.velocity = ccp(-player.velocity.x, -player.velocity.y);
		
		if (enemy.position.y < -enemy.contentSize.height*0.5) {
			int j = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
			int k = (int)screenSize.height + arc4random() % (int)(screenSize.height*0.5);
			
			enemy.position = ccp(j, k);
		}
		[enemy update];
	}
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	
	player.fuel = PLAYER_MAX_FUEL;
	player.decelaration = CGPointZero;
	playerBoost = YES;
	
	return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	screenTouched = NO;
}

-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	lor = 0;
	if(acceleration.x > 0)
		lor = 1;
	else if(acceleration.x < 0)
		lor = -1;
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[staticEnemies release];
	[super dealloc];
}

@end
