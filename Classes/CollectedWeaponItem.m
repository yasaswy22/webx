//
//  CollectedWeaponItem.m
//  WebX
//
//  Created by Sunny's Mac on 1/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CollectedWeaponItem.h"
#import "GlobalVars.h"
#import "EnemyItem.h"

@implementation CollectedWeaponItem

@synthesize velocity;
@synthesize acceleration;
@synthesize collisionForce;
@synthesize frictionalForce;
@synthesize nonStatic;
@synthesize flying;
@synthesize active;

-(void) update {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	self.position = ccp(self.position.x+velocity.x, self.position.y+velocity.y);
}

-(void) holdUnactivatedCollectedWeapon: (CGPoint) playerPosition {
		
		if (!self.active) {
			self.visible = NO;
			self.velocity = CGPointZero;
			self.position = playerPosition;
		}
}

-(void) activateCollectedWeapon: (CGPoint) clickedLocation {
	if (!self.active) {
		
		self.active = YES;
		self.visible = YES;
		
		CGPoint clickedLocationPositionToWeapon = ccp(clickedLocation.x - self.position.x, 
													  clickedLocation.y - self.position.y);
		
		CGFloat clickedLocationAngleToWeapon = ccpToAngle(ccpNormalize(clickedLocationPositionToWeapon));
		
		self.velocity = ccp(NON_STATIC_WEAPON_SHOOT_VELOCITY*cos(clickedLocationAngleToWeapon),
												NON_STATIC_WEAPON_SHOOT_VELOCITY*sin(clickedLocationAngleToWeapon));
	}
}

-(void) enemyMovementCollectableWeaponCollision: (NSMutableArray *) enemies {
	
	CGRect collectedWeaponRect = CGRectMake(self.position.x - self.contentSize.width*0.5, 
											self.position.y - self.contentSize.height*0.5, 
											self.contentSize.width, self.contentSize.height);
	
	CGRect collectedWeaponInfluenceRect = CGRectMake(self.position.x - FLYING_WEAPON_DESTRUCTIVE_INFLUENCE*self.contentSize.width*0.5, 
													 self.position.y - FLYING_WEAPON_DESTRUCTIVE_INFLUENCE*self.contentSize.height*0.5, 
													 FLYING_WEAPON_DESTRUCTIVE_INFLUENCE*self.contentSize.width, 
													 FLYING_WEAPON_DESTRUCTIVE_INFLUENCE*self.contentSize.height);
	
	BOOL collidedWithEnemy = NO;
	
	for(int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];
		
		if (self.nonStatic) {
			if (self.active) {
				
				CGRect enemyRect = CGRectMake(enemy.position.x - enemy.contentSize.width*0.5, 
											  enemy.position.y - enemy.contentSize.height*0.5, 
											  enemy.contentSize.width, enemy.contentSize.height);
				
				if(CGRectIntersectsRect(collectedWeaponRect, enemyRect)) { 
					
					CGPoint enemyPositionTocollectedWeapon = ccp(enemy.position.x - self.position.x, 
																 enemy.position.y - self.position.y);
					CGFloat enemyAngleTocollectedWeapon = ccpToAngle(enemyPositionTocollectedWeapon);
					
					enemy.collisionForce = ccp(STATIC_ENEMY_COLLISION_FORCE*cos(enemyAngleTocollectedWeapon), 
											  STATIC_ENEMY_COLLISION_FORCE*sin(enemyAngleTocollectedWeapon));
					
					enemy.frictionalForce = ccp(STATIC_ENEMY_FRICTIONAL_DECELERATION*cos(enemyAngleTocollectedWeapon),
											   STATIC_ENEMY_FRICTIONAL_DECELERATION*sin(enemyAngleTocollectedWeapon));
				}
			}
		}

		else if(self.flying) {
			if (self.active) {
	
				CGRect enemyRect = CGRectMake(enemy.position.x - enemy.contentSize.width*0.5, 
											  enemy.position.y - enemy.contentSize.height*0.5, 
											  enemy.contentSize.width, enemy.contentSize.height);
				
				if(CGRectIntersectsRect(collectedWeaponRect, enemyRect)) { 
					collidedWithEnemy = YES;
				}
			}
		}
	}
	
	if (collidedWithEnemy) {
		for(int i=0; i<enemies.count; i++) {
			EnemyItem *enemy = [enemies objectAtIndex:i];
			if (self.active) {
				
				CGRect enemyRect = CGRectMake(enemy.position.x - enemy.contentSize.width*0.5, 
											  enemy.position.y - enemy.contentSize.height*0.5, 
											  enemy.contentSize.width, enemy.contentSize.height);
				
				if(CGRectIntersectsRect(collectedWeaponInfluenceRect, enemyRect)) { 
					[enemy resetEnemyPosition];

				}
			}
			
		}
		self.active = NO;
	}
}

-(void) checkCollectedWeaponBoundaries {
		if (self.position.y < -self.contentSize.height*0.5 ||
			self.position.y > screenSize.height-self.contentSize.height*0.5 ||
			self.position.x < -self.contentSize.width*0.5 ||
			self.position.x > screenSize.width-self.contentSize.width*0.5) {
			
			self.active = NO;
		}
}

@end
