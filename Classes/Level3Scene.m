//
//  Level3Scene.m
//  WebX
//
//  Created by Sunny's Mac on 1/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Level3Scene.h"
#import "MainMenuScene.h"
#import "GlobalVars.h"
#import "EnemyItem.h"

@implementation Level3Scene
+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* level3Scene = [CCScene node];
	Level3Scene* level3Layer = [Level3Scene node];
	[level3Scene addChild:level3Layer];
	return level3Scene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		screenSize = [[CCDirector sharedDirector] winSize];
		//		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		self.isAccelerometerEnabled=YES;
		
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:15];
		
		CCMenuItemFont *exitButton = [CCMenuItemFont itemFromString:@"exit" target:self selector:@selector(returnMainMenu:)];
		exitButton.color = ccc3(TEXT_COLOR);
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5, 
								  screenSize.height - exitButton.contentSize.height * 0.5);
		
		pauseButton = [CCMenuItemFont itemFromString:@"  pause" target:self selector:@selector(pauseGame:)];
		pauseButton.color = ccc3(TEXT_COLOR);
		pauseButton.position = ccp(screenSize.width - pauseButton.contentSize.width * 0.5, 
								   pauseButton.contentSize.height * 0.5);
		
		CCMenu* menu = [CCMenu menuWithItems:exitButton, pauseButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu];
		
		player = [PlayerItem spriteWithFile:@"Volleyball_Ball.png"];
		player.position = ccp(screenSize.width*0.5, MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		player.velocity = CGPointZero;
		player.acceleration = CGPointZero;
		player.decelaration = CGPointZero;
		player.velocity = ccp(0, PLAYER_INITIAL_VELOCITY);
		player.acceleration = ccp(0, PLAYER_INITIAL_ACCELERATION);
		[self addChild:player z:2];
		
		fuelTank = [FuelItem spriteWithFile:@"fuel_tank.png"];
		int i = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
		int j = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
		fuelTank.position = ccp(i, j);
		[self addChild:fuelTank z:2];
		
		flyingEnemies = [[NSMutableArray alloc]init];
		CCTexture2D *enemyTexture = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_cyan.png"];
		
		// In Layer 1..
		for (int i=0; i<(int)NUMBER_OF_FLYING_ENEMIES; i++) {
			EnemyItem *enemy = [EnemyItem spriteWithTexture:enemyTexture];
			
			int side = arc4random()%2;
			int j = arc4random() % (int)(screenSize.width*0.5);
			int k = (int) screenSize.height * 0.5 + arc4random() % (int)(screenSize.height*0.5);
			
			if (side == 0) {
				enemy.position = ccp(-j, k);
				enemy.velocity = ccp(FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
			else if (side == 1) {
				enemy.position = ccp(screenSize.width + j, k);
				enemy.velocity = ccp(-FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
			
			enemy.collisionForce = CGPointZero;
			enemy.frictionalForce = CGPointZero;
			[flyingEnemies addObject:enemy];
			[self addChild:enemy z:1];
		}
		
		[self scheduleUpdate];
		screenTouched = NO;
	}
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	[self powerUpForces];
	[self playerBoostMovement];
	[self frictionalForces];
	[self updateFuelTankPosition];
	[self updateEnemiesPosition];
	[self updatePlayerPosition];
	[self playerMovementPlayerEnemiesCollision];
	[self enemyMovementPlayerEnemiesCollision];
	[self checkPlayerTakesFuel];
	
	deltaTime = delta;
	
	[player update];
	
	if(!CGPointEqualToPoint(player.velocity, CGPointZero)) {
		CGPoint pos = player.position;
		pos.x += lor*2 ;
		player.position = pos;
	}
}

-(void) powerUpForces {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	if (player.fuel > 0) {
		// Acceleration due to player pressing the screen..
		player.acceleration = ccp(player.acceleration.x, player.acceleration.y + TOUCH_ACCELERATION);
	}
}

-(void) playerBoostMovement {
		
		if (!CGPointEqualToPoint(player.velocity, CGPointZero)) {	
			if (playerBoost) {
				if (player.position.y < MAX_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT) {
					player.position = ccp(player.position.x, player.position.y+(TOUCH_PLAYER_VER_BOOST_ACCELERATION*deltaTime));
				}
				else if(player.position.y >= MAX_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT){
					playerBoost = NO;
				}
			}
			else if(!playerBoost) {
				if (player.position.y > MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT) {
					player.position = ccp(player.position.x, player.position.y-(TOUCH_PLAYER_VER_BOOST_ACCELERATION*0.25*deltaTime));
				}
			}
		}
	}
	
-(void) frictionalForces {
	if (!CGPointEqualToPoint(player.velocity, CGPointZero)) {
	player.decelaration = ccp(player.decelaration.x, player.decelaration.y+DYNAMIC_FRICTIONAL_DECELERATION);
	
	CGFloat playerCollisionForceValue = sqrt(player.collisionForce.x*player.collisionForce.x +
											 player.collisionForce.y*player.collisionForce.y);
	CGFloat playerFrictionalForceValue = sqrt(player.collisionFrictionalForce.x*player.collisionFrictionalForce.x +
											  player.collisionFrictionalForce.y*player.collisionFrictionalForce.y);
	
	if (playerCollisionForceValue > playerFrictionalForceValue) {
		player.collisionForce = ccp(player.collisionForce.x - player.collisionFrictionalForce.x, 
									player.collisionForce.y - player.collisionFrictionalForce.y);
		player.velocity = ccp(player.velocity.x + player.collisionForce.x, 
							  player.velocity.y + player.collisionForce.y);
		//		[player update];
		//		CCLOG(@"player.velocity: (%f, %f)", player.velocity.x, player.velocity.y);
	}
	else if(playerCollisionForceValue <= playerFrictionalForceValue){
		player.collisionForce = CGPointZero;
		player.collisionFrictionalForce = CGPointZero;
		player.velocity = ccp(0, player.velocity.y);
	}
	}
}

-(void) updateFuelTankPosition {
	
	fuelTank.velocity = ccp(-player.velocity.x, -player.velocity.y);
	
	if (fuelTank.position.y < -fuelTank.contentSize.height*0.5 || 
		fuelTank.position.x < -fuelTank.contentSize.width*0.5 ||
		fuelTank.position.x > screenSize.width+fuelTank.contentSize.width*0.5) {
		
		int j = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
		int k = (int)screenSize.height + arc4random() % (int)(screenSize.height*0.5);
		
		fuelTank.position = ccp(j, k);
		fuelTank.visible = YES;
	}
	[fuelTank update];
}

-(void) updateEnemiesPosition {
	
	for (int i=0; i<flyingEnemies.count; i++) {
		EnemyItem *enemy = [flyingEnemies objectAtIndex:i];
		
		enemy.position = ccp(enemy.position.x, enemy.position.y-player.velocity.y);
		
		if (enemy.velocity.x > 0) {
			if(enemy.position.x > screenSize.width+enemy.contentSize.width*0.5 ||
			   enemy.position.y < -enemy.contentSize.height*0.5) {
				
				int side = arc4random()%2;
				int j = arc4random() % (int)(screenSize.width*0.5);
				int k = arc4random() % (int)(screenSize.height*1.5);
				
				if (side == 0) {
					enemy.position = ccp(-j, k);
					enemy.velocity = ccp(FLYING_ENEMY_HOR_ACCELERATION, 0);
				}
				else if (side == 1) {
					enemy.position = ccp(screenSize.width + j, k);
					enemy.velocity = ccp(-FLYING_ENEMY_HOR_ACCELERATION, 0);
				}
			}
		}
		else if (enemy.velocity.x < 0) {
			if(enemy.position.x < -enemy.contentSize.width*0.5 ||
			   enemy.position.y < -enemy.contentSize.height*0.5) {
				
				int side = arc4random()%2;
				int j = arc4random() % (int)(screenSize.width*0.5);
				int k = arc4random() % (int)(screenSize.height*1.5);
				
				if (side == 0) {
					enemy.position = ccp(-j, k);
					enemy.velocity = ccp(FLYING_ENEMY_HOR_ACCELERATION, 0);
				}
				else if (side == 1) {
					enemy.position = ccp(screenSize.width + j, k);
					enemy.velocity = ccp(-FLYING_ENEMY_HOR_ACCELERATION, 0);
				}
			}
		}
		else if(enemy.velocity.x == 0 && enemy.position.y < -enemy.contentSize.height*0.5) {
			int side = arc4random()%2;
			int j = arc4random() % (int)(screenSize.width*0.5);
			int k = arc4random() % (int)(screenSize.height*1.5);
			
			if (side == 0) {
				enemy.position = ccp(-j, k);
				enemy.velocity = ccp(FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
			else if (side == 1) {
				enemy.position = ccp(screenSize.width + j, k);
				enemy.velocity = ccp(-FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
		}
		[enemy update];
	}
}

-(void) updatePlayerPosition {
	if (player.position.x < player.contentSize.width*0.5) {
		player.position = ccp(player.contentSize.width*0.5, player.position.y);
	}
	else if (player.position.x > screenSize.width-player.contentSize.width*0.5) {
		player.position = ccp(screenSize.width-player.contentSize.width*0.5, player.position.y);
	}
}

-(void) playerMovementPlayerEnemiesCollision {
	for (int i=0; i<flyingEnemies.count; i++) {
		EnemyItem *enemy = [flyingEnemies objectAtIndex:i];
		
		CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
									   player.position.y - player.contentSize.height*0.5, 
									   player.contentSize.width, player.contentSize.height);
		
		CGRect enemyRect = CGRectMake(enemy.position.x - enemy.contentSize.width*0.5, 
									  enemy.position.y - enemy.contentSize.height*0.5, 
									  enemy.contentSize.width, enemy.contentSize.height);
		
		if(CGRectIntersectsRect(playerRect, enemyRect) && enemy.position.y > player.position.y) {
			
			CGPoint playerPositionToEnemy = ccp(player.position.x - enemy.position.x, player.position.y - enemy.position.y);
			CGFloat playerAngleToEnemy = ccpToAngle(playerPositionToEnemy);
			
			CGFloat playerVelocity = sqrt(player.velocity.x*player.velocity.x + player.velocity.y*player.velocity.y);
			CGFloat collisionForceMagnitude = PLAYER_COLLISION_FORCE_PLAYER_ENEMY_COLLISION * playerVelocity / PLAYER_MAX_VELOCITY;
			CGFloat frictionalForceMagnitude = PLAYER_FRICTIONAL_DECELERATION_PLAYER_ENEMY_COLLISION * playerVelocity / PLAYER_MAX_VELOCITY;
			
			player.collisionForce = ccp(collisionForceMagnitude*cos(playerAngleToEnemy), 
										collisionForceMagnitude*sin(playerAngleToEnemy));
			player.collisionFrictionalForce = ccp(frictionalForceMagnitude*cos(playerAngleToEnemy),
												  frictionalForceMagnitude*sin(playerAngleToEnemy));
		}
	}
}

-(void) enemyMovementPlayerEnemiesCollision {
	for (int i=0; i<flyingEnemies.count; i++) {
		EnemyItem *enemy = [flyingEnemies objectAtIndex:i];
		
		CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
									   player.position.y - player.contentSize.height*0.5, 
									   player.contentSize.width, player.contentSize.height);
		
		CGRect enemyRect = CGRectMake(enemy.position.x - enemy.contentSize.width*0.5, 
									  enemy.position.y - enemy.contentSize.height*0.5, 
									  enemy.contentSize.width, enemy.contentSize.height);
		
		if(CGRectIntersectsRect(playerRect, enemyRect)) {
			
			enemy.position = ccp(enemy.position.x, enemy.position.y-player.velocity.y);
			if(FLYING_ENEMY_VER_FALL_VELOCITY!=0)
				enemy.velocity = ccp(0, FLYING_ENEMY_VER_FALL_VELOCITY);
			else {
				enemy.velocity = ccp(0, FLYING_ENEMY_VER_FALL_VELOCITY_MIN);
			}
		}
		
		[enemy update];
	}
}

-(void) checkPlayerTakesFuel {
	
	CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
								   player.position.y - player.contentSize.height*0.5, 
								   player.contentSize.width, player.contentSize.height);
	
	CGRect fuelRect = CGRectMake(fuelTank.position.x - fuelTank.contentSize.width*0.5, 
								 fuelTank.position.y - fuelTank.contentSize.height*0.5, 
								 fuelTank.contentSize.width, fuelTank.contentSize.height);
	
	if(CGRectIntersectsRect(playerRect, fuelRect)) {
		
		player.fuel = PLAYER_MAX_FUEL;
		player.decelaration = CGPointZero;
		playerBoost = YES;
		fuelTank.visible = NO;
	}
}

-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	lor = 0;
	if(acceleration.x > ACCELEROMETER_MIN_VALUE)
		lor = 1;
	else if(acceleration.x < -ACCELEROMETER_MIN_VALUE)
		lor = -1;
}

-(void) pauseGame: (id) sender {
	if ([[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] resume];
		[pauseButton setString:@"  pause"];
	}
	else if (![[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] pause];
		[pauseButton setString:@"resume"];
	}
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[flyingEnemies release];
	[super dealloc];
}

@end
