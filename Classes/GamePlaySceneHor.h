//
//  GamePlayScene.h
//  WebProject
//
//  Created by Sunny's Mac on 12/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "EnemyItem.h"
#import "EnemyWeaponItem.h"
#import "GlobalVars.h"

@interface GamePlaySceneHor : CCLayer {

@private
	PlayerItem *player;
	NSMutableArray *enemies;
	NSMutableArray *enemyWeapons;
	BOOL screenTouched;
	BOOL playerStatic;
	CCLabelTTF *velocityLabel;
	CCLabelTTF *accelerationLabel;
	CCLabelTTF *decelerationLabel;
	CCLabelTTF *fuelLabel;
	NSString *velocityLabelText;
	NSString *accelerationLabelText;
	NSString *decelerationLabelText;
	NSString *fuelLabelText;
	CGRect playerInfluence;
	ccTime deltaTime;
	BOOL playerBoost;
	CGFloat lor;
	
	CCSprite *background1;
	CCSprite *background2;
	CCSprite *sideWallLeft1;
	CCSprite *sideWallLeft2;
	CCSprite *sideWallRight1;
	CCSprite *sideWallRight2;
}

@end
