//
//  ChaserItemShortGameplay.h
//  WebX
//
//  Created by Sunny's Mac on 2/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface ChaserItemShortGameplay : CCSprite {

@private
	BOOL active;
	
}

@property BOOL active;

-(void) resetChaserItemShortGameplay;

@end
