//
//  FacebookTable.h
//  WebX
//
//  Created by Sunny's Mac on 7/5/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GameCenterFacebookTable;

@interface FacebookTable : NSManagedObject {
@private
}
@property (nonatomic, retain) NSData * facebookPic;
@property (nonatomic, retain) NSString * facebookName;
@property (nonatomic, retain) NSNumber * facebookUUID;
@property (nonatomic, retain) NSNumber * playingThisGame;
@property (nonatomic, retain) GameCenterFacebookTable * facebookGCRelation;

+(FacebookTable *) entryWithFacebookData:(NSDictionary *)facebookData inManagedObjectContext:(NSManagedObjectContext *)context;

@end
