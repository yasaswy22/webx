//
//  GameOverScene.m
//  WebX
//
//  Created by Sunny's Mac on 12/31/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GameOverScene.h"
#import "MainMenuScene.h"

@implementation GameOverScene

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* gameOverScene = [CCScene node];
	GameOverScene* gameOverLayer = [GameOverScene node];
	[gameOverScene addChild:gameOverLayer];
	return gameOverScene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		
		screenSize = [[CCDirector sharedDirector] winSize];
		CCSprite *background = [CCSprite spriteWithFile:@"level_select.png"];
		background.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
		[self addChild:background];
		
		CCLabelTTF *title = [CCLabelTTF labelWithString:@"Game Over" fontName:@"Marker Felt" fontSize:32];
		title.position =  ccp(screenSize.width * 0.5, screenSize.height*0.8 - title.contentSize.height * 0.5);
		title.color = ccc3(TEXT_COLOR);
		[self addChild: title];
		
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:32];
		CCMenuItemFont *gameItemButton = [CCMenuItemFont itemFromString:@"back" target:self selector:@selector(returnMainMenu:)];
		gameItemButton.color = ccc3(TEXT_COLOR);
		gameItemButton.position = ccp(screenSize.width * 0.5, screenSize.height*0.5);
		CCMenu* menu = [CCMenu menuWithItems:gameItemButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu];
                
        sharedGlobalSingletonVarsInstance = [GlobalSingletonVars sharedGlobalSingletonVars];
        sharedGlobalSingletonVarsInstance.justPlayed = YES;
	}
	return self;
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[super dealloc];
}

@end
