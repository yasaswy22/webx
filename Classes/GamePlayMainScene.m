//
//  GamePlayMainScene.m
//  WebX
//
//  Created by Sunny's Mac on 2/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GamePlayMainScene.h"
#import "MainMenuScene.h"
#import "GamePlayShortScene.h"
#import "ChaserItem.h"
#import "WeaponItem.h"
#import "CollectableItem.h"
#import "CollectedWeaponItem.h"
#import "EnemyItem.h"
#import "GridItem.h"
#import "FuelItem.h"
#import "GameOverScene.h"
#import "FlyingMountainItem.h"
#import "AshItem.h"

#define NUM_FLYINGMOUNTAINS_LAYER1 2
#define NUM_FLYINGMOUNTAINS_LAYER2 10
#define NUM_ASHES 100

@implementation GamePlayMainScene

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* gamePlayMainScene = [CCScene node];
	GamePlayMainScene* gamePlayMainLayer = [GamePlayMainScene node];
	[gamePlayMainScene addChild:gamePlayMainLayer];
	return gamePlayMainScene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		screenSize = [[CCDirector sharedDirector] winSize];
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		self.isAccelerometerEnabled=YES;
		
        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES && [[UIScreen mainScreen] scale] == 2.00) {
            retinaDisplay = YES;
        }
        
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:15];
		
        ashes = [[NSMutableArray alloc] init];
        for (int i=0; i<NUM_ASHES; i++) {
            AshItem *ash = [AshItem spriteWithFile:@"ash.png"];
            [ash setPositioningInitially];
            [ashes addObject:ash];
            [self addChild:ash z:10];
        }

        CCSprite *atmosphere = [CCSprite spriteWithFile:@"background.png"];
		atmosphere.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
		[self addChild:atmosphere z:0];
        
//        CCSprite *terrain = [CCSprite spriteWithFile:@"terrain.png"];
//		terrain.position = ccp(terrain.contentSize.width*0.5, terrain.contentSize.height*0.5);
//		[self addChild:terrain z:5];
   
#pragma mark flying mountains        
        CCTexture2D *flyingMountain1Texture = [[CCTextureCache sharedTextureCache] addImage: @"flying_mountain_1.png"];
        CCTexture2D *flyingMountain2Texture = [[CCTextureCache sharedTextureCache] addImage: @"flying_mountain_2.png"];
        flyingMountains1 = [[NSMutableArray alloc] init];
        for (int i=0; i<NUM_FLYINGMOUNTAINS_LAYER1; i++) {
            FlyingMountainItem *flyingMountain = [FlyingMountainItem spriteWithTexture:flyingMountain1Texture];
            [flyingMountain setPositioningInitiallyLayer1Rocks];
            [self addChild:flyingMountain z:2];
            [flyingMountains1 addObject:flyingMountain];
        }

        flyingMountains2 = [[NSMutableArray alloc] init];
        for (int i=0; i<NUM_FLYINGMOUNTAINS_LAYER2; i++) {
            FlyingMountainItem *flyingMountain = [FlyingMountainItem spriteWithTexture:flyingMountain2Texture];
            [flyingMountain setPositioningInitiallyLayer2Rocks];
            [self addChild:flyingMountain z:1];
            [flyingMountains2 addObject:flyingMountain];
        }
        
		CCMenuItemFont *exitButton = [CCMenuItemFont itemFromString:@"exit" target:self selector:@selector(returnMainMenu:)];
		exitButton.color = ccc3(TEXT_COLOR);
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5, 
								  screenSize.height - exitButton.contentSize.height * 0.5);
		
		pauseButton = [CCMenuItemFont itemFromString:@"  pause" target:self selector:@selector(pauseGame:)];
		pauseButton.color = ccc3(TEXT_COLOR);
		pauseButton.position = ccp(screenSize.width - pauseButton.contentSize.width * 0.5, 
								   pauseButton.contentSize.height * 0.5);
		
		//		CCMenu* menu = [CCMenu menuWithItems:exitButton, pauseButton, nil];
		CCMenu* menu = [CCMenu menuWithItems:exitButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu z:10];
        
        sharedGlobalSingletonVarsInstance = [GlobalSingletonVars sharedGlobalSingletonVars];
        
		scoreLabel= [CCLabelTTF labelWithString:@"0" fontName:@"Marker Felt" fontSize:13];
		scoreLabel.anchorPoint = ccp(0,1);
		scoreLabel.position = ccp(0, screenSize.height);
		[self addChild:scoreLabel z:10];
		
		gamePlayMethods = [[GamePlayMethods alloc] init]; 
		
        #pragma mark initialize Player
        if (retinaDisplay) {
            NSString *plistFileName = [NSString stringWithFormat:@"white_skeleton-hd.plist"];
            NSString *imageFileName = [NSString stringWithFormat:@"white_skeleton-hd.png"];
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: plistFileName];
            CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
            [self addChild:spriteBatch z:9];
            NSMutableArray *skeletonAnimFrames = [NSMutableArray array];
            for(int i = 1; i <= 11; ++i) {
                if (i<10) {
                    [skeletonAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image0000%d.png", i]]];
                }
                else{
                    [skeletonAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image000%d.png", i]]];
                }
                skeletonAnim = [CCAnimation animationWithFrames:skeletonAnimFrames delay:0.1f];
            }
            
            player = [PlayerItem spriteWithSpriteFrameName:@"Image00001.png"];
            CCAction *skeletonAction = [CCRepeatForever actionWithAction:
                                            [CCAnimate actionWithAnimation:skeletonAnim restoreOriginalFrame:NO]];
            [player runAction:skeletonAction];
            [spriteBatch addChild:player];
        }
        else{
            NSString *plistFileName = [NSString stringWithFormat:@"white_skeleton.plist"];
            NSString *imageFileName = [NSString stringWithFormat:@"white_skeleton.png"];
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: plistFileName];
            CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
            [self addChild:spriteBatch z:9];
            NSMutableArray *skeletonAnimFrames = [NSMutableArray array];
            for(int i = 1; i <= 10; ++i) {
                if (i<10) {
                    [skeletonAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image0000%d.png", i]]];
                }
                else{
                    [skeletonAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image000%d.png", i]]];
                }
                skeletonAnim = [CCAnimation animationWithFrames:skeletonAnimFrames delay:0.1f];
            }
            
            player = [PlayerItem spriteWithSpriteFrameName:@"Image00001.png"];
            CCAction *skeletonAction = [CCRepeatForever actionWithAction:
                                        [CCAnimate actionWithAnimation:skeletonAnim restoreOriginalFrame:NO]];
            [player runAction:skeletonAction];
            [spriteBatch addChild:player];
        }

//		player = [PlayerItem spriteWithFile:@"Volleyball_Ball.png"];
		player.position = ccp(screenSize.width*0.5, MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		player.velocity = CGPointZero;
		player.numNonStaticCollectables = 0;
		player.acceleration = CGPointZero;
		player.decelaration = CGPointZero;
		player.playerBoost = NO;
        player.barLength = 0;
		player.velocity = ccp(0, PLAYER_MAX_VELOCITY);
		player.acceleration = ccp(0, PLAYER_INITIAL_ACCELERATION);
		sharedGlobalSingletonVarsInstance.maxFuelItemsThatCanBeTaken = MAX_FUEL_ITEMS_INITIALLY;
//		[self addChild:player z:9];
		
        #pragma mark initialize Foot (Fuel)
        
		CCTexture2D *fuelItemTexture = [[CCTextureCache sharedTextureCache] addImage: @"foot.png"];
		
		fuelItems1 = [[NSMutableArray alloc] init];
		for (int i=0; i<PATTERN_1_NUM_OF_FUEL_ITEMS; i++) {
			FuelItem *fuelItem = [FuelItem spriteWithTexture:fuelItemTexture];
			[fuelItem makeFuelItemInactive];
			[fuelItems1 addObject:fuelItem];
			[self addChild:fuelItem z:9];			
		}
		
		/*		
        
        #pragma mark initialize Static Enemies
        //Allows collision and reduces speed of the player when collided
        
		staticEnemies = [[NSMutableArray alloc]init];
		CCTexture2D *enemyTexture = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_brown.png"];
		for (int i=0; i<NUMBER_OF_STATIC_ENEMIES; i++) {
			EnemyItem *enemy = [EnemyItem spriteWithTexture:enemyTexture];
			
			int j = arc4random() % (int)(screenSize.width);
			int k = (int)screenSize.height +arc4random() % (int)(screenSize.height);
			
			enemy.position = ccp(j, k);
			enemy.collisionForce = CGPointZero;
			enemy.frictionalForce = CGPointZero;
			[staticEnemies addObject:enemy];
			[self addChild:enemy z:1];
		}		
		
        #pragma mark initialize Collectable items
        //Currently with cyan and green balls
        
		nonStaticCollectables = [[NSMutableArray alloc]init];
		CCTexture2D *collectableTextureNonStatic = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_green.png"];
		for (int i=0; i<NUMBER_OF_NON_STATIC_ENEMIES; i++) {
			CollectableItem *collectable = [CollectableItem spriteWithTexture:collectableTextureNonStatic];
			
			int j = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
			int k = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
			
			collectable.position = ccp(j, k);
			collectable.collisionForce = CGPointZero;
			collectable.frictionalForce = CGPointZero;
			collectable.nonStatic = YES;
            collectable.visible = NO;
			[nonStaticCollectables addObject:collectable];
			[self addChild:collectable z:1];
		}
        
        flyingCollectables = [[NSMutableArray alloc]init];
		CCTexture2D *collectableTextureFlying = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_cyan.png"];
		for (int i=0; i<NUMBER_OF_FLYING_ENEMIES; i++) {
			CollectableItem *collectable = [CollectableItem spriteWithTexture:collectableTextureFlying];
			
			int side = arc4random()%2;
			int j = arc4random() % (int)(screenSize.width*0.5);
			int k = (int) screenSize.height * 0.5 + arc4random() % (int)(screenSize.height*0.5);
			
			if (side == 0) {
				collectable.position = ccp(-j, k);
				collectable.velocity = ccp(FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
			else if (side == 1) {
				collectable.position = ccp(screenSize.width + j, k);
				collectable.velocity = ccp(-FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
			
			collectable.position = ccp(j, k);
			collectable.collisionForce = CGPointZero;
			collectable.frictionalForce = CGPointZero;
			collectable.flying = YES;
            collectable.visible = NO;
			[flyingCollectables addObject:collectable];
			[self addChild:collectable z:1];
		}
		
        #pragma mark initialize Collected items
        //Currently with small cyan and green balls. They rotate around the player after collected
        
		nonStaticWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *weaponTextureNonStatic = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_green_weapon.png"];
		for (int i = 0; i<MAX_NON_STATIC_WEAPONS; i++) {
			WeaponItem *nonStaticWeapon = [WeaponItem spriteWithTexture: weaponTextureNonStatic];
			nonStaticWeapon.active = NO;
			nonStaticWeapon.position = WEAPON_ITEM_RESET_POSITION;
			[nonStaticWeapons addObject:nonStaticWeapon];
			[self addChild:nonStaticWeapon];
		}
		
        flyingWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *weaponTextureFlying = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_cyan_weapon.png"];
		for (int i = 0; i<MAX_FLYING_WEAPONS; i++) {
			WeaponItem *flyingWeapon = [WeaponItem spriteWithTexture: weaponTextureFlying];
			flyingWeapon.active = NO;
			flyingWeapon.position = WEAPON_ITEM_RESET_POSITION;
			[flyingWeapons addObject:flyingWeapon];
			[self addChild:flyingWeapon z:1];
		}
        
        #pragma mark initialize Collected Weapon items
        //Currently with normal cyan and green balls. They are used to shoot the enemies.
        
		nonStaticCollectedWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *collectedWeaponTextureNonStatic = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_green.png"];
		for (int i = 0; i<MAX_NON_STATIC_WEAPONS; i++) {
			CollectedWeaponItem *nonStaticCollectedWeapon = [CollectedWeaponItem spriteWithTexture: collectedWeaponTextureNonStatic];
			nonStaticCollectedWeapon.velocity = CGPointZero;
			nonStaticCollectedWeapon.acceleration = CGPointZero;			
			nonStaticCollectedWeapon.collisionForce = CGPointZero;
			nonStaticCollectedWeapon.frictionalForce = CGPointZero;
			nonStaticCollectedWeapon.nonStatic = YES;
			nonStaticCollectedWeapon.active = NO;
			nonStaticCollectedWeapon.visible = NO;
			nonStaticCollectedWeapon.position = player.position;
			[nonStaticCollectedWeapons addObject:nonStaticCollectedWeapon];
			[self addChild:nonStaticCollectedWeapon];
		}
		
		flyingCollectedWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *collectedWeaponTextureFlying = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_cyan.png"];
		for (int i = 0; i<MAX_FLYING_WEAPONS; i++) {
			CollectedWeaponItem *flyingCollectedWeapon = [CollectedWeaponItem spriteWithTexture: collectedWeaponTextureFlying];
			flyingCollectedWeapon.velocity = CGPointZero;
			flyingCollectedWeapon.acceleration = CGPointZero;			
			flyingCollectedWeapon.collisionForce = CGPointZero;
			flyingCollectedWeapon.frictionalForce = CGPointZero;
			flyingCollectedWeapon.flying = YES;
			flyingCollectedWeapon.active = NO;
			flyingCollectedWeapon.visible = NO;
			flyingCollectedWeapon.position = player.position;
			[flyingCollectedWeapons addObject:flyingCollectedWeapon];
			[self addChild:flyingCollectedWeapon z:1];
		}
		*/	
        
//#pragma mark semi transparent layer
//        CCTexture2D *texture_blurLayer = [[CCTextureCache sharedTextureCache] addImage: @"layer_semi_transparent.png"];
//        CCSprite *blurLayer1 = [CCSprite spriteWithTexture:texture_blurLayer];
//		blurLayer1.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
//		[self addChild:blurLayer1 z:8];

//        CCSprite *blurLayer2 = [CCSprite spriteWithTexture:texture_blurLayer];
//		blurLayer2.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
//		[self addChild:blurLayer2 z:6];
//
//        CCSprite *blurLayer3 = [CCSprite spriteWithTexture:texture_blurLayer];
//		blurLayer3.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
//		[self addChild:blurLayer3 z:4];
//
//        CCSprite *blurLayer4 = [CCSprite spriteWithTexture:texture_blurLayer];
//		blurLayer4.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
//		[self addChild:blurLayer4 z:2];
        
#pragma mark initialize Rock items
        //Currently with Gray and Red bars. They block the player movement as obstacles
        
		rocks = [[NSMutableArray alloc] init];
		for (int j=0; j<NUMBER_OF_ROCKS; j++) {		
            BlockItem *block;
            
            if (retinaDisplay) {
                NSString *plistFileName = [NSString stringWithFormat:@"rock_horizontal_rotation-hd.plist"];
                NSString *imageFileName = [NSString stringWithFormat:@"rock_horizontal_rotation-hd.png"];
                [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: plistFileName];
                CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
                [self addChild:spriteBatch z:9];
                NSMutableArray *rotateAnimFrames = [NSMutableArray array];
                for(int i = 1; i <= 30; ++i) {
                    if (i<10) {
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image0000%d.png", i]]];
                    }
                    else{
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image000%d.png", i]]];
                    }
                    rotateAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.1f];
                }
                
                block = [BlockItem spriteWithSpriteFrameName:@"Image00001.png"];
                block.willKill = NO;
                CCAction *rotateAction = [CCRepeatForever actionWithAction:
                                          [CCAnimate actionWithAnimation:rotateAnim restoreOriginalFrame:NO]];
                [block runAction:rotateAction];
                [spriteBatch addChild:block];
            }
            else{
                
                NSString *plistFileName = [NSString stringWithFormat:@"rock_horizontal_rotation.plist", j+1];
                NSString *imageFileName = [NSString stringWithFormat:@"rock_horizontal_rotation.png", j+1];
                
                [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:plistFileName];
                CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
                [self addChild:spriteBatch z:9];
                NSMutableArray *rotateAnimFrames = [NSMutableArray array];
                for(int i = 1; i <= 30; ++i) {
                    if (i<10) {
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image0000%d.png", i]]];
                    }
                    else{
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image000%d.png", i]]];
                    }
                    rotateAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.1f];
                }
                
                block = [BlockItem spriteWithSpriteFrameName:@"Image00001.png"];
                block.willKill = NO;
                CCAction *rotateAction = [CCRepeatForever actionWithAction:
                                          [CCAnimate actionWithAnimation:rotateAnim restoreOriginalFrame:NO]];
                [block runAction:rotateAction];
                [spriteBatch addChild:block];
            }	
    
			[block resetBlockPosition];
            [rocks addObject:block];
		}
		[gamePlayMethods checkAndActivateBlock: rocks];	
        
#pragma mark initialize background Rock items in layer 1
        //Currently with Gray and Red bars. They block the player movement as obstacles
        
		rocksBackgroundLayer1 = [[NSMutableArray alloc] init];
		for (int j=0; j<NUMBER_OF_ROCKS; j++) {		
            BlockItem *block;
            
            if (retinaDisplay) {
                NSString *plistFileName = [NSString stringWithFormat:@"rock_horizontal_rotation-hd.plist"];
                NSString *imageFileName = [NSString stringWithFormat:@"rock_horizontal_rotation-hd.png"];
                [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: plistFileName];
                CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
                //                [self addChild:spriteBatch z:7];
                [self addChild:spriteBatch z:3];
                NSMutableArray *rotateAnimFrames = [NSMutableArray array];
                for(int i = 1; i <= 30; ++i) {
                    if (i<10) {
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image0000%d.png", i]]];
                    }
                    else{
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image000%d.png", i]]];
                    }
                    rotateAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.1f];
                }
                
                block = [BlockItem spriteWithSpriteFrameName:@"Image00001.png"];
                block.willKill = NO;
                CCAction *rotateAction = [CCRepeatForever actionWithAction:
                                          [CCAnimate actionWithAnimation:rotateAnim restoreOriginalFrame:NO]];
                int randomScaleFactor = 10 + arc4random() %  40; 
                [block setScale:randomScaleFactor * 0.01];
                [block runAction:rotateAction];
//                int depthOfSprites = 4+arc4random()%7;
//                [spriteBatch addChild:block z:depthOfSprites];
                [spriteBatch addChild:block];                
            }
            else{
                
                NSString *plistFileName = [NSString stringWithFormat:@"rock_horizontal_rotation.plist", j+1];
                NSString *imageFileName = [NSString stringWithFormat:@"rock_horizontal_rotation.png", j+1];
                
                [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:plistFileName];
                CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
                //                [self addChild:spriteBatch z:7];
                [self addChild:spriteBatch z:3];
                NSMutableArray *rotateAnimFrames = [NSMutableArray array];
                for(int i = 1; i <= 30; ++i) {
                    if (i<10) {
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image0000%d.png", i]]];
                    }
                    else{
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image000%d.png", i]]];
                    }
                    rotateAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.1f];
                }
                
                block = [BlockItem spriteWithSpriteFrameName:@"Image00001.png"];
                block.willKill = NO;
                CCAction *rotateAction = [CCRepeatForever actionWithAction:
                                          [CCAnimate actionWithAnimation:rotateAnim restoreOriginalFrame:NO]];
                int randomScaleFactor = 10 + arc4random() %  40; 
                [block setScale:randomScaleFactor * 0.01];
                [block runAction:rotateAction];
//                int depthOfSprites = 4+arc4random()%7;
//                [spriteBatch addChild:block z:depthOfSprites];
                [spriteBatch addChild:block];                
            }	
            
			[block resetBlockPosition];
			[rocksBackgroundLayer1 addObject:block];
		}
		[gamePlayMethods checkAndActivateBlocksInBackgroundLayer1: rocksBackgroundLayer1];
        
    #pragma mark initialize Burning Rock items
        //Currently with Gray and Red bars. They block the player movement as obstacles
        
		burningRocks = [[NSMutableArray alloc] init];
		for (int j=0; j<NUMBER_OF_BURNING_ROCKS; j++) {		
            BlockItem *block;
            
            if (retinaDisplay) {
//                NSString *plistFileName = [NSString stringWithFormat:@"rock%d_horizontal_rotation-hd.plist", j+1+20];
//                NSString *imageFileName = [NSString stringWithFormat:@"rock%d_horizontal_rotation-hd.png", j+1+20];
                NSString *plistFileName = [NSString stringWithFormat:@"burning_rock_horizontal_rotation-hd.plist"];
                NSString *imageFileName = [NSString stringWithFormat:@"burning_rock_horizontal_rotation-hd.png"];
                [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: plistFileName];
                CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
                [self addChild:spriteBatch z:9];
                NSMutableArray *rotateAnimFrames = [NSMutableArray array];
                for(int i = 1; i <= 30; ++i) {
                    if (i<10) {
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image0000%d.png", i]]];
                    }
                    else{
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image000%d.png", i]]];
                    }
                    rotateAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.1f];
                }
                
                block = [BlockItem spriteWithSpriteFrameName:@"Image00001.png"];
                //block.willKill = YES;
                block.willKill = NO;
                CCAction *rotateAction = [CCRepeatForever actionWithAction:
                                   [CCAnimate actionWithAnimation:rotateAnim restoreOriginalFrame:NO]];
                [block runAction:rotateAction];
                [spriteBatch addChild:block];
            }
            else{
                
//                NSString *plistFileName = [NSString stringWithFormat:@"rock%d_horizontal_rotation.plist", j+1+20];
//                NSString *imageFileName = [NSString stringWithFormat:@"rock%d_horizontal_rotation.png", j+1+20];
                NSString *plistFileName = [NSString stringWithFormat:@"burning_rock_horizontal_rotation.plist"];
                NSString *imageFileName = [NSString stringWithFormat:@"burning_rock_horizontal_rotation.png"];
                [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:plistFileName];
                CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
                [self addChild:spriteBatch z:9];
                NSMutableArray *rotateAnimFrames = [NSMutableArray array];
                for(int i = 1; i <= 30; ++i) {
                    if (i<10) {
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image0000%d.png", i]]];
                    }
                    else{
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image000%d.png", i]]];
                    }
                    rotateAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.1f];
                }
                
                block = [BlockItem spriteWithSpriteFrameName:@"Image00001.png"];
                block.willKill = YES;
                CCAction *rotateAction = [CCRepeatForever actionWithAction:
                              [CCAnimate actionWithAnimation:rotateAnim restoreOriginalFrame:NO]];
                [block runAction:rotateAction];
                [spriteBatch addChild:block];
            }

			[block resetBlockPosition];
			[burningRocks addObject:block];
		}
		[gamePlayMethods checkAndActivateBlock: burningRocks];	
        
    #pragma mark initialize background Burning Rock items in layer 1
        //Currently with Gray and Red bars. They block the player movement as obstacles
        
		burningRocksBackgroundLayer1 = [[NSMutableArray alloc] init];
		for (int j=0; j<NUMBER_OF_BURNING_ROCKS; j++) {		
            BlockItem *block;
            
            if (retinaDisplay) {
//                NSString *plistFileName = [NSString stringWithFormat:@"rock%d_horizontal_rotation-hd.plist", j+1+20];
//                NSString *imageFileName = [NSString stringWithFormat:@"rock%d_horizontal_rotation-hd.png", j+1+20];
                NSString *plistFileName = [NSString stringWithFormat:@"burning_rock_horizontal_rotation-hd.plist"];
                NSString *imageFileName = [NSString stringWithFormat:@"burning_rock_horizontal_rotation-hd.png"];
                [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: plistFileName];
                CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
                [self addChild:spriteBatch z:3];
                NSMutableArray *rotateAnimFrames = [NSMutableArray array];
                for(int i = 1; i <= 30; ++i) {
                    if (i<10) {
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image0000%d.png", i]]];
                    }
                    else{
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image000%d.png", i]]];
                    }
                    rotateAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.1f];
                }
                
                block = [BlockItem spriteWithSpriteFrameName:@"Image00001.png"];
                block.willKill = NO;
                CCAction *rotateAction = [CCRepeatForever actionWithAction:
                              [CCAnimate actionWithAnimation:rotateAnim restoreOriginalFrame:NO]];
                int randomScaleFactor = 10 + arc4random() %  40; 
                [block setScale:randomScaleFactor * 0.01];
                [block runAction:rotateAction];
//                int depthOfSprites = 4+arc4random()%7;
//                [spriteBatch addChild:block z:depthOfSprites];
                [spriteBatch addChild:block];
            }
            else{
                
//                NSString *plistFileName = [NSString stringWithFormat:@"rock%d_horizontal_rotation.plist", j+1+20];
//                NSString *imageFileName = [NSString stringWithFormat:@"rock%d_horizontal_rotation.png", j+1+20];
                NSString *plistFileName = [NSString stringWithFormat:@"burning_rock_horizontal_rotation.plist"];
                NSString *imageFileName = [NSString stringWithFormat:@"burning_rock_horizontal_rotation.png"];
                [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:plistFileName];
                CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
                [self addChild:spriteBatch z:3];
                NSMutableArray *rotateAnimFrames = [NSMutableArray array];
                for(int i = 1; i <= 30; ++i) {
                    if (i<10) {
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image0000%d.png", i]]];
                    }
                    else{
                        [rotateAnimFrames addObject:
                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                          [NSString stringWithFormat:@"Image000%d.png", i]]];
                    }
                    rotateAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.1f];
                }
                
                block = [BlockItem spriteWithSpriteFrameName:@"Image00001.png"];
                block.willKill = NO;
                CCAction *rotateAction = [CCRepeatForever actionWithAction:
                              [CCAnimate actionWithAnimation:rotateAnim restoreOriginalFrame:NO]];
                int randomScaleFactor = 10 + arc4random() %  40; 
                [block setScale:randomScaleFactor * 0.01];
                [block runAction:rotateAction];
//                int depthOfSprites = 4+arc4random()%7;
                [spriteBatch addChild:block];
//                [spriteBatch addChild:block z:depthOfSprites];
            }	
            
			[block resetBlockPosition];
			[burningRocksBackgroundLayer1 addObject:block];
		}
		[gamePlayMethods checkAndActivateBlocksInBackgroundLayer1: burningRocksBackgroundLayer1];
        
//        #pragma mark initialize Chasing Enemy items
//        //Currently with White balls. They chase the player.
//        
//		chasingEnemiesPlacementGrid = [[NSMutableArray alloc] init];
//		for (int j=1; j<=NUM_HOR_GRIDS_VERTICALLY; j++) {
//			for (int i=0; i<NUM_OF_UNIT_GRIDS_HORZONTALLY; i++) {
//				
//				GridItem *enemyPlacementGrid = [[[GridItem alloc] init] autorelease];
//				enemyPlacementGrid.gridRect = CGRectMake((i)*UNIT_GRID_WIDTH, -(j)*UNIT_GRID_HEIGHT,
//														 UNIT_GRID_WIDTH, UNIT_GRID_HEIGHT);
//				enemyPlacementGrid.isFree = YES;
//				[chasingEnemiesPlacementGrid addObject:enemyPlacementGrid];
//				//CCLOG(@"gridRect.origin:(%f, %f)", enemyPlacementGrid.gridRect.origin.x, enemyPlacementGrid.gridRect.origin.y);
//			}
//		}
//		
//		chasingEnemies = [[NSMutableArray alloc]init];
////		CCTexture2D *chasingEnemyTexture = [[CCTextureCache sharedTextureCache] addImage: @"Soccer_Ball.png"];
//		for (int i=0; i<NUMBER_OF_CHASING_ENEMIES; i++) {
//			ChaserItem *chasingEnemy;
//            if (retinaDisplay) {
//                NSString *plistFileName = [NSString stringWithFormat:@"blue_skeleton-hd.plist"];
//                NSString *imageFileName = [NSString stringWithFormat:@"blue_skeleton-hd.png"];
//                [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: plistFileName];
//                CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
//                [self addChild:spriteBatch z:9];
//                NSMutableArray *skeletonAnimFrames = [NSMutableArray array];
//                for(int i = 1; i <= 10; ++i) {
//                    if (i<10) {
//                        [skeletonAnimFrames addObject:
//                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
//                          [NSString stringWithFormat:@"Image0000%d.png", i]]];
//                    }
//                    else{
//                        [skeletonAnimFrames addObject:
//                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
//                          [NSString stringWithFormat:@"Image000%d.png", i]]];
//                    }
//                    skeletonAnim = [CCAnimation animationWithFrames:skeletonAnimFrames delay:0.1f];
//                }
//                
//                chasingEnemy = [ChaserItem spriteWithSpriteFrameName:@"Image00001.png"];
//                CCAction *skeletonAction = [CCRepeatForever actionWithAction:
//                                            [CCAnimate actionWithAnimation:skeletonAnim restoreOriginalFrame:NO]];
//                [chasingEnemy runAction:skeletonAction];
//                [spriteBatch addChild:chasingEnemy];
//            }
//            else{
//                NSString *plistFileName = [NSString stringWithFormat:@"blue_skeleton.plist"];
//                NSString *imageFileName = [NSString stringWithFormat:@"blue_skeleton.png"];
//                [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: plistFileName];
//                CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:imageFileName];
//                [self addChild:spriteBatch z:9];
//                NSMutableArray *skeletonAnimFrames = [NSMutableArray array];
//                for(int i = 1; i <= 10; ++i) {
//                    if (i<10) {
//                        [skeletonAnimFrames addObject:
//                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
//                          [NSString stringWithFormat:@"Image0000%d.png", i]]];
//                    }
//                    else{
//                        [skeletonAnimFrames addObject:
//                         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
//                          [NSString stringWithFormat:@"Image000%d.png", i]]];
//                    }
//                    skeletonAnim = [CCAnimation animationWithFrames:skeletonAnimFrames delay:0.1f];
//                }
//                
//                chasingEnemy = [ChaserItem spriteWithSpriteFrameName:@"Image00001.png"];
//                CCAction *skeletonAction = [CCRepeatForever actionWithAction:
//                                            [CCAnimate actionWithAnimation:skeletonAnim restoreOriginalFrame:NO]];
//                [chasingEnemy runAction:skeletonAction];
//                [spriteBatch addChild:chasingEnemy];
//            }
//			[chasingEnemy makeChaserEnemyInactive];
//			[chasingEnemies addObject:chasingEnemy];
//		}
//		
//		[gamePlayMethods placeChasingEnemiesInTheGrid:chasingEnemiesPlacementGrid :chasingEnemies];
		//		*/		
		[self scheduleUpdate];
		playerTouched = NO;
		screenTouched = NO;
		accumulatedTime = 0;
		totalTime = 0;
        
    }
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	deltaTime = delta;
	
	previousPlayerPosition = player.position;
	[gamePlayMethods updateScore:player :sharedGlobalSingletonVarsInstance];
	[scoreLabel setString: [gamePlayMethods displayScore:sharedGlobalSingletonVarsInstance]];
    [gamePlayMethods updateBarLength:player :sharedGlobalSingletonVarsInstance];
	
	[player powerUpForces];
	[player playerFrictionalForces];
	[player limitPlayerBoundaries];
	[player playerFrictionalForcesEnemyCollisionMovement];
	[player updateDuringMainGamePlay];
	[player updateAccelerometerInput: acceX];	
	[player maintainPlayerAtMinHeight: deltaTime];  // should be before player block collision to bring back to min height
    
    for (int i = 0; i<NUM_ASHES; i++) {
        AshItem *ash = [ashes objectAtIndex:i];
        if (accumulatedTime < 1) {
            accumulatedTime = accumulatedTime + delta;
            [ash update];
        }
        else if(accumulatedTime >= 1){
            accumulatedTime = 0;
            [ash updateAll];
        }
    }
    
    for (int i=0; i<flyingMountains1.count; i++) {
        FlyingMountainItem *flyingMountain = [flyingMountains1 objectAtIndex:i];
        [flyingMountain update];
    }

    for (int i=0; i<flyingMountains2.count; i++) {
        FlyingMountainItem *flyingMountain = [flyingMountains2 objectAtIndex:i];
        [flyingMountain update];
    }
    
	[gamePlayMethods updateFuelItems:player :fuelItems1 :staticEnemies: sharedGlobalSingletonVarsInstance];
	if ([gamePlayMethods checkInactivePattern1:fuelItems1]) {
		[gamePlayMethods placeFuelItemsPattern1: fuelItems1];
	}

	for (int i =0; i<rocks.count; i++) {
		BlockItem *block = [rocks objectAtIndex:i];
		[block updateBlockPosition];
		[gamePlayMethods playerMovementPlayerBlockCollision: player :block];
		[block checkCollectedWeaponsBlockCollision:nonStaticCollectedWeapons];
		[block checkCollectedWeaponsBlockCollision:flyingCollectedWeapons];
		[block checkEnemyItemBlockCollision:staticEnemies];
		[block checkChaserEnemyItemBlockCollision: chasingEnemies];
	}
	[gamePlayMethods checkAndActivateBlock: rocks];
    
    for (int i =0; i<rocksBackgroundLayer1.count; i++) {
		BlockItem *block = [rocksBackgroundLayer1 objectAtIndex:i];
		[block updateBlockPosition];
	}
    [gamePlayMethods checkAndActivateBlocksInBackgroundLayer1: rocksBackgroundLayer1];	

    
	for (int i =0; i<burningRocks.count; i++) {
		BlockItem *block = [burningRocks objectAtIndex:i];
		[block updateBlockPosition];
		[gamePlayMethods playerMovementPlayerBlockCollision: player :block]; 
		[block checkCollectedWeaponsBlockCollision:nonStaticCollectedWeapons];
		[block checkCollectedWeaponsBlockCollision:flyingCollectedWeapons];
		[block checkEnemyItemBlockCollision:staticEnemies];
		[block checkChaserEnemyItemBlockCollision: chasingEnemies];
	}
	[gamePlayMethods checkAndActivateBlock: burningRocks];
    
    for (int i =0; i<burningRocksBackgroundLayer1.count; i++) {
		BlockItem *block = [burningRocksBackgroundLayer1 objectAtIndex:i];
		[block updateBlockPosition];
	}
    [gamePlayMethods checkAndActivateBlocksInBackgroundLayer1: burningRocksBackgroundLayer1];	
    
    if (player.killed == YES) {
        [self endGamePlay];
    }
	
	/*
	for (int i=0; i<staticEnemies.count; i++) {
		EnemyItem *enemy = [staticEnemies objectAtIndex:i];
		
		[enemy updateEnemiesPosition: player.velocity];
		[gamePlayMethods playerMovementPlayerEnemiesCollision: player :enemy];
		[enemy staticEnemyFrictionalForce];
	}
	
	for (int i=0; i<nonStaticCollectables.count; i++) {
		CollectableItem *nonStaticCollectable = [nonStaticCollectables objectAtIndex:i];
		
		[nonStaticCollectable updateCollectablesPosition: player.velocity];
		[gamePlayMethods moveCollectablesTowardsPlayer: player :nonStaticCollectable];
		[gamePlayMethods checkPlayerTakesCollectable: player :nonStaticCollectable :nonStaticWeapons :flyingWeapons];
	}
	
	for(int i=0; i<nonStaticWeapons.count; i++) {
		WeaponItem *nonStaticWeapon = [nonStaticWeapons objectAtIndex:i];
		
		[nonStaticWeapon rotateWeapons: player.position :previousPlayerPosition];
	}
	
	for(int i=0; i<nonStaticCollectedWeapons.count; i++) {
		CollectedWeaponItem *nonStaticCollectedWeapon = [nonStaticCollectedWeapons objectAtIndex:i];
		[nonStaticCollectedWeapon update];
		[nonStaticCollectedWeapon holdUnactivatedCollectedWeapon: player.position];
		[nonStaticCollectedWeapon checkCollectedWeaponBoundaries];
		[nonStaticCollectedWeapon enemyMovementCollectableWeaponCollision: staticEnemies];
	}
	
	for (int i=0; i<flyingCollectables.count; i++) {
		CollectableItem *flyingCollectable = [flyingCollectables objectAtIndex:i];
		
		[flyingCollectable updateCollectablesPosition: player.velocity];
		[gamePlayMethods moveCollectablesTowardsPlayer: player :flyingCollectable];
		[gamePlayMethods checkPlayerTakesCollectable: player :flyingCollectable :nonStaticWeapons :flyingWeapons];
	}
	
	for(int i=0; i<flyingWeapons.count; i++) {
		WeaponItem *flyingWeapon = [flyingWeapons objectAtIndex:i];
		
		[flyingWeapon rotateWeapons: player.position :previousPlayerPosition];
	}
	
	for(int i=0; i<flyingCollectedWeapons.count; i++) {
		CollectedWeaponItem *flyingCollectedWeapon = [flyingCollectedWeapons objectAtIndex:i];
		[flyingCollectedWeapon update];
		[flyingCollectedWeapon holdUnactivatedCollectedWeapon: player.position];
		[flyingCollectedWeapon checkCollectedWeaponBoundaries];
		[flyingCollectedWeapon enemyMovementCollectableWeaponCollision: staticEnemies];
	}
	
	[gamePlayMethods showSelectedWeapons: player :nonStaticWeapons :flyingWeapons];
	[player changePlayerWeaponDefault];
	
    */
    
//	[gamePlayMethods placeChasingEnemiesInTheGrid: chasingEnemiesPlacementGrid :chasingEnemies];
//	for (int i=0; i<chasingEnemies.count; i++) {
//		ChaserItem *chasingEnemy = [chasingEnemies objectAtIndex:i];
//		
//		[chasingEnemy updateChasingEnemyMovement: player.velocity];
//		[chasingEnemy moveChasingEnemiesTowardsPlayer: player];
//		[chasingEnemy vibrateChasingEnemies: deltaTime];
//		[chasingEnemy chasingEnemyBackingMovement: player :deltaTime];
//	}
    
//    if ([gamePlayMethods checkChasingEnemiesNearPlayer:player :chasingEnemies]) {
//        [self startShortGamePlay];
//    }
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint unconvertedLocation = [touch locationInView: [touch view]];
	CGPoint location = [[CCDirector sharedDirector] convertToGL: unconvertedLocation];
	
	touchedLocation = location;
	
	screenTouched = YES;
	return YES;
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint unconvertedLocation = [touch locationInView: [touch view]];
	CGPoint location = [[CCDirector sharedDirector] convertToGL: unconvertedLocation];
	
	touchedLocation = location;
	
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	
	screenTouched = NO;
	
	CGPoint unconvertedLocation = [touch locationInView: [touch view]];
	CGPoint location = [[CCDirector sharedDirector] convertToGL: unconvertedLocation];
	
	if (![player touchedPlayer:location]) {
		[gamePlayMethods activateNonStaticCollectedWeapons: player :nonStaticCollectedWeapons: location];
		[gamePlayMethods deactivateNonStaticWeaponsForRotationAroundPlayer: player :nonStaticWeapons];
		[gamePlayMethods activateFlyingCollectedWeapons: player :flyingCollectedWeapons: location];
		[gamePlayMethods deactivateFlyingWeaponsForRotationAroundPlayer: player :flyingWeapons];
	}
	else if([player touchedPlayer:location]){
		[player changePlayerSelectedWeapon];
	}
	
}

-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	acceX = 0;
	if(acceleration.x > ACCELEROMETER_MIN_VALUE)
		acceX = 1;
	else if(acceleration.x < -ACCELEROMETER_MIN_VALUE)
		acceX = -1;
}

-(void) pauseGame: (id) sender {
	if ([[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] resume];
		[pauseButton setString:@"  pause"];
	}
	else if (![[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] pause];
		[pauseButton setString:@"resume"];
	}
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) startShortGamePlay {    
    [[CCDirector sharedDirector] replaceScene: [GamePlayShortScene scene]];
}

-(void) endGamePlay {
	[[CCDirector sharedDirector] replaceScene: [GameOverScene scene]];
}

-(void) draw {
	[super draw];
	
	glEnable(GL_LINE_SMOOTH);
	glColor4ub(125, 100, 100, 255);  
    glLineWidth(1.0f);
	
	if (screenTouched) {
		//		CCLOG(@"player.position: (%f, %f), touchedLocation: (%f, %f)", 
		//			  player.position.x, player.position.y, touchedLocation.x, touchedLocation.y);
		ccDrawLine(player.position, touchedLocation);
	}
	
	glEnable(GL_LINE_SMOOTH);
	glColor4ub(100, 100, 100, 255);  
    glLineWidth(3.0f);
	
	ccDrawLine(ccp(0, screenSize.height*0.95), ccp(player.barLength, screenSize.height*0.95));
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
    
	[gamePlayMethods release];
	[fuelItems1 release];
	[staticEnemies release];
	[chasingEnemies release];
	[nonStaticCollectables release];
	[nonStaticWeapons release];
	[nonStaticCollectedWeapons release];
	[flyingCollectables release];
	[flyingWeapons release];
	[flyingCollectedWeapons release];
	[rocks release];
    [rocksBackgroundLayer1 release];
	[burningRocks release];
    [burningRocksBackgroundLayer1 release];
	[chasingEnemiesPlacementGrid release];
    [flyingMountains1 release];
    [flyingMountains2 release];
    [ashes release];
	[super dealloc];
}

@end
