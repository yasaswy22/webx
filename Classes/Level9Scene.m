//
//  Level9Scene.m
//  WebX
//
//  Created by Sunny's Mac on 1/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Level9Scene.h"
#import "GlobalVars.h"
#import "MainMenuScene.h"
#import "WeaponItem.h"
#import "CollectableItem.h"
#import "CollectedWeaponItem.h"

@implementation Level9Scene

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* level9Scene = [CCScene node];
	Level9Scene* level9Layer = [Level9Scene node];
	[level9Scene addChild:level9Layer];
	return level9Scene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		screenSize = [[CCDirector sharedDirector] winSize];
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		self.isAccelerometerEnabled=YES;
		
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:15];
		
		CCMenuItemFont *exitButton = [CCMenuItemFont itemFromString:@"exit" target:self selector:@selector(returnMainMenu:)];
		exitButton.color = ccc3(TEXT_COLOR);
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5, 
								  screenSize.height - exitButton.contentSize.height * 0.5);
		
		pauseButton = [CCMenuItemFont itemFromString:@"  pause" target:self selector:@selector(pauseGame:)];
		pauseButton.color = ccc3(TEXT_COLOR);
		pauseButton.position = ccp(screenSize.width - pauseButton.contentSize.width * 0.5, 
								   pauseButton.contentSize.height * 0.5);
		
		CCMenu* menu = [CCMenu menuWithItems:exitButton, pauseButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu];
		
		player = [PlayerItem spriteWithFile:@"Volleyball_Ball.png"];
		player.position = ccp(screenSize.width*0.5, MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		player.velocity = CGPointZero;
		player.numNonStaticCollectables = 0;
		player.acceleration = CGPointZero;
		player.decelaration = CGPointZero;
		player.velocity = ccp(0, PLAYER_INITIAL_VELOCITY);
		player.acceleration = ccp(0, PLAYER_INITIAL_ACCELERATION);
		[self addChild:player z:2];
		
		fuelTank = [FuelItem spriteWithFile:@"fuel_tank.png"];
		int i = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
		int j = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
		fuelTank.position = ccp(i, j);
		[self addChild:fuelTank z:2];
		
		nonStaticCollectables = [[NSMutableArray alloc]init];
		CCTexture2D *collectableTextureNonStatic = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_green.png"];
		for (int i=0; i<NUMBER_OF_NON_STATIC_ENEMIES; i++) {
			CollectableItem *collectable = [CollectableItem spriteWithTexture:collectableTextureNonStatic];
			
			int j = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
			int k = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
			
			collectable.position = ccp(j, k);
			collectable.collisionForce = CGPointZero;
			collectable.frictionalForce = CGPointZero;
			collectable.nonStatic = YES;
			[nonStaticCollectables addObject:collectable];
			[self addChild:collectable z:1];
		}
		
		nonStaticWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *weaponTextureNonStatic = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_green.png"];
		for (int i = 0; i<MAX_NON_STATIC_WEAPONS; i++) {
			WeaponItem *nonStaticWeapon = [WeaponItem spriteWithTexture: weaponTextureNonStatic];
			nonStaticWeapon.visible = NO;
			nonStaticWeapon.active = NO;
			nonStaticWeapon.position = ccp(player.position.x, player.position.y);
			[nonStaticWeapons addObject:nonStaticWeapon];
			[self addChild:nonStaticWeapon];
		}
		
		nonStaticCollectedWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *collectedWeaponTextureNonStatic = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_green.png"];
		for (int i = 0; i<MAX_NON_STATIC_WEAPONS; i++) {
			CollectedWeaponItem *nonStaticCollectedWeapon = [CollectedWeaponItem spriteWithTexture: collectedWeaponTextureNonStatic];
			nonStaticCollectedWeapon.velocity = CGPointZero;
			nonStaticCollectedWeapon.acceleration = CGPointZero;			
			nonStaticCollectedWeapon.collisionForce = CGPointZero;
			nonStaticCollectedWeapon.frictionalForce = CGPointZero;
			nonStaticCollectedWeapon.nonStatic = YES;
			nonStaticCollectedWeapon.active = NO;
			nonStaticCollectedWeapon.visible = NO;
			nonStaticCollectedWeapon.position = ccp(player.position.x, player.position.y);
			[nonStaticCollectedWeapons addObject:nonStaticCollectedWeapon];
			[self addChild:nonStaticCollectedWeapon];
		}
		
		[self scheduleUpdate];
		playerTouched = NO;
	}
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	deltaTime = delta;
	
	[player powerUpForces];
	[player playerBoostMovement: deltaTime];
	[player playerFrictionalForces];
	[fuelTank updateFuelTankPosition: player.velocity];
	[player checkPlayerTakesFuel: fuelTank];
	[player limitPlayerBoundaries];
	
	for (int i=0; i<nonStaticCollectables.count; i++) {
		CollectableItem *nonStaticCollectable = [nonStaticCollectables objectAtIndex:i];
		
		[nonStaticCollectable updateCollectablesPosition: player.velocity];
		[player checkPlayerTakesCollectable: nonStaticCollectable];
	}
	
	for(int i=0; i<MAX_NON_STATIC_WEAPONS; i++) {
		WeaponItem *nonStaticWeapon = [nonStaticWeapons objectAtIndex:i];
		
		[nonStaticWeapon weaponsPositionPlayerBoostMovement: player.velocity: player.playerBoost :deltaTime];
		[nonStaticWeapon weaponsPositionAccelerometerInputIntoAccount: player.velocity: acceX];
		[nonStaticWeapon rotateWeapons: player.position];
		[nonStaticWeapon positionNonActiveWeapons: player.position];
	}
	[self activateWeaponsForRotationAroundPlayer];
	[self shootCollectedWeapon];
	[self deactivateCollectedWeapon];
	[player update];
	[player updateAccelerometerInput: acceX];
}

-(void) shootCollectedWeapon {
	
	for(int i=0; i<MAX_NON_STATIC_WEAPONS; i++) {
		
		CollectedWeaponItem *nonStaticCollectedWeapon = [nonStaticCollectedWeapons objectAtIndex:i];
		
		if (nonStaticCollectedWeapon.active) {
			nonStaticCollectedWeapon.position = ccp(nonStaticCollectedWeapon.position.x, 
													nonStaticCollectedWeapon.position.y - player.velocity.y);
		}
		else {
			nonStaticCollectedWeapon.position = player.position;
		}
	}
}

-(void) activateCollectedWeapon {
	for(int i=0; i<MAX_NON_STATIC_WEAPONS; i++) {
		CollectedWeaponItem *nonStaticCollectedWeapon = [nonStaticCollectedWeapons objectAtIndex: i];
		if (!nonStaticCollectedWeapon.active && player.numNonStaticCollectables > 0) {
			nonStaticCollectedWeapon.active = YES;
			nonStaticCollectedWeapon.visible = YES;
		}
	}
}

-(void) deactivateCollectedWeapon {
	
	for(int i=0; i<MAX_NON_STATIC_WEAPONS; i++) {
		
		CollectedWeaponItem *nonStaticCollectedWeapon = [nonStaticCollectedWeapons objectAtIndex:i];
		
		if (nonStaticCollectedWeapon.position.y < -nonStaticCollectedWeapon.contentSize.height*0.5 ||
			nonStaticCollectedWeapon.position.y > screenSize.height-nonStaticCollectedWeapon.contentSize.height*0.5 ||
			nonStaticCollectedWeapon.position.x < -nonStaticCollectedWeapon.contentSize.width*0.5 ||
			nonStaticCollectedWeapon.position.x > screenSize.width-nonStaticCollectedWeapon.contentSize.width*0.5) {
			
			nonStaticCollectedWeapon.active = NO;
			nonStaticCollectedWeapon.visible = NO;
		}
	}
}

-(void) activateWeaponsForRotationAroundPlayer {
	
	if (player.numNonStaticCollectables == 1) {
		WeaponItem *nonStaticWeapon = [nonStaticWeapons objectAtIndex:0];
		if (CGPointEqualToPoint(player.position, nonStaticWeapon.position)) {
			nonStaticWeapon.position = ccp(player.position.x+WEAPON_MAGNITUDE_FROM_PLAYER, player.position.y);
		}
		nonStaticWeapon.active = YES;
		nonStaticWeapon.visible = YES;
	}
	else if (player.numNonStaticCollectables > 1){
		
		WeaponItem *nonStaticWeapon = [nonStaticWeapons objectAtIndex:0];
		CGPoint firstNonStaticWeaponPosition = nonStaticWeapon.position;
		CGPoint newRotatedPoint;
		CGFloat weaponAngleToPlayerRadians;
		
		for (int i=1; i<player.numNonStaticCollectables; i++) {
			nonStaticWeapon = [nonStaticWeapons objectAtIndex:i];
			CGFloat degreesOfSeperation = 360.0/(CGFloat)player.numNonStaticCollectables;
			newRotatedPoint = ccpRotateByAngle(firstNonStaticWeaponPosition, player.position, CC_DEGREES_TO_RADIANS(degreesOfSeperation*i));
			weaponAngleToPlayerRadians = ccpToAngle(ccpSub(newRotatedPoint, player.position));
			nonStaticWeapon.position = ccp(player.position.x+WEAPON_MAGNITUDE_FROM_PLAYER*cos(weaponAngleToPlayerRadians), 
										   player.position.y+WEAPON_MAGNITUDE_FROM_PLAYER*sin(weaponAngleToPlayerRadians));
			nonStaticWeapon.visible = YES;
			nonStaticWeapon.active = YES;
		}
	}
}

-(void) deactivateWeaponsForRotationAroundPlayer {
	
	if (player.numNonStaticCollectables > 0) {
		player.numNonStaticCollectables --;
		for (int i=player.numNonStaticCollectables; i<MAX_NON_STATIC_WEAPONS; i++) {
			WeaponItem *nonStaticWeapon = [nonStaticWeapons objectAtIndex:i];
			nonStaticWeapon.active = NO;
			nonStaticWeapon.visible = NO;
		}
	}
}

//*****************************

-(void) frictionalForces {
	if (!CGPointEqualToPoint(player.velocity, CGPointZero)) {
		player.decelaration = ccp(player.decelaration.x, player.decelaration.y+DYNAMIC_FRICTIONAL_DECELERATION);
		
		CGFloat playerCollisionForceValue = sqrt(player.collisionForce.x*player.collisionForce.x +
												 player.collisionForce.y*player.collisionForce.y);
		CGFloat playerFrictionalForceValue = sqrt(player.collisionFrictionalForce.x*player.collisionFrictionalForce.x +
												  player.collisionFrictionalForce.y*player.collisionFrictionalForce.y);
		
		if (playerCollisionForceValue > playerFrictionalForceValue) {
			player.collisionForce = ccp(player.collisionForce.x - player.collisionFrictionalForce.x, 
										player.collisionForce.y - player.collisionFrictionalForce.y);
			player.velocity = ccp(player.velocity.x + player.collisionForce.x, 
								  player.velocity.y + player.collisionForce.y);
			//		[player update];
			//		CCLOG(@"player.velocity: (%f, %f)", player.velocity.x, player.velocity.y);
		}
		else if(playerCollisionForceValue <= playerFrictionalForceValue){
			player.collisionForce = CGPointZero;
			player.collisionFrictionalForce = CGPointZero;
			player.velocity = ccp(0, player.velocity.y);
		}
		
		for (int i=0; i<nonStaticCollectables.count; i++) {
			CollectableItem *collectable = [nonStaticCollectables objectAtIndex:i];
			
			CGFloat collectableCollisionForceValue = sqrt(collectable.collisionForce.x*collectable.collisionForce.x +
														  collectable.collisionForce.y*collectable.collisionForce.y);
			CGFloat collectableFrictionalForceValue = sqrt(collectable.frictionalForce.x*collectable.frictionalForce.x +
														   collectable.frictionalForce.y*collectable.frictionalForce.y);		
			
			if (collectableCollisionForceValue > collectableFrictionalForceValue) {
				collectable.collisionForce = ccp(collectable.collisionForce.x - collectable.frictionalForce.x, 
												 collectable.collisionForce.y - collectable.frictionalForce.y);
				collectable.velocity = ccp(collectable.velocity.x + collectable.collisionForce.x, 
										   collectable.velocity.y + collectable.collisionForce.y);
				[collectable update];
			}
			else if(collectableCollisionForceValue <= collectableFrictionalForceValue){
				collectable.collisionForce = CGPointZero;
				collectable.frictionalForce = CGPointZero;
			}
		}
	}
}

-(void) playerMovementPlayerEnemiesCollision {
	
	for (int i=0; i<nonStaticCollectables.count; i++) {
		CollectableItem *collectable = [nonStaticCollectables objectAtIndex:i];
		
		CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
									   player.position.y - player.contentSize.height*0.5, 
									   player.contentSize.width, player.contentSize.height);
		
		CGRect collectableRect = CGRectMake(collectable.position.x - collectable.contentSize.width*0.5, 
											collectable.position.y - collectable.contentSize.height*0.5, 
											collectable.contentSize.width, collectable.contentSize.height);
		
		if(CGRectIntersectsRect(playerRect, collectableRect) && collectable.position.y > player.position.y) {
			
			CGPoint playerPositionToEnemy = ccp(player.position.x - collectable.position.x, player.position.y - collectable.position.y);
			CGFloat playerAngleToEnemy = ccpToAngle(playerPositionToEnemy);
			
			CGFloat playerVelocity = sqrt(player.velocity.x*player.velocity.x + player.velocity.y*player.velocity.y);
			CGFloat collisionForceMagnitude = PLAYER_COLLISION_FORCE_PLAYER_ENEMY_COLLISION * playerVelocity / PLAYER_MAX_VELOCITY;
			CGFloat frictionalForceMagnitude = PLAYER_FRICTIONAL_DECELERATION_PLAYER_ENEMY_COLLISION * playerVelocity / PLAYER_MAX_VELOCITY;
			
			player.collisionForce = ccp(collisionForceMagnitude*cos(playerAngleToEnemy), 
										collisionForceMagnitude*sin(playerAngleToEnemy));
			player.collisionFrictionalForce = ccp(frictionalForceMagnitude*cos(playerAngleToEnemy),
												  frictionalForceMagnitude*sin(playerAngleToEnemy));
		}
	}
}

-(void) enemyMovementPlayerEnemiesCollision {
	for (int i=0; i<nonStaticCollectables.count; i++) {
		CollectableItem *collectable = [nonStaticCollectables objectAtIndex:i];
		
		CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
									   player.position.y - player.contentSize.height*0.5, 
									   player.contentSize.width, player.contentSize.height);
		
		CGRect collectableRect = CGRectMake(collectable.position.x - collectable.contentSize.width*0.5, 
											collectable.position.y - collectable.contentSize.height*0.5, 
											collectable.contentSize.width, collectable.contentSize.height);
		
		if(CGRectIntersectsRect(playerRect, collectableRect)) {
			
			CGPoint collectablePositionToPlayer = ccp(collectable.position.x - player.position.x, collectable.position.y - player.position.y);
			CGFloat collectableAngleToPlayer = ccpToAngle(collectablePositionToPlayer);
			
			CGFloat playerVelocity = sqrt(player.velocity.x*player.velocity.x + player.velocity.y*player.velocity.y);
			CGFloat collisionForceMagnitude = ENEMY_COLLISION_FORCE * playerVelocity / PLAYER_MAX_VELOCITY;
			CGFloat frictionalForceMagnitude = ENEMY_FRICTIONAL_DECELERATION * playerVelocity / PLAYER_MAX_VELOCITY;
			
			collectable.collisionForce = ccp(collisionForceMagnitude*cos(collectableAngleToPlayer), 
											 collisionForceMagnitude*sin(collectableAngleToPlayer));
			
			collectable.frictionalForce = ccp(frictionalForceMagnitude*cos(collectableAngleToPlayer),
											  frictionalForceMagnitude*sin(collectableAngleToPlayer));
		}
	}
}

//*****************************

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	
	return YES;
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint unconvertedLocation = [touch locationInView: [touch view]];
	CGPoint location = [[CCDirector sharedDirector] convertToGL: unconvertedLocation];
	
	CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
								   player.position.y - player.contentSize.height*0.5, 
								   player.contentSize.width, player.contentSize.height);
	
	if (CGRectContainsPoint(playerRect, location)) {
		[self activateCollectedWeapon];
		[self deactivateWeaponsForRotationAroundPlayer];
	}
}

-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	acceX = 0;
	if(acceleration.x > ACCELEROMETER_MIN_VALUE)
		acceX = 1;
	else if(acceleration.x < -ACCELEROMETER_MIN_VALUE)
		acceX = -1;
}

-(void) pauseGame: (id) sender {
	if ([[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] resume];
		[pauseButton setString:@"  pause"];
	}
	else if (![[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] pause];
		[pauseButton setString:@"resume"];
	}
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[nonStaticCollectables release];
	[nonStaticWeapons release];
	[nonStaticCollectedWeapons release];
	[super dealloc];
}

@end


@end
