//
//  GamePlayScene.h
//  WebProject
//
//  Created by Sunny's Mac on 12/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "EnemyItem.h"
#import "GlobalVars.h"

@interface GamePlayScene : CCLayer {

@private
	PlayerItem *player;
	NSMutableArray *enemies;
	BOOL screenTouched;
	BOOL playerStatic;
	CCLabelTTF *velocityLabel;
	CCLabelTTF *accelerationLabel;
	NSString *velocityLabelText;
	NSString *accelerationLabelText;
}

@end
