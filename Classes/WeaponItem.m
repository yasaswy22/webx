//
//  WeaponItem.m
//  WebX
//
//  Created by Sunny's Mac on 1/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "WeaponItem.h"
#import "GlobalVars.h"

@implementation WeaponItem

@synthesize active;

-(void) weaponsPositionPlayerBoostMovement: (CGPoint) playerVelocity: (BOOL) playerBoost: (ccTime) dTime  {
	if (!CGPointEqualToPoint(playerVelocity, CGPointZero)) {	
			if (active) {
				if (playerBoost) {
					self.position = ccp(self.position.x, 
												   self.position.y+(TOUCH_PLAYER_VER_BOOST_ACCELERATION*dTime));
				}
				else if(!playerBoost) {
					self.position = ccp(self.position.x, 
												   self.position.y-(TOUCH_PLAYER_VER_BOOST_ACCELERATION*0.25*dTime));
				}
			}
	}
}

-(void) rotateWeapons: (CGPoint) playerPosition :(CGPoint) previousPlayerPosition {
	if (self.active) {
		
		if (!CGPointEqualToPoint(playerPosition, previousPlayerPosition)) {
			CGPoint playerMovement = ccpSub(playerPosition, previousPlayerPosition);
			self.position = ccpAdd(self.position, playerMovement);
		}

		CGPoint newRotatedPoint = ccpRotateByAngle(self.position, playerPosition, UNIT_DEGREE_IN_RADIANS);
		CGFloat weaponAngleToPlayerRadians = ccpToAngle(ccpSub(newRotatedPoint, playerPosition));
		self.position = ccp(playerPosition.x+WEAPON_MAGNITUDE_FROM_PLAYER*cos(weaponAngleToPlayerRadians), 
							playerPosition.y+WEAPON_MAGNITUDE_FROM_PLAYER*sin(weaponAngleToPlayerRadians));
	}
}

@end
