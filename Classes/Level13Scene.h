//
//  Level13Scene.h
//  WebX
//
//  Created by Sunny's Mac on 1/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "FuelItem.h"

@interface Level13Scene : CCLayer {

@private
	PlayerItem *player;
	FuelItem *fuelTank;
	CCMenuItemFont *pauseButton;
	NSMutableArray *staticEnemies;
	NSMutableArray *nonStaticCollectables;
	NSMutableArray *nonStaticWeapons;
	NSMutableArray *nonStaticCollectedWeapons;
	NSMutableArray *flyingCollectables;
	NSMutableArray *flyingWeapons;
	NSMutableArray *flyingCollectedWeapons;
	NSMutableArray *blocks;
	BOOL playerTouched;
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat acceX;
	ccTime deltaTime;
	ccTime accumulatedTime;
	ccTime totalTime;
	CGPoint touchedLocation;
	CGPoint previousPlayerPosition;
}

-(void) moveWeaponsTowardsPlayer;
-(void) getPlayerInfluenceRect;
-(void) returnMainMenu: (id) sender;

@end
