//
//  EnemyWeaponItem.h
//  WebX
//
//  Created by Sunny's Mac on 1/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface EnemyWeaponItem : CCSprite {
@private
	CGPoint velocity;
	BOOL weaponSpawned;
	BOOL reachedMaxLength;
	CGPoint intialEnemyWeaponPlayerInfluenceVelocity;
}

-(void) update;

@property CGPoint velocity;
@property BOOL weaponSpawned;
@property BOOL reachedMaxLength;
@property CGPoint intialEnemyWeaponPlayerInfluenceVelocity;

@end
