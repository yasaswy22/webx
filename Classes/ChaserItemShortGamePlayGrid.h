//
//  ChaserItemShortGamePlayGrid.h
//  WebX
//
//  Created by Sunny's Mac on 2/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChaserItemShortGamePlayGrid : NSObject {

@private
	NSArray *patternArrayArray;
}	

@property (nonatomic, retain) NSArray *patternArrayArray;

@end
