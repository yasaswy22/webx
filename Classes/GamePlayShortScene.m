//
//  GamePlayShortScene.m
//  WebX
//
//  Created by Sunny's Mac on 3/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GamePlayShortScene.h"
#import "MainMenuScene.h"
#import "GamePlayMainScene.h"
#import "ChaserItemShortGameplay.h"
#import "GameOverScene.h"

@implementation GamePlayShortScene

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* gamePlayShortScene = [CCScene node];
	GamePlayShortScene* gamePlayShortLayer = [GamePlayShortScene node];
	[gamePlayShortScene addChild:gamePlayShortLayer];
	return gamePlayShortScene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		screenSize = [[CCDirector sharedDirector] winSize];
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		self.isAccelerometerEnabled=YES;
        
        
        scoreLabel= [CCLabelTTF labelWithString:@"0" fontName:@"Marker Felt" fontSize:13];
		scoreLabel.anchorPoint = ccp(0,1);
		scoreLabel.position = ccp(0, screenSize.height);
		[self addChild:scoreLabel z:4];
		
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:15];
		
		CCMenuItemFont *exitButton = [CCMenuItemFont itemFromString:@"exit" target:self selector:@selector(returnMainMenu:)];
		exitButton.color = ccc3(TEXT_COLOR);
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5, 
								  screenSize.height - exitButton.contentSize.height * 0.5);
		
        //		pauseButton = [CCMenuItemFont itemFromString:@"  pause" target:self selector:@selector(pauseGame:)];
        //		pauseButton.color = ccc3(TEXT_COLOR);
        //		pauseButton.position = ccp(screenSize.width - pauseButton.contentSize.width * 0.5, 
        //								   pauseButton.contentSize.height * 0.5);
		
        //		CCMenu* menu = [CCMenu menuWithItems:exitButton, pauseButton, nil];
		CCMenu* menu = [CCMenu menuWithItems:exitButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu z:3];
        
        sharedGlobalSingletonVarsInstance = [GlobalSingletonVars sharedGlobalSingletonVars];
        
		gamePlayMethods = [[GamePlayMethods alloc] init]; 
		
		player = [PlayerItem spriteWithFile:@"Volleyball_Ball.png"];
		player.position = ccp(screenSize.width*0.5, screenSize.height - UNIT_GRID_HEIGHT);
        player.currentState = notMoving;
		player.velocity = CGPointZero;
		player.numNonStaticCollectables = 0;
		player.acceleration = CGPointZero;
		player.decelaration = CGPointZero;
		player.playerBoost = NO;
		[self addChild:player z:3];
		
		chaserItemShortGamePlayGrid = [[ChaserItemShortGamePlayGrid alloc] init];
		currentChaserItemGridRects = [[NSMutableArray alloc] init];
		chasingEnemiesInShortGameplay = [[NSMutableArray alloc]init];
		
        CCTexture2D *chasingEnemiesInShortGameplayTexture = [[CCTextureCache sharedTextureCache] addImage: @"Soccer_Ball.png"];
		int j = 0;
		int x;
		int y = screenSize.height / UNIT_GRID_HEIGHT;
        int ranPatternNum = arc4random()%[chaserItemShortGamePlayGrid.patternArrayArray count];
        currentChaserItemGrid = [chaserItemShortGamePlayGrid.patternArrayArray objectAtIndex: ranPatternNum];
		for (int i=0; i<[currentChaserItemGrid count]; i++) {
			x = i % (int)(screenSize.width / UNIT_GRID_WIDTH +1);
			if (j < (screenSize.width / UNIT_GRID_WIDTH +1)) {
				j++;
			}
			else {
				j=0;
				j++;
				y--;
			}
			ChaserItemShortGameplay *chasingEnemyInShortGameplay = [ChaserItemShortGameplay spriteWithTexture:chasingEnemiesInShortGameplayTexture];			
			if ([[currentChaserItemGrid objectAtIndex:i] boolValue]) {
				chasingEnemyInShortGameplay.active = YES;
				chasingEnemyInShortGameplay.position = ccp(x*UNIT_GRID_WIDTH, y*UNIT_GRID_HEIGHT);
                
                CGRect gridUnitRect = CGRectMake(x*UNIT_GRID_WIDTH - UNIT_GRID_WIDTH*0.5, 
                                                 y*UNIT_GRID_HEIGHT - UNIT_GRID_HEIGHT*0.5, 
                                                 UNIT_GRID_WIDTH, UNIT_GRID_HEIGHT);
                [currentChaserItemGridRects addObject: [NSValue valueWithCGRect:gridUnitRect]];
			}
			else {
				[chasingEnemyInShortGameplay resetChaserItemShortGameplay];
			}
			[chasingEnemiesInShortGameplay addObject:chasingEnemyInShortGameplay];
			[self addChild:chasingEnemyInShortGameplay z:3];
		}
        
		[self scheduleUpdate];
		playerTouched = NO;
		screenTouched = NO;
		accumulatedTime = 0;
		totalTime = 0;
        
	}
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	deltaTime = delta;
	
	previousPlayerPosition = player.position;
    [scoreLabel setString: [gamePlayMethods displayScore:sharedGlobalSingletonVarsInstance]];
    [gamePlayMethods updateBarLength:player :sharedGlobalSingletonVarsInstance];
    
	[gamePlayMethods checkPlayerChaserItemGridCollisionShortGamePlay: player :currentChaserItemGridRects];
    [player updateDirectionShortGamePlay];
    [player updateDuringShortGamePlay];
    
    if (player.position.x < 0 || player.position.x > screenSize.width || 
        player.position.y < 0 || player.position.y > screenSize.height) {
        [self returnMainGamePlay];
    }
    
    if (sharedGlobalSingletonVarsInstance.fuelItemsTaken < 0) {
        [self endGamePlay];
    }
    
    accumulatedTime +=deltaTime;
    
    if (accumulatedTime > TIME_ALLOWED_IN_SHORT_GAMEPLAY) {
        [gamePlayMethods updateFuelItemsShortGameplay:sharedGlobalSingletonVarsInstance];
        accumulatedTime = 0;
    }
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint unconvertedLocation = [touch locationInView: [touch view]];
	CGPoint location = [[CCDirector sharedDirector] convertToGL: unconvertedLocation];
	
	touchedLocation = location;
    touchStartLocation = location;
	
	screenTouched = YES;
	return YES;
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint unconvertedLocation = [touch locationInView: [touch view]];
	CGPoint location = [[CCDirector sharedDirector] convertToGL: unconvertedLocation];
	
	//CCLOG(@"ccTouchMoved: Touch points: (%f, %f)", location.x, location.y);
	touchMovedLocation = location;
	
	CGFloat deltaX = touchStartLocation.x - touchMovedLocation.x;
	CGFloat deltaY = touchStartLocation.y - touchMovedLocation.y;
	CGFloat absoluteDeltaX = fabs(touchStartLocation.x - touchMovedLocation.x);
	CGFloat absoluteDeltaY = fabs(touchStartLocation.y - touchMovedLocation.y);
	
	if (absoluteDeltaX >= kMinimumGestureLength && absoluteDeltaY <= kMaximumVariance) {
		// handle horizontal swipe...
		if (deltaX < 0) {
			//CCLOG(@"Moved right");
			player.currentState = movedRight;
		}
		else if (deltaX > 0) {
			//CCLOG(@"Moved left");
			player.currentState = movedLeft;
		}
	}
	else if (absoluteDeltaY >= kMinimumGestureLength && absoluteDeltaX <= kMaximumVariance){
		// handle vertical swipe...
		if (deltaY < 0) {
			//CCLOG(@"Moved top");
			player.currentState = movedTop;
		}
		else if (deltaY > 0) {
			//CCLOG(@"Moved bottom");
			player.currentState = movedBottom;
		}
	}
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
}

-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	acceX = 0;
	if(acceleration.x > ACCELEROMETER_MIN_VALUE)
		acceX = 1;
	else if(acceleration.x < -ACCELEROMETER_MIN_VALUE)
		acceX = -1;
}

-(void) pauseGame: (id) sender {
	if ([[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] resume];
		[pauseButton setString:@"  pause"];
	}
	else if (![[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] pause];
		[pauseButton setString:@"resume"];
	}
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) returnMainGamePlay {
	[[CCDirector sharedDirector] replaceScene: [GamePlayMainScene scene]];
}

-(void) endGamePlay {
	[[CCDirector sharedDirector] replaceScene: [GameOverScene scene]];
}

-(void) draw {
	[super draw];
	
	glEnable(GL_LINE_SMOOTH);
	glColor4ub(125, 100, 100, 255);  
    glLineWidth(1.0f);
	
	ccDrawLine(ccp(0, screenSize.height*0.95), ccp(player.barLength, screenSize.height*0.95));
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
    
	[gamePlayMethods release];
	[chaserItemShortGamePlayGrid release];
	[chasingEnemiesInShortGameplay release];
    [currentChaserItemGridRects release];
	[super dealloc];
}

@end

