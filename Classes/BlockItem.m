//
//  BlockItem.m
//  WebX
//
//  Created by Sunny's Mac on 1/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BlockItem.h"
#import "CollectedWeaponItem.h"
#import "EnemyItem.h"
#import "ChaserItem.h"

@implementation BlockItem

@synthesize active;
@synthesize willKill;
@synthesize velocity;

-(void) update {
	self.position = ccp(self.position.x+velocity.x, self.position.y+velocity.y);
}

-(void) updateBlockPosition {
	
	if (active) {
//        CGFloat currentVelocityY = 1 + arc4random() % (int)(targetVelocity.y);
//		velocity = ccp(-targetVelocity.x, -currentVelocityY);
		if (self.position.y < -self.contentSize.height*0.5) {
			[self resetBlockPosition];
		}
        [self update];
	}
}
	
-(void) checkCollectedWeaponsBlockCollision: (NSMutableArray *) collectedWeapons {
	
	CGRect blockRect = CGRectMake(self.position.x - self.contentSize.width*0.5, 
								  self.position.y - self.contentSize.height*0.5, 
								  self.contentSize.width, self.contentSize.height);
	
	for (int i=0; i<collectedWeapons.count; i++) {
		CollectedWeaponItem *collecteWeapon = [collectedWeapons objectAtIndex:i];
		
		CGRect collecteWeaponRect = CGRectMake(collecteWeapon.position.x - collecteWeapon.contentSize.width*0.5, 
											   collecteWeapon.position.y - collecteWeapon.contentSize.height*0.5, 
											   collecteWeapon.contentSize.width, collecteWeapon.contentSize.height);
		
		if (CGRectIntersectsRect(blockRect, collecteWeaponRect)) {
			collecteWeapon.active = NO;
		}
	}
}

-(void) checkEnemyItemBlockCollision: (NSMutableArray *) enemies {
	
	CGRect blockRect = CGRectMake(self.position.x - self.contentSize.width*0.5, 
								  self.position.y - self.contentSize.height*0.5, 
								  self.contentSize.width, self.contentSize.height);
	
	for (int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];
		
		CGRect enemyRect = CGRectMake(enemy.position.x - enemy.contentSize.width*0.5, 
									enemy.position.y - enemy.contentSize.height*0.5, 
									enemy.contentSize.width, enemy.contentSize.height);
		
		if (CGRectIntersectsRect(blockRect, enemyRect)) {
			[enemy resetEnemyPosition];
		}
	}
}

-(void) checkChaserEnemyItemBlockCollision: (NSMutableArray *) chasingEnemies {
	
	CGRect blockRect = CGRectMake(self.position.x - self.contentSize.width*0.5, 
								  self.position.y - self.contentSize.height*0.5, 
								  self.contentSize.width, self.contentSize.height);
	
	for (int i=0; i<chasingEnemies.count; i++) {
		ChaserItem *enemy = [chasingEnemies objectAtIndex:i];
		
		CGRect enemyRect = CGRectMake(enemy.position.x - enemy.contentSize.width*0.5, 
									  enemy.position.y - enemy.contentSize.height*0.5, 
									  enemy.contentSize.width, enemy.contentSize.height);
		
		if (CGRectIntersectsRect(blockRect, enemyRect) && enemy.position.y > 0) {
			[enemy makeChaserEnemyInactive];
		}
	}
}

-(void) resetBlockPosition {
	self.active = NO;
    
    int i = arc4random() % (int)(screenSize.width);
    int j = (int)(screenSize.height + self.contentSize.height*0.5) +arc4random() % (int)(screenSize.height);
    
	self.position = ccp(i, j);
}

-(void) activateBlock {
		
//		int side = arc4random() % 2;
//		
//		if (side == 0) {
//			self.position = ccp(0, screenSize.height+self.contentSize.height*0.5);
//		}
//		else if(side == 1) {
//			self.position = ccp(screenSize.width, screenSize.height+self.contentSize.height*0.5);
//		}
		active = YES;
}

@end
