//
//  FacebookGameCenterMappingViewController.m
//  WebX
//
//  Created by Sunny's Mac on 7/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FacebookGameCenterMappingViewController.h"
#import "FacebookTable.h"
#import "PersonalDetailsTable.h"
#import "WebXAppDelegate.h"
#define FRIENDS_PAGE_MULTIPLE 1
#define SIZE_OF_ROW_IN_TABLE_VIEW 64*FRIENDS_PAGE_MULTIPLE
#define FONT_OF_NAME_IN_TABLE_VIEW 16*FRIENDS_PAGE_MULTIPLE

@implementation FacebookGameCenterMappingViewController

@synthesize freindsList;
@synthesize fetchedFacebookEntries;

@synthesize managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.managedObjectContext = [(WebXAppDelegate*)[UIApplication sharedApplication].delegate managedObjectContext];
        
        [NSTimer scheduledTimerWithTimeInterval: 0.2
                                         target: self
                                       selector: @selector(resetTime:) 
                                       userInfo: nil repeats:YES];
        
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinner.center = CGPointMake(self.view.bounds.size.width*0.5, self.view.bounds.size.height*0.5);
        [self.view addSubview:spinner];
        [self showingloadingActivity];
        
        [self loadFriendsFacebookDetailsFromCoreData];
        
        facebookAppId = [NSString stringWithString:@"168636726530184"];
        facebook = [[Facebook alloc] initWithAppId:facebookAppId];
        facebook.accessToken    = [[NSUserDefaults standardUserDefaults] stringForKey:@"AccessToken"];
        facebook.expirationDate = (NSDate *) [[NSUserDefaults standardUserDefaults] objectForKey:@"ExpirationDate"];
        
        [self getFriendsListDetails];
    }
    
    return self;
}

- (void)dealloc
{
    [self.freindsList release];
    [facebook release];
    [self.managedObjectContext release];
    [self.fetchedFacebookEntries release];    
    [spinner release];
    [super dealloc];
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Activity Indicator

-(void) resetTime:(NSTimer *)t{
    if (timerForSpinning > 10) {
        [self notShowingloadingActivity];
    }
    else{
        timerForSpinning += 1;
        [self showingloadingActivity];
    }
}

-(void) showingloadingActivity{
    [spinner startAnimating];
    self.view.userInteractionEnabled = NO;
}

-(void) notShowingloadingActivity{
    [spinner stopAnimating];
    self.view.userInteractionEnabled = YES;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self.freindsList release];
    self.freindsList = nil;
    [self.managedObjectContext release];
    self.managedObjectContext = nil;
    [self.fetchedFacebookEntries release];
    self.fetchedFacebookEntries = nil;
    [spinner release];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark UITableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    if ([fetchedFacebookEntries count] == 0) {
        return 1;
    }
    else {
        return [fetchedFacebookEntries count];            
    }
    
    return 0;
}

-(NSString *) tableView:(UITableView *)tableView nameAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.fetchedFacebookEntries lastObject] != nil) {
        FacebookTable *facebookData = [self.fetchedFacebookEntries objectAtIndex:indexPath.row];
        return facebookData.facebookName;
    }
    else{
        return @"You have no friends...";
    }
    return nil;
}

- (UIImage*)image: (UIImage *)image scaleToSize:(CGSize)size  {
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, size.width, size.height), [image CGImage]);
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

-(UIImage *) tableView:(UITableView *)tableView pictureAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.fetchedFacebookEntries lastObject] != nil) {
        FacebookTable *facebookData = [self.fetchedFacebookEntries objectAtIndex:indexPath.row];
        UIImage *tempImg = [UIImage imageWithData:facebookData.facebookPic];
        return [self image:tempImg scaleToSize:CGSizeMake(tempImg.size.width*FRIENDS_PAGE_MULTIPLE, 
                                                          tempImg.size.height*FRIENDS_PAGE_MULTIPLE)];     
    }
    UIImage *lodingImage = [UIImage imageNamed:@"Question_Mark_Icon_BlueGlow"];
    return [self image:lodingImage scaleToSize:CGSizeMake(lodingImage.size.width*FRIENDS_PAGE_MULTIPLE, 
                                                          lodingImage.size.height*FRIENDS_PAGE_MULTIPLE)];   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SIZE_OF_ROW_IN_TABLE_VIEW;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FacebookTableViewCellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:YES animated:YES];
    cell.textLabel.font = [UIFont fontWithName:nil size:FONT_OF_NAME_IN_TABLE_VIEW];
    cell.textLabel.text = [self tableView:tableView nameAtIndexPath:indexPath];
    cell.imageView.image = [self tableView:tableView pictureAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    [self postOnFaceBookFriends];
}

#pragma mark Facebook delegate methods

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    return [facebook handleOpenURL:url]; 
}

-(void) fbDidLogin{
    [self synchronizeFacebook];
    [self getFriendsListDetails];
    
}

-(void) synchronizeFacebook{
    [[NSUserDefaults standardUserDefaults] setObject:facebook.accessToken forKey:@"AccessToken"];
    [[NSUserDefaults standardUserDefaults] setObject:facebook.expirationDate forKey:@"ExpirationDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) authorizeFacebook{
    NSArray *facebookPermissions = [NSArray arrayWithObjects:@"offline_access", 
                                    @"user_about_me", @"read_stream",@"publish_stream", nil];
    [facebook authorize:facebookPermissions delegate:self];
}

-(void) getFriendsListDetails{
    NSString* fql = 
    @"SELECT name, pic_square, uid FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me()) LIMIT 25";
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObject:fql forKey:@"query"];
    [facebook requestWithMethodName:@"fql.query" 
                          andParams:params 
                      andHttpMethod:@"GET" 
                        andDelegate: self];
}

-(void)request:(FBRequest *)request didLoad:(id)result{
    
    timerForSpinning = 0;
    
    if ([result isKindOfClass:[NSArray class]]) {
        NSArray *allResponses = result;
        
        for ( int i=0; i < [allResponses count]; i++ ) {   
            NSDictionary *response = [allResponses objectAtIndex:i];
            
            if ([[request.params valueForKey:@"query"] isEqualToString:
                 @"SELECT name, pic_square, uid FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me()) LIMIT 25"]){
                [FacebookTable entryWithFacebookData:response inManagedObjectContext:self.managedObjectContext];          
            }
        }
        [self loadFriendsFacebookDetailsFromCoreData];
    }
}

- (void) publishStreamFriends {
    
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   facebookAppId, @"app_id",
                                   @"http://developers.facebook.com/docs/reference/dialogs/", @"link",
                                   @"http://fbrell.com/f8.jpg", @"picture",
                                   @"Escape from Hell", @"name",
                                   @"I just escaped hell and score 9999 points. World Rank: 1. Is the no one to challenge me", @"caption",
                                   @"Escape from Hell is a arcade game with intense action and a great community using Facebook and Game Center", @"description",
                                   @"",  @"message",
                                   nil];
    
    [facebook dialog:@"feed" andParams:params andDelegate:self];
}

-(void) postOnFaceBookFriends {
    
    if (![facebook isSessionValid]){
        [self authorizeFacebook];
    }
    if ([facebook isSessionValid]) {
        [self publishStreamFriends];
    }
}

- (IBAction)FacebookViewFinished:(id)sender {
    
    // Saves changes in the application's managed object context before the application terminates.
    
    [self dismissModalViewController];
}

-(void) loadFriendsFacebookDetailsFromCoreData {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"FacebookTable" 
                                   inManagedObjectContext:self.managedObjectContext]];
    [request setPredicate:nil];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"facebookName" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
    
    NSError *error = nil;
    [self.fetchedFacebookEntries release];
    self.fetchedFacebookEntries  = [[self.managedObjectContext executeFetchRequest:request error:&error] retain];
    [request release];
    
    [self.freindsList reloadData];
}

#pragma mark Views (Facebook View)

// Helper methods

-(UIViewController*) getRootViewController
{
	return [UIApplication sharedApplication].keyWindow.rootViewController;
}

-(void) presentViewController:(UIViewController*)vc
{
	UIViewController* rootVC = [self getRootViewController];
	[rootVC presentModalViewController:vc animated:YES];
}

-(void) dismissModalViewController
{
	UIViewController* rootVC = [self getRootViewController];
	[rootVC dismissModalViewControllerAnimated:YES];
}

@end