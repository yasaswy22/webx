//
//  Level9Scene.h
//  WebX
//
//  Created by Sunny's Mac on 1/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "FuelItem.h"

@interface Level9Scene : CCLayer {

@private
	PlayerItem *player;
	FuelItem *fuelTank;
	CCMenuItemFont *pauseButton;
	NSMutableArray *nonStaticCollectables;
	NSMutableArray *nonStaticWeapons;
	NSMutableArray *nonStaticCollectedWeapons;
	BOOL playerTouched;
	BOOL playerBoost;
	CGFloat acceX;
	ccTime deltaTime;
}

@end
