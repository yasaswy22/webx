//
//  FlyingMountainItem.h
//  WebX
//
//  Created by Sunny's Mac on 8/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface FlyingMountainItem : CCSprite {

@private
    CGPoint velocity;
}

@property CGPoint velocity;

-(void) update;
-(void) setPositioning;
-(void) setPositioningInitiallyLayer1Rocks;
-(void) setPositioningInitiallyLayer2Rocks;
-(void) setScaling;
-(void) setPositionInAnyQuadrant;
-(void) setPositionInAnyQuadrantInitially;

@end
