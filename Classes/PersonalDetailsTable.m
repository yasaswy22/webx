//
//  PersonalDetailsTable.m
//  WebX
//
//  Created by Sunny's Mac on 6/29/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "PersonalDetailsTable.h"


@implementation PersonalDetailsTable
@dynamic facebookName;
@dynamic facebookPic;
@dynamic facebookUUID;
@dynamic gameCenterAlias;
@dynamic gameCenterEmail;
@dynamic gameCenterScore;
@dynamic gameCenterID;

+(PersonalDetailsTable *) entryOrUpdateWithFacebookData:(NSDictionary *)facebookData inManagedObjectContext:(NSManagedObjectContext *)context{
    
    PersonalDetailsTable *facebookEntry = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"PersonalDetailsTable" inManagedObjectContext:context]];
//    [request setPredicate:[NSPredicate predicateWithFormat:@"facebookUUID = %@", 
//                           [facebookData objectForKey:@"uid"]]];
    NSError *error = nil;
    facebookEntry = [[context executeFetchRequest:request error:&error] lastObject];
    
    if (!error && !facebookEntry) {
        facebookEntry = [NSEntityDescription insertNewObjectForEntityForName:@"PersonalDetailsTable" inManagedObjectContext:context];
        
        facebookEntry.facebookName = [NSString stringWithString:[facebookData valueForKey:@"name"]];
        facebookEntry.facebookUUID = (NSNumber *)[facebookData valueForKey:@"uid"];
        facebookEntry.facebookPic = [NSData dataWithContentsOfURL:
                                     [NSURL URLWithString:[facebookData valueForKey:@"pic_square"]]];
    }
    else if (!error && facebookEntry) {
        [facebookEntry setValue:[NSString stringWithString:[facebookData valueForKey:@"name"]] forKey:@"facebookName"];
        [facebookEntry setValue:(NSNumber *)[facebookData valueForKey:@"uid"] forKey:@"facebookUUID"];
        [facebookEntry setValue:[NSData dataWithContentsOfURL:
                                 [NSURL URLWithString:[facebookData valueForKey:@"pic_square"]]] forKey:@"facebookPic"];
    }
    // Commit the change.
    if (![context save:&error]) {
        // Handle the error.
        NSLog(@"Saving changes failed: %@", error);
        
    }
    
    return facebookEntry;
}

+(PersonalDetailsTable *) entryOrUpdateWithGameCenterAlias:(NSString *)gameCenterAlias entryWithGameCenterID:(NSString *)gameCenterID inManagedObjectContext:(NSManagedObjectContext *)context{
    
    PersonalDetailsTable *gameCenterEntry = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"PersonalDetailsTable" inManagedObjectContext:context]];
//    [request setPredicate:[NSPredicate predicateWithFormat:@"gameCenterID = %@", gameCenterID]];
    NSError *error = nil;
    gameCenterEntry = [[context executeFetchRequest:request error:&error] lastObject];
    
    if (!error && !gameCenterEntry) {
        NSLog(@"No Gamecenter entry yet in core data");
        gameCenterEntry = [NSEntityDescription insertNewObjectForEntityForName:@"PersonalDetailsTable" inManagedObjectContext:context];
        
        gameCenterEntry.gameCenterAlias = gameCenterAlias;
        gameCenterEntry.gameCenterID = gameCenterID;
        gameCenterEntry.gameCenterScore = 0;

    }
    else if (!error && gameCenterEntry) {
                NSLog(@"1 entry there in Gamecenter in core data");
                [gameCenterEntry setValue:gameCenterAlias forKey:@"gameCenterAlias"];
                [gameCenterEntry setValue:gameCenterID forKey:@"gameCenterID"];        
                [gameCenterEntry setValue:0 forKey:@"gameCenterScore"];
    }
    
    // Commit the change.
    if (![context save:&error]) {
        // Handle the error.
        NSLog(@"Saving changes failed: %@", error);
        
    }
    
    return gameCenterEntry;
}

+(PersonalDetailsTable *) updateGameCenterScore:(NSNumber *)gameCenterScore 
                                forGameCenterID:(NSString *)gameCenterID 
                             forGameCenterAlias:(NSString *)gameCenterAlias 
                         inManagedObjectContext:(NSManagedObjectContext *)context{
    
    PersonalDetailsTable *gameCenterEntry = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"PersonalDetailsTable" inManagedObjectContext:context]];
//    [request setPredicate:[NSPredicate predicateWithFormat:@"gameCenterID = %@", gameCenterID]];
    NSError *error = nil;
    gameCenterEntry = [[context executeFetchRequest:request error:&error] lastObject];
    
    if (!error && !gameCenterEntry) {
        gameCenterEntry = [NSEntityDescription insertNewObjectForEntityForName:@"PersonalDetailsTable" inManagedObjectContext:context];
        
        gameCenterEntry.gameCenterAlias = gameCenterAlias;
        gameCenterEntry.gameCenterID = gameCenterID;
        gameCenterEntry.gameCenterScore = (NSNumber *)[gameCenterID integerValue];
    }
    else if (!error && gameCenterEntry) {
        [gameCenterEntry setValue:gameCenterScore forKey:@"gameCenterScore"];
        [gameCenterEntry setValue:gameCenterID forKey:@"gameCenterID"];
        [gameCenterEntry setValue:gameCenterAlias forKey:@"gameCenterAlias"];
    }
    
    // Commit the change.
    if (![context save:&error]) {
        // Handle the error.
        NSLog(@"Saving changes failed: %@", error);
        
    }
    
    return gameCenterEntry;
}

@end
