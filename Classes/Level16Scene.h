//
//  Level16Scene.h
//  WebX
//
//  Created by Sunny's Mac on 2/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "FuelItem.h"

@interface Level16Scene : CCLayer {

@private
	PlayerItem *player;
	FuelItem *fuelTank;
	CCMenuItemFont *pauseButton;
	NSMutableArray *staticEnemies;
	NSMutableArray *chasingEnemies;
	NSMutableArray *nonStaticCollectables;
	NSMutableArray *nonStaticWeapons;
	NSMutableArray *nonStaticCollectedWeapons;
	NSMutableArray *flyingCollectables;
	NSMutableArray *flyingWeapons;
	NSMutableArray *flyingCollectedWeapons;
	NSMutableArray *blocks;
	NSMutableArray *chasingEnemiesPlacementGrid;
	BOOL playerTouched;
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat acceX;
	ccTime deltaTime;
	ccTime accumulatedTime;
	ccTime totalTime;
	CGPoint touchedLocation;
	CGPoint previousPlayerPosition;
}

-(void) returnMainMenu: (id) sender;

@end
