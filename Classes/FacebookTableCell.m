//
//  FacebookTableCell.m
//  WebX
//
//  Created by Sunny's Mac on 7/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FacebookTableCell.h"


@implementation FacebookTableCell

@synthesize profilePicture;
@synthesize profileName;
@synthesize score;
@synthesize backgroundCellView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [profilePicture release];
    [profileName release];
    [score release];
    [backgroundCellView release];
    [super dealloc];
}

@end
