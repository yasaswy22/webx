//
//  Level11Scene.m
//  WebX
//
//  Created by Sunny's Mac on 1/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Level11Scene.h"
#import "GlobalVars.h"
#import "MainMenuScene.h"
#import "WeaponItem.h"
#import "CollectableItem.h"
#import "CollectedWeaponItem.h"
#import "EnemyItem.h"

@implementation Level11Scene

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* level11Scene = [CCScene node];
	Level11Scene* level11Layer = [Level11Scene node];
	[level11Scene addChild:level11Layer];
	return level11Scene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		screenSize = [[CCDirector sharedDirector] winSize];
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		self.isAccelerometerEnabled=YES;
		
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:15];
		
		CCMenuItemFont *exitButton = [CCMenuItemFont itemFromString:@"exit" target:self selector:@selector(returnMainMenu:)];
		exitButton.color = ccc3(TEXT_COLOR);
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5, 
								  screenSize.height - exitButton.contentSize.height * 0.5);
		
		pauseButton = [CCMenuItemFont itemFromString:@"  pause" target:self selector:@selector(pauseGame:)];
		pauseButton.color = ccc3(TEXT_COLOR);
		pauseButton.position = ccp(screenSize.width - pauseButton.contentSize.width * 0.5, 
								   pauseButton.contentSize.height * 0.5);
		
		CCMenu* menu = [CCMenu menuWithItems:exitButton, pauseButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu];
		
		player = [PlayerItem spriteWithFile:@"Volleyball_Ball.png"];
		player.position = ccp(screenSize.width*0.5, MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		player.velocity = CGPointZero;
		player.numFlyingCollectables = 0;
		player.acceleration = CGPointZero;
		player.decelaration = CGPointZero;
		player.velocity = ccp(0, PLAYER_INITIAL_VELOCITY);
		player.acceleration = ccp(0, PLAYER_INITIAL_ACCELERATION);
		[self addChild:player z:2];
		
		fuelTank = [FuelItem spriteWithFile:@"fuel_tank.png"];
		int i = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
		int j = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
		fuelTank.position = ccp(i, j);
		[self addChild:fuelTank z:2];
		
		staticEnemies = [[NSMutableArray alloc]init];
		CCTexture2D *enemyTexture = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_brown.png"];
		for (int i=0; i<NUMBER_OF_STATIC_ENEMIES; i++) {
			EnemyItem *enemy = [EnemyItem spriteWithTexture:enemyTexture];
			
			int j = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
			int k = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
			
			enemy.position = ccp(j, k);
			enemy.collisionForce = CGPointZero;
			enemy.frictionalForce = CGPointZero;
			[staticEnemies addObject:enemy];
			[self addChild:enemy z:1];
		}
		
		flyingCollectables = [[NSMutableArray alloc]init];
		CCTexture2D *collectableTextureFlying = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_cyan.png"];
		for (int i=0; i<NUMBER_OF_FLYING_ENEMIES; i++) {
			CollectableItem *collectable = [CollectableItem spriteWithTexture:collectableTextureFlying];
			
			int side = arc4random()%2;
			int j = arc4random() % (int)(screenSize.width*0.5);
			int k = (int) screenSize.height * 0.5 + arc4random() % (int)(screenSize.height*0.5);
			
			if (side == 0) {
				collectable.position = ccp(-j, k);
				collectable.velocity = ccp(FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
			else if (side == 1) {
				collectable.position = ccp(screenSize.width + j, k);
				collectable.velocity = ccp(-FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
			
			collectable.position = ccp(j, k);
			collectable.collisionForce = CGPointZero;
			collectable.frictionalForce = CGPointZero;
			collectable.flying = YES;
			[flyingCollectables addObject:collectable];
			[self addChild:collectable z:1];
		}
		
		flyingWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *weaponTextureFlying = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_cyan.png"];
		for (int i = 0; i<MAX_FLYING_WEAPONS; i++) {
			WeaponItem *flyingWeapon = [WeaponItem spriteWithTexture: weaponTextureFlying];
			flyingWeapon.active = NO;
			flyingWeapon.position = WEAPON_ITEM_RESET_POSITION;
			[flyingWeapons addObject:flyingWeapon];
			[self addChild:flyingWeapon];
		}
		
		flyingCollectedWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *collectedWeaponTextureFlying = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_cyan.png"];
		for (int i = 0; i<MAX_FLYING_WEAPONS; i++) {
			CollectedWeaponItem *flyingCollectedWeapon = [CollectedWeaponItem spriteWithTexture: collectedWeaponTextureFlying];
			flyingCollectedWeapon.velocity = CGPointZero;
			flyingCollectedWeapon.acceleration = CGPointZero;			
			flyingCollectedWeapon.collisionForce = CGPointZero;
			flyingCollectedWeapon.frictionalForce = CGPointZero;
			flyingCollectedWeapon.flying = YES;
			flyingCollectedWeapon.active = NO;
			flyingCollectedWeapon.visible = NO;
			flyingCollectedWeapon.position = player.position;
			[flyingCollectedWeapons addObject:flyingCollectedWeapon];
			[self addChild:flyingCollectedWeapon];
		}
		
		[self scheduleUpdate];
		playerTouched = NO;
		screenTouched = NO;
		accumulatedTime = 0;
		totalTime = 0;
	}
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	deltaTime = delta;
	
	if (screenTouched) {
		if (accumulatedTime > delta*1.5) {
			//CCLOG(@"accumulatedTime: %f, delta: %f", accumulatedTime, delta);
			[player powerUpForces];
			[player playerFrictionalForces];
			[player limitPlayerBoundaries];
			[player playerFrictionalForcesEnemyCollisionMovement];
			[player checkPlayerTakesFuel: fuelTank];
			[fuelTank updateFuelTankPosition: player.velocity];
			
			for (int i=0; i<staticEnemies.count; i++) {
				EnemyItem *enemy = [staticEnemies objectAtIndex:i];
				
				[enemy updateEnemiesPosition: player.velocity];
				[player playerMovementPlayerEnemiesCollision: enemy];
				[enemy staticEnemyFrictionalForce];
			}
			
			for (int i=0; i<flyingCollectables.count; i++) {
				CollectableItem *flyingCollectable = [flyingCollectables objectAtIndex:i];
				
				[flyingCollectable updateCollectablesPosition: player.velocity];
				[player checkPlayerTakesCollectable: flyingCollectable];
			}
			
			for(int i=0; i<flyingWeapons.count; i++) {
				WeaponItem *flyingWeapon = [flyingWeapons objectAtIndex:i];
				
				[flyingWeapon weaponsPositionPlayerBoostMovement: player.velocity: player.playerBoost :deltaTime];
				[flyingWeapon weaponsPositionAccelerometerInputIntoAccount: player.velocity: acceX];
				[flyingWeapon rotateWeapons: player.position];
			}
			
			for(int i=0; i<flyingCollectedWeapons.count; i++) {
				CollectedWeaponItem *flyingCollectedWeapon = [flyingCollectedWeapons objectAtIndex:i];
				[flyingCollectedWeapon update];
				[flyingCollectedWeapon holdUnactivatedCollectedWeapon: player.position];
				[flyingCollectedWeapon checkCollectedWeaponBoundaries];
				[flyingCollectedWeapon enemyMovementCollectableWeaponCollision: staticEnemies];
			}
			
			[player activateFlyingWeaponsForRotationAroundPlayer: flyingWeapons];
			[player update];
			
			accumulatedTime = 0;
		}
		
		if (totalTime > MAX_TIME_SLOWDOWN) {
			screenTouched = NO;
			totalTime = 0;
		}
		accumulatedTime += delta;
		totalTime +=delta;
	}
	else {
		[player powerUpForces];
		[player playerFrictionalForces];
		[player limitPlayerBoundaries];
		[player playerFrictionalForcesEnemyCollisionMovement];
		[player checkPlayerTakesFuel: fuelTank];
		[fuelTank updateFuelTankPosition: player.velocity];
		
		for (int i=0; i<staticEnemies.count; i++) {
			EnemyItem *enemy = [staticEnemies objectAtIndex:i];
			
			[enemy updateEnemiesPosition: player.velocity];
			[player playerMovementPlayerEnemiesCollision: enemy];
			[enemy staticEnemyFrictionalForce];
		}
		
		for (int i=0; i<flyingCollectables.count; i++) {
			CollectableItem *flyingCollectable = [flyingCollectables objectAtIndex:i];
			
			[flyingCollectable updateCollectablesPosition: player.velocity];
			[player checkPlayerTakesCollectable: flyingCollectable];
		}
		
		for(int i=0; i<flyingWeapons.count; i++) {
			WeaponItem *flyingWeapon = [flyingWeapons objectAtIndex:i];
			
			[flyingWeapon weaponsPositionPlayerBoostMovement: player.velocity: player.playerBoost :deltaTime];
			[flyingWeapon weaponsPositionAccelerometerInputIntoAccount: player.velocity: acceX];
			[flyingWeapon rotateWeapons: player.position];
		}
		
		for(int i=0; i<flyingCollectedWeapons.count; i++) {
			CollectedWeaponItem *flyingCollectedWeapon = [flyingCollectedWeapons objectAtIndex:i];
			[flyingCollectedWeapon update];
 			[flyingCollectedWeapon holdUnactivatedCollectedWeapon: player.position];
			[flyingCollectedWeapon checkCollectedWeaponBoundaries];
			[flyingCollectedWeapon enemyMovementCollectableWeaponCollision: staticEnemies];
		}
		
		[player activateFlyingWeaponsForRotationAroundPlayer: flyingWeapons];
		[player update];
	}
	
	[player playerBoostMovement: deltaTime];
	[player updateAccelerometerInput: acceX];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	screenTouched = YES;
	return YES;
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	
	screenTouched = NO;
	
	CGPoint unconvertedLocation = [touch locationInView: [touch view]];
	CGPoint location = [[CCDirector sharedDirector] convertToGL: unconvertedLocation];
	
	[player activateFlyingCollectedWeapons: flyingCollectedWeapons: location];
	[player deactivateFlyingWeaponsForRotationAroundPlayer: flyingWeapons];
}

-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	acceX = 0;
	if(acceleration.x > ACCELEROMETER_MIN_VALUE)
		acceX = 1;
		else if(acceleration.x < -ACCELEROMETER_MIN_VALUE)
			acceX = -1;
			}

-(void) pauseGame: (id) sender {
	if ([[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] resume];
		[pauseButton setString:@"  pause"];
	}
	else if (![[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] pause];
		[pauseButton setString:@"resume"];
	}
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[staticEnemies release];
	[flyingCollectables release];
	[flyingWeapons release];
	[flyingCollectedWeapons release];
	[super dealloc];
}

@end
