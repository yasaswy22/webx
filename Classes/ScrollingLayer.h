//
//  ScrollingLayer.h
//  WebX
//
//  Created by Sunny's Mac on 1/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface ScrollingLayer : CCLayer {

@private 
	CGPoint velocity;
}

@property CGPoint velocity;

-(void) update;

@end
