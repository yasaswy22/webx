//
//  Level11Scene.h
//  WebX
//
//  Created by Sunny's Mac on 1/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "FuelItem.h"

@interface Level11Scene : CCLayer {
	
@private
	PlayerItem *player;
	FuelItem *fuelTank;
	CCMenuItemFont *pauseButton;
	NSMutableArray *staticEnemies;
	NSMutableArray *flyingCollectables;
	NSMutableArray *flyingWeapons;
	NSMutableArray *flyingCollectedWeapons;
	BOOL playerTouched;
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat acceX;
	ccTime deltaTime;
	ccTime accumulatedTime;
	ccTime totalTime;
}

@end
