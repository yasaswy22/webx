//
//  GridItem.h
//  WebX
//
//  Created by Sunny's Mac on 2/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GridItem : NSObject {

@private
	CGRect gridRect;
	BOOL isFree;
}

@property CGRect gridRect;
@property BOOL isFree;

@end
