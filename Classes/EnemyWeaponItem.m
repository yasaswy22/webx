//
//  EnemyWeapon.m
//  WebX
//
//  Created by Sunny's Mac on 1/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EnemyWeaponItem.h"


@implementation EnemyWeaponItem

@synthesize velocity;
@synthesize weaponSpawned;
@synthesize reachedMaxLength;
@synthesize intialEnemyWeaponPlayerInfluenceVelocity;

-(void) update {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	self.position = ccp(self.position.x+velocity.x, self.position.y+velocity.y);
}

@end
