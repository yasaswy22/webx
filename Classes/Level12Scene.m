//
//  Level12Scene.m
//  WebX
//
//  Created by Sunny's Mac on 1/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Level12Scene.h"
#import "MainMenuScene.h"
#import "WeaponItem.h"
#import "CollectableItem.h"
#import "CollectedWeaponItem.h"
#import "EnemyItem.h"

@implementation Level12Scene

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* level12Scene = [CCScene node];
	Level12Scene* level12Layer = [Level12Scene node];
	[level12Scene addChild:level12Layer];
	return level12Scene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		screenSize = [[CCDirector sharedDirector] winSize];
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		self.isAccelerometerEnabled=YES;
		
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:15];
		
		CCMenuItemFont *exitButton = [CCMenuItemFont itemFromString:@"exit" target:self selector:@selector(returnMainMenu:)];
		exitButton.color = ccc3(TEXT_COLOR);
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5, 
								  screenSize.height - exitButton.contentSize.height * 0.5);
		
		pauseButton = [CCMenuItemFont itemFromString:@"  pause" target:self selector:@selector(pauseGame:)];
		pauseButton.color = ccc3(TEXT_COLOR);
		pauseButton.position = ccp(screenSize.width - pauseButton.contentSize.width * 0.5, 
								   pauseButton.contentSize.height * 0.5);
		
		CCMenu* menu = [CCMenu menuWithItems:exitButton, pauseButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu];
		
		player = [PlayerItem spriteWithFile:@"Volleyball_Ball.png"];
		player.position = ccp(screenSize.width*0.5, MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		player.velocity = CGPointZero;
		player.numNonStaticCollectables = 0;
		player.acceleration = CGPointZero;
		player.decelaration = CGPointZero;
		player.velocity = ccp(0, PLAYER_INITIAL_VELOCITY);
		player.acceleration = ccp(0, PLAYER_INITIAL_ACCELERATION);
		[self addChild:player z:2];
		
		fuelTank = [FuelItem spriteWithFile:@"fuel_tank.png"];
		int i = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
		int j = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
		fuelTank.position = ccp(i, j);
		[self addChild:fuelTank z:2];
		
		staticEnemies = [[NSMutableArray alloc]init];
		CCTexture2D *enemyTexture = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_brown.png"];
		for (int i=0; i<NUMBER_OF_STATIC_ENEMIES; i++) {
			EnemyItem *enemy = [EnemyItem spriteWithTexture:enemyTexture];
			
			int j = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
			int k = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
			
			enemy.position = ccp(j, k);
			enemy.collisionForce = CGPointZero;
			enemy.frictionalForce = CGPointZero;
			[staticEnemies addObject:enemy];
			[self addChild:enemy z:1];
		}
		
		nonStaticCollectables = [[NSMutableArray alloc]init];
		CCTexture2D *collectableTextureNonStatic = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_green.png"];
		for (int i=0; i<NUMBER_OF_NON_STATIC_ENEMIES; i++) {
			CollectableItem *collectable = [CollectableItem spriteWithTexture:collectableTextureNonStatic];
			
			int j = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
			int k = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
			
			collectable.position = ccp(j, k);
			collectable.collisionForce = CGPointZero;
			collectable.frictionalForce = CGPointZero;
			collectable.nonStatic = YES;
			[nonStaticCollectables addObject:collectable];
			[self addChild:collectable z:1];
		}
		
		nonStaticWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *weaponTextureNonStatic = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_green.png"];
		for (int i = 0; i<MAX_NON_STATIC_WEAPONS; i++) {
			WeaponItem *nonStaticWeapon = [WeaponItem spriteWithTexture: weaponTextureNonStatic];
			nonStaticWeapon.visible = NO;
			nonStaticWeapon.active = NO;
			nonStaticWeapon.position = WEAPON_ITEM_RESET_POSITION;
			[nonStaticWeapons addObject:nonStaticWeapon];
			[self addChild:nonStaticWeapon];
		}
		
		nonStaticCollectedWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *collectedWeaponTextureNonStatic = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_green.png"];
		for (int i = 0; i<MAX_NON_STATIC_WEAPONS; i++) {
			CollectedWeaponItem *nonStaticCollectedWeapon = [CollectedWeaponItem spriteWithTexture: collectedWeaponTextureNonStatic];
			nonStaticCollectedWeapon.velocity = CGPointZero;
			nonStaticCollectedWeapon.acceleration = CGPointZero;			
			nonStaticCollectedWeapon.collisionForce = CGPointZero;
			nonStaticCollectedWeapon.frictionalForce = CGPointZero;
			nonStaticCollectedWeapon.nonStatic = YES;
			nonStaticCollectedWeapon.active = NO;
			nonStaticCollectedWeapon.visible = NO;
			nonStaticCollectedWeapon.position = player.position;
			[nonStaticCollectedWeapons addObject:nonStaticCollectedWeapon];
			[self addChild:nonStaticCollectedWeapon];
		}
		
		flyingCollectables = [[NSMutableArray alloc]init];
		CCTexture2D *collectableTextureFlying = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_cyan.png"];
		for (int i=0; i<NUMBER_OF_FLYING_ENEMIES; i++) {
			CollectableItem *collectable = [CollectableItem spriteWithTexture:collectableTextureFlying];
			
			int side = arc4random()%2;
			int j = arc4random() % (int)(screenSize.width*0.5);
			int k = (int) screenSize.height * 0.5 + arc4random() % (int)(screenSize.height*0.5);
			
			if (side == 0) {
				collectable.position = ccp(-j, k);
				collectable.velocity = ccp(FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
			else if (side == 1) {
				collectable.position = ccp(screenSize.width + j, k);
				collectable.velocity = ccp(-FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
			
			collectable.position = ccp(j, k);
			collectable.collisionForce = CGPointZero;
			collectable.frictionalForce = CGPointZero;
			collectable.flying = YES;
			[flyingCollectables addObject:collectable];
			[self addChild:collectable z:1];
		}
		
		flyingWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *weaponTextureFlying = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_cyan.png"];
		for (int i = 0; i<MAX_FLYING_WEAPONS; i++) {
			WeaponItem *flyingWeapon = [WeaponItem spriteWithTexture: weaponTextureFlying];
			flyingWeapon.visible = NO;
			flyingWeapon.active = NO;
			flyingWeapon.position = WEAPON_ITEM_RESET_POSITION;
			[flyingWeapons addObject:flyingWeapon];
			[self addChild:flyingWeapon];
		}
		
		flyingCollectedWeapons = [[NSMutableArray alloc] init];
		CCTexture2D *collectedWeaponTextureFlying = [[CCTextureCache sharedTextureCache] addImage: @"soccer_ball_cyan.png"];
		for (int i = 0; i<MAX_FLYING_WEAPONS; i++) {
			CollectedWeaponItem *flyingCollectedWeapon = [CollectedWeaponItem spriteWithTexture: collectedWeaponTextureFlying];
			flyingCollectedWeapon.velocity = CGPointZero;
			flyingCollectedWeapon.acceleration = CGPointZero;			
			flyingCollectedWeapon.collisionForce = CGPointZero;
			flyingCollectedWeapon.frictionalForce = CGPointZero;
			flyingCollectedWeapon.flying = YES;
			flyingCollectedWeapon.active = NO;
			flyingCollectedWeapon.visible = NO;
			flyingCollectedWeapon.position = player.position;
			[flyingCollectedWeapons addObject:flyingCollectedWeapon];
			[self addChild:flyingCollectedWeapon];
		}
		
		[self scheduleUpdate];
		playerTouched = NO;
		screenTouched = NO;
		accumulatedTime = 0;
		totalTime = 0;
	}
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	deltaTime = delta;
	
	[player powerUpForces];
	[player playerFrictionalForces];
	[player limitPlayerBoundaries];
	[player playerFrictionalForcesEnemyCollisionMovement];
	[player checkPlayerTakesFuel: fuelTank];
	[fuelTank updateFuelTankPosition: player.velocity];
	
	for (int i=0; i<staticEnemies.count; i++) {
		EnemyItem *enemy = [staticEnemies objectAtIndex:i];
		
		[enemy updateEnemiesPosition: player.velocity];
		[player playerMovementPlayerEnemiesCollision: enemy];
		[enemy staticEnemyFrictionalForce];
	}
	
	for (int i=0; i<nonStaticCollectables.count; i++) {
		CollectableItem *nonStaticCollectable = [nonStaticCollectables objectAtIndex:i];
		
		[nonStaticCollectable updateCollectablesPosition: player.velocity];
		[player checkPlayerTakesCollectable :nonStaticCollectable];
	}
	
	for(int i=0; i<nonStaticWeapons.count; i++) {
		WeaponItem *nonStaticWeapon = [nonStaticWeapons objectAtIndex:i];
		
		[nonStaticWeapon weaponsPositionPlayerBoostMovement: player.velocity: player.playerBoost :deltaTime];
		[nonStaticWeapon weaponsPositionAccelerometerInputIntoAccount: player.velocity: acceX];
		[nonStaticWeapon rotateWeapons: player.position];
	}
	
	for(int i=0; i<nonStaticCollectedWeapons.count; i++) {
		CollectedWeaponItem *nonStaticCollectedWeapon = [nonStaticCollectedWeapons objectAtIndex:i];
		[nonStaticCollectedWeapon update];
		[nonStaticCollectedWeapon holdUnactivatedCollectedWeapon: player.position];
		[nonStaticCollectedWeapon checkCollectedWeaponBoundaries];
		[nonStaticCollectedWeapon enemyMovementCollectableWeaponCollision: staticEnemies];
	}
	
	for (int i=0; i<flyingCollectables.count; i++) {
		CollectableItem *flyingCollectable = [flyingCollectables objectAtIndex:i];
		
		[flyingCollectable updateCollectablesPosition: player.velocity];
		[player checkPlayerTakesCollectable :flyingCollectable];
	}
	
	for(int i=0; i<flyingWeapons.count; i++) {
		WeaponItem *flyingWeapon = [flyingWeapons objectAtIndex:i];
		
		[flyingWeapon weaponsPositionPlayerBoostMovement: player.velocity: player.playerBoost :deltaTime];
		[flyingWeapon weaponsPositionAccelerometerInputIntoAccount: player.velocity: acceX];
		[flyingWeapon rotateWeapons: player.position];
	}
	
	for(int i=0; i<flyingCollectedWeapons.count; i++) {
		CollectedWeaponItem *flyingCollectedWeapon = [flyingCollectedWeapons objectAtIndex:i];
		[flyingCollectedWeapon update];
		[flyingCollectedWeapon holdUnactivatedCollectedWeapon: player.position];
		[flyingCollectedWeapon checkCollectedWeaponBoundaries];
		[flyingCollectedWeapon enemyMovementCollectableWeaponCollision: staticEnemies];
	}
	
	[player activateNonStaticWeaponsForRotationAroundPlayer: nonStaticWeapons];
	[player activateFlyingWeaponsForRotationAroundPlayer: flyingWeapons];
	[player showSelectedWeapons: nonStaticWeapons: flyingWeapons];
	[player changePlayerWeaponDefault];
	[player update];
	
	[player playerBoostMovement: deltaTime];
	[player updateAccelerometerInput: acceX];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	screenTouched = YES;
	return YES;
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	
	screenTouched = NO;
	
	CGPoint unconvertedLocation = [touch locationInView: [touch view]];
	CGPoint location = [[CCDirector sharedDirector] convertToGL: unconvertedLocation];
	
	if (![player touchedPlayer:location]) {
		[player activateNonStaticCollectedWeapons: nonStaticCollectedWeapons: location];
		[player deactivateNonStaticWeaponsForRotationAroundPlayer: nonStaticWeapons];
		[player activateFlyingCollectedWeapons: flyingCollectedWeapons: location];
		[player deactivateFlyingWeaponsForRotationAroundPlayer: flyingWeapons];
	}
	else if([player touchedPlayer:location]){
		[player changePlayerSelectedWeapon];
	}
	
}

-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	acceX = 0;
	if(acceleration.x > ACCELEROMETER_MIN_VALUE)
		acceX = 1;
	else if(acceleration.x < -ACCELEROMETER_MIN_VALUE)
		acceX = -1;
}

-(void) pauseGame: (id) sender {
	if ([[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] resume];
		[pauseButton setString:@"  pause"];
	}
	else if (![[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] pause];
		[pauseButton setString:@"resume"];
	}
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[staticEnemies release];
	[nonStaticCollectables release];
	[nonStaticWeapons release];
	[nonStaticCollectedWeapons release];
	[super dealloc];
}

@end
