//
//  GamePlayShortScene.h
//  WebX
//
//  Created by Sunny's Mac on 3/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "GamePlayMethods.h"
#import "ChaserItemShortGamePlayGrid.h"
#import "GlobalSingletonVars.h"

@interface GamePlayShortScene : CCLayer {
    
@private
	GamePlayMethods *gamePlayMethods;
	ChaserItemShortGamePlayGrid *chaserItemShortGamePlayGrid;
	PlayerItem *player;
	CCMenuItemFont *pauseButton;
	NSMutableArray *chasingEnemies;
	NSMutableArray *chasingEnemiesInShortGameplay;
	NSMutableArray *chasingEnemiesPlacementGrid;
	BOOL playerTouched;
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat acceX;
	ccTime deltaTime;
	ccTime accumulatedTime;
	ccTime totalTime;
	CGPoint touchedLocation;
	CGPoint previousPlayerPosition;
	CCLabelTTF *scoreLabel;
    NSArray *currentChaserItemGrid;
    NSMutableArray *currentChaserItemGridRects;
    CGPoint touchStartLocation;
    CGPoint touchMovedLocation;
    GlobalSingletonVars *sharedGlobalSingletonVarsInstance;
}

+(id) scene;
-(void) returnMainMenu: (id) sender;
-(void) returnMainGamePlay;
-(void) endGamePlay;

@end
