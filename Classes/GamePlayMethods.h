//
//  GamePlayMethods.h
//  WebX
//
//  Created by Sunny's Mac on 2/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerItem.h"
#import "FuelItem.h"
#import "BlockItem.h"
#import "EnemyItem.h"
#import "WeaponItem.h"
#import "CollectableItem.h"
#import "CollectedWeaponItem.h"
#import "GridItem.h"
#import "GlobalSingletonVars.h"

@class PlayerItem;
@class FuelItem;
@class BlockItem;
@class EnemyItem;
@class CollectableItem;

@interface GamePlayMethods : NSObject {
	
}

-(void) updateMaxFuelItemsThatCanBeTaken: (GlobalSingletonVars*) sharedGlobalVarsInstance;
-(void) checkPlayerTakesFuel: (PlayerItem*) player :(FuelItem*) fuelItem :(NSMutableArray*) enemies :(GlobalSingletonVars*) sharedGlobalVarsInstance;
-(void) updateFuelItems :(PlayerItem*) player :(NSMutableArray *)fuelItems :(NSMutableArray*) enemies :(GlobalSingletonVars*) sharedGlobalVarsInstance;
-(void) placeFuelItemsPattern1: (NSMutableArray *) fuelItems1;
-(BOOL) checkInactivePattern1: (NSMutableArray *) fuelItems1;
-(BOOL) checkChasingEnemiesNearPlayer: (PlayerItem*) player :(NSMutableArray*) chasingEnemies;
-(void) checkAndActivateBlock: (NSMutableArray *) blocks;
-(void) checkAndActivateBlocksInBackgroundLayer1: (NSMutableArray *)blocks;
-(void) playerMovementPlayerBlockCollision: (PlayerItem*)player :(BlockItem*)block;
-(void) playerMovementPlayerEnemiesCollision: (PlayerItem*)player :(EnemyItem *)enemy;
-(void) moveCollectablesTowardsPlayer: (PlayerItem*)player :(CollectableItem *)collectable;
-(void) checkPlayerTakesCollectable: (PlayerItem*)player :(CollectableItem *) collectable;
-(void) checkPlayerTakesCollectable: (PlayerItem*)player :(CollectableItem *) collectable: (NSMutableArray *) nonStaticWeapons: (NSMutableArray *) flyingWeapons;
-(void) resetNonStaticWeaponsForRotationAroundPlayer: (PlayerItem*)player :(NSMutableArray *) nonStaticWeapons;
-(void) deactivateNonStaticWeaponsForRotationAroundPlayer: (PlayerItem*)player :(NSMutableArray *) nonStaticWeapons;
-(void) resetFlyingWeaponsForRotationAroundPlayer: (PlayerItem*)player :(NSMutableArray *) flyingWeapons;
-(void) deactivateFlyingWeaponsForRotationAroundPlayer: (PlayerItem*)player :(NSMutableArray *) flyingWeapons;
-(void) activateNonStaticCollectedWeapons: (PlayerItem*)player :(NSMutableArray *) nonStaticCollectedWeapons: (CGPoint) clickedLocation;
-(void) activateFlyingCollectedWeapons: (PlayerItem*)player :(NSMutableArray *) flyingCollectedWeapons: (CGPoint) clickedLocation;
-(void) updateVacantGrids: (NSMutableArray *) chasingEnemiesPlacementGrid: (NSMutableArray *) chasingEnemies;
-(void) placeChasingEnemiesInTheGrid: (NSMutableArray *) chasingEnemiesPlacementGrid: (NSMutableArray *) chasingEnemies;
-(void) showSelectedWeapons: (PlayerItem*)player :(NSMutableArray *) nonStaticWeapons: (NSMutableArray *) flyingWeapons;
-(void) checkPlayerChaserItemGridCollisionShortGamePlay: (PlayerItem*)player :(NSMutableArray*) currentChaserItemGridRects;
-(void) updateScore: (PlayerItem*)player :(GlobalSingletonVars*) sharedGlobalVarsInstance;
-(void) updateFuelItemsShortGameplay: (GlobalSingletonVars*) sharedGlobalVarsInstance;
-(void) updateBarLength: (PlayerItem*)player :(GlobalSingletonVars*) sharedGlobalVarsInstance;
-(NSString*) displayScore:(GlobalSingletonVars*) sharedGlobalVarsInstance;

@end
