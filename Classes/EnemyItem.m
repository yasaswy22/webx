//
//  EnemyItem.m
//  WebProject
//
//  Created by Sunny's Mac on 12/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "EnemyItem.h"
#import "GlobalVars.h"
#import "CollectedWeaponItem.h"

@implementation EnemyItem

@synthesize velocity;
@synthesize acceleration;
@synthesize collisionForce;
@synthesize frictionalForce;

-(void) update {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	self.position = ccp(self.position.x+velocity.x, self.position.y+velocity.y);
}

-(void) updateEnemiesPosition: (CGPoint) targetVelocity {
	
		velocity = ccp(-targetVelocity.x, -targetVelocity.y);
		
		if (self.position.y < -self.contentSize.height*0.5 || 
			self.position.x < -self.contentSize.width*0.5 ||
			self.position.x > screenSize.width+self.contentSize.width*0.5) {
			
			[self resetEnemyPosition];
		}
		[self update];
}

-(void) staticEnemyFrictionalForce {
		
		CGFloat enemyCollisionForceValue = sqrt(self.collisionForce.x*self.collisionForce.x +
												self.collisionForce.y*self.collisionForce.y);
		CGFloat enemyFrictionalForceValue = sqrt(self.frictionalForce.x*self.frictionalForce.x +
												 self.frictionalForce.y*self.frictionalForce.y);		
		
		if (enemyCollisionForceValue > enemyFrictionalForceValue) {
			self.collisionForce = ccp(self.collisionForce.x - self.frictionalForce.x, 
									   self.collisionForce.y - self.frictionalForce.y);
			self.velocity = ccp(self.velocity.x + self.collisionForce.x, 
								 self.velocity.y + self.collisionForce.y);
			[self update];
		}
		else if(enemyCollisionForceValue <= enemyFrictionalForceValue){
			self.collisionForce = CGPointZero;
			self.frictionalForce = CGPointZero;
		}
}

-(void) resetEnemyPosition {
	int j = arc4random() % (int)(screenSize.width);
	int k = (int)screenSize.height + arc4random() % (int)(screenSize.height);
	
	self.position = ccp(j, k);
	collisionForce = CGPointZero;
	frictionalForce = CGPointZero;
}

-(void) resetEnemyPositionForBoost {
	int j = arc4random() % (int)(screenSize.width);
	int k = (int)screenSize.height*2.0 + arc4random() % (int)(screenSize.height);
	
	self.position = ccp(j, k);
	collisionForce = CGPointZero;
	frictionalForce = CGPointZero;
}

@end
