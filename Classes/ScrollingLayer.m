//
//  ScrollingLayer.m
//  WebX
//
//  Created by Sunny's Mac on 1/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ScrollingLayer.h"


@implementation ScrollingLayer

@synthesize velocity;

-(void) update {
	self.position = ccp(self.position.x+velocity.x, self.position.y+velocity.y);
}

@end
