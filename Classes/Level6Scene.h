//
//  Level6Scene.h
//  WebX
//
//  Created by Sunny's Mac on 1/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "FuelItem.h"
#import "ScrollingLayer.h"

@interface Level6Scene : CCLayer {

@private
	PlayerItem *player;
	FuelItem *fuelTank;
	CCMenuItemFont *pauseButton;
	NSMutableArray *staticEnemies;
	NSMutableArray *flyingEnemies;
	NSMutableArray *nonStaticEnemies;	
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat lor;
	ccTime deltaTime;
}

@end
