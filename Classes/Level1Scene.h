//
//  Level1Scene.h
//  WebX
//
//  Created by Sunny's Mac on 1/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "FuelItem.h"

@interface Level1Scene : CCLayer {

@private
	PlayerItem *player;
	FuelItem *fuelTank;
	CCMenuItemFont *pauseButton;
	NSMutableArray *staticEnemies;
	NSMutableArray *nonStaticEnemies;
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat acceX;
	ccTime deltaTime;
}

@end
