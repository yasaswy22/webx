//
//  FacebookViewController.h
//  WebX
//
//  Created by Sunny's Mac on 6/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"
#import <QuartzCore/QuartzCore.h>

@class PersonalDetailsTable;
@class FacebookTableCell;

@interface FacebookViewController : UIViewController <FBDialogDelegate, FBSessionDelegate, FBRequestDelegate>{
    Facebook *facebook;
    NSString *facebookAppId;
    IBOutlet UITableView *freindsList;
    IBOutlet FacebookTableCell *facebookTableCell;
    NSArray *fetchedFacebookEntries;
    PersonalDetailsTable *personalDetails;
    
    UIActivityIndicatorView *spinner;
    int timerForSpinning;
    
    NSManagedObjectContext *managedObjectContext;
}

@property (nonatomic, retain) UITableView *freindsList;
@property (nonatomic, retain) FacebookTableCell *facebookTableCell;
@property (nonatomic, retain) NSArray *fetchedFacebookEntries;
@property (nonatomic, retain) PersonalDetailsTable *personalDetails;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

- (IBAction)FacebookViewFinished:(id)sender;
-(void) resetTime:(NSTimer *)t;
-(void) showingloadingActivity;
-(void) notShowingloadingActivity;

-(void) synchronizeFacebook;
-(void) authorizeFacebook;
-(void) getPersonalProfileDetails;
-(void) getFriendsListDetails;
- (void) publishStreamPersonal;
- (void) publishStreamFriends;
-(void) postOnFaceBookPersonal;
-(void) postOnFaceBookFriends;
-(void) loadPersonalFacebookDetailsFromCoreData;
-(void) loadFriendsFacebookDetailsFromCoreData;

-(UIViewController*) getRootViewController;
-(void) presentViewController:(UIViewController*)vc;
-(void) dismissModalViewController;

@end
