//
//  Level8Scene.m
//  WebX
//
//  Created by Sunny's Mac on 1/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Level8Scene.h"
#import "MainMenuScene.h"
#import "GlobalVars.h"
#import "BlockItem.h"

@implementation Level8Scene

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* level8Scene = [CCScene node];
	Level8Scene* level8Layer = [Level8Scene node];
	[level8Scene addChild:level8Layer];
	return level8Scene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		screenSize = [[CCDirector sharedDirector] winSize];
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		self.isAccelerometerEnabled=YES;
		
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:15];
		
		CCMenuItemFont *exitButton = [CCMenuItemFont itemFromString:@"exit" target:self selector:@selector(returnMainMenu:)];
		exitButton.color = ccc3(TEXT_COLOR);
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5, 
								  screenSize.height - exitButton.contentSize.height * 0.5);
		
		pauseButton = [CCMenuItemFont itemFromString:@"  pause" target:self selector:@selector(pauseGame:)];
		pauseButton.color = ccc3(TEXT_COLOR);
		pauseButton.position = ccp(screenSize.width - pauseButton.contentSize.width * 0.5, 
								   pauseButton.contentSize.height * 0.5);
		
		CCMenu* menu = [CCMenu menuWithItems:exitButton, pauseButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu z:4];
		
		player = [PlayerItem spriteWithFile:@"Volleyball_Ball.png"];
		player.position = ccp(screenSize.width*0.5, MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		player.velocity = CGPointZero;
		player.acceleration = CGPointZero;
		player.decelaration = CGPointZero;
		player.velocity = ccp(0, PLAYER_INITIAL_VELOCITY);
		player.acceleration = ccp(0, PLAYER_INITIAL_ACCELERATION);
		[self addChild:player z:2];
		
		fuelTank = [FuelItem spriteWithFile:@"fuel_tank.png"];
		[fuelTank resetFuelTankPosition];
		[self addChild:fuelTank z:2];
		
		blocks = [[NSMutableArray alloc] init];
		for (int i=0; i<NUMBER_OF_BARS; i++) {
			
			NSString *fileName = @"bar";
			NSString *s1 = [NSString stringWithFormat:@"%i", i+1];
			fileName = [fileName stringByAppendingString:s1];
			fileName = [fileName stringByAppendingString:@".png"];
			
			BlockItem *block = [BlockItem spriteWithFile:fileName];
			block.position = ccp(screenSize.width*2.0, block.contentSize.height*0.5);
			block.active = NO;
			if (i+1 ==3 || i+1 == 6) {
				block.willKill = YES;
			}
			else {
				block.willKill = NO;
			}
			
			[blocks addObject:block];
			[self addChild:block z:2];
		}
		
		[player checkAndActivateBlock: blocks];
		
		[self scheduleUpdate];
		screenTouched = NO;
		accumulatedTime = 0;
	}
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	deltaTime = delta;
	
	if (screenTouched) {
		if (accumulatedTime > delta*1.5) {
			//CCLOG(@"accumulatedTime: %f, delta: %f", accumulatedTime, delta);
			[player powerUpForces];
			[player playerFrictionalForces];
			[fuelTank updateFuelTankPosition: player.velocity];
			[player checkPlayerTakesFuel: fuelTank];
			[player limitPlayerBoundaries];
			[player maintainPlayerAtMinHeight: deltaTime];  // should be before player block collision to bring back to min height
			
			for (int i =0; i<blocks.count; i++) {
				BlockItem *block = [blocks objectAtIndex:i];
				[block updateBlockPosition:player.velocity];
				[player playerMovementPlayerBlockCollision: block];
			}
			
			[player update];
			
			accumulatedTime = 0;
		}
		accumulatedTime += delta;
	}
	else {
		[player powerUpForces];
		[player playerFrictionalForces];
		[fuelTank updateFuelTankPosition: player.velocity];
		[player checkPlayerTakesFuel: fuelTank];
		[player limitPlayerBoundaries];
		[player maintainPlayerAtMinHeight: deltaTime];  // should be before player block collision to bring back to min height
		
		for (int i =0; i<blocks.count; i++) {
			BlockItem *block = [blocks objectAtIndex:i];
			[block updateBlockPosition:player.velocity];
			[player playerMovementPlayerBlockCollision: block];
		}
		[player update];
	}
	
	[player checkAndActivateBlock: blocks];
	[player playerBoostMovement: deltaTime];
	[player updateAccelerometerInput: acceX];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	screenTouched = YES;
	return YES;
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	screenTouched = NO;
}

-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	acceX = 0;
	if(acceleration.x > ACCELEROMETER_MIN_VALUE)
		acceX = ACCELEROMETER__VALUE;
	else if(acceleration.x < -ACCELEROMETER_MIN_VALUE)
		acceX = -ACCELEROMETER__VALUE;
}

-(void) pauseGame: (id) sender {
	if ([[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] resume];
		[pauseButton setString:@"  pause"];
	}
	else if (![[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] pause];
		[pauseButton setString:@"resume"];
	}
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[blocks release];
	[super dealloc];
}

@end