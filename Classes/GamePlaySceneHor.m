//
//  GamePlayScene.m
//  WebProject
//
//  Created by Sunny's Mac on 12/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GamePlaySceneHor.h"
#import "MainMenuScene.h"
#import "GameOverScene.h"

#define kMinimumGestureLength	10
#define kMaximumVariance		5

@implementation GamePlaySceneHor

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* gamePlayScene = [CCScene node];
	GamePlaySceneHor* gamePlayLayer = [GamePlaySceneHor node];
	[gamePlayScene addChild:gamePlayLayer];
	return gamePlayScene;
}

-(id) init {
	if((self = [super init])) {
		CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
		screenSize = [[CCDirector sharedDirector] winSize];
		
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		self.isAccelerometerEnabled=YES;
		
		CCTexture2D *backgroundTexture = [[CCTextureCache sharedTextureCache] addImage:@"rock_background_2.png"];
		CCTexture2D *sideWallLeftTexture = [[CCTextureCache sharedTextureCache] addImage:@"sidescroll_left_20.png"];
		CCTexture2D *sideWallRightTexture = [[CCTextureCache sharedTextureCache] addImage:@"sidescroll_right_20.png"];
		
		background1 = [CCSprite spriteWithTexture:backgroundTexture];
		background1.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
		[self addChild:background1 z:-1];
		
		background2 = [CCSprite spriteWithTexture:backgroundTexture];
		background2.position = ccp(screenSize.width*0.5, screenSize.height*1.5-1);
		[self addChild:background2 z:-1];
		
		sideWallLeft1 = [CCSprite spriteWithTexture:sideWallLeftTexture];
		sideWallLeft1.position = ccp(sideWallLeft1.contentSize.width*0.5, screenSize.height*0.5);
		[self addChild:sideWallLeft1 z:2];
		
		sideWallLeft2 = [CCSprite spriteWithTexture:sideWallLeftTexture];
		sideWallLeft2.position = ccp(sideWallLeft2.contentSize.width*0.5, screenSize.height*1.5-1);
		[self addChild:sideWallLeft2 z:2];
		
		sideWallRight1 = [CCSprite spriteWithTexture:sideWallRightTexture];
		sideWallRight1.position = ccp(screenSize.width - sideWallRight1.contentSize.width*0.5, screenSize.height*0.5);
		[self addChild:sideWallRight1 z:2];
		
		sideWallRight2 = [CCSprite spriteWithTexture:sideWallRightTexture];
		sideWallRight2.position = ccp(screenSize.width - sideWallRight2.contentSize.width*0.5, screenSize.height*1.5-1);
		[self addChild:sideWallRight2 z:2];
		
		CCMenuItemImage* exitButton = [CCMenuItemImage itemFromNormalImage:@"exit_brown.png" 
															 selectedImage:@"exit_brown.png"
																	target:self
																  selector:@selector(returnMainMenu:)];
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5,
								  screenSize.height - exitButton.contentSize.height * 0.5);
		CCMenu* menu = [CCMenu menuWithItems:exitButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu z:3];
		
		velocityLabel= [CCLabelTTF labelWithString:@"Nothing touched" fontName:@"Marker Felt" fontSize:13];
		velocityLabel.anchorPoint = ccp(0,1);
		velocityLabel.position = ccp(0, screenSize.height);
		[self addChild:velocityLabel z:2];
		
		accelerationLabel= [CCLabelTTF labelWithString:@"Nothing touched" fontName:@"Marker Felt" fontSize:13];
		accelerationLabel.anchorPoint = ccp(0,1);
		accelerationLabel.position = ccp(0, screenSize.height - (velocityLabel.contentSize.height+5));
		[self addChild:accelerationLabel z:2];
		
		decelerationLabel= [CCLabelTTF labelWithString:@"Nothing touched" fontName:@"Marker Felt" fontSize:13];
		decelerationLabel.anchorPoint = ccp(0,1);
		decelerationLabel.position = ccp(0, screenSize.height - (velocityLabel.contentSize.height+5+
																 accelerationLabel.contentSize.height+5));
		[self addChild:decelerationLabel z:2];
		
		fuelLabel= [CCLabelTTF labelWithString:@"Nothing touched" fontName:@"Marker Felt" fontSize:13];
		fuelLabel.anchorPoint = ccp(0,1);
		fuelLabel.position = ccp(0, screenSize.height - (velocityLabel.contentSize.height+5+
														 accelerationLabel.contentSize.height+5+
														 fuelLabel.contentSize.height+5));
		[self addChild:fuelLabel z:2];
		
		player = [PlayerItem spriteWithFile:@"hero.png"];
		player.position = ccp(screenSize.width*0.5, MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		player.velocity = CGPointZero;
		player.acceleration = CGPointZero;
		player.decelaration = CGPointZero;
		
		[self getPlayerInfluenceRect];
		
		player.velocity = ccp(0, PLAYER_INITIAL_VELOCITY);
		player.acceleration = ccp(0, PLAYER_INITIAL_ACCELERATION);
		[self addChild:player z:2];
		
		enemies = [[NSMutableArray alloc]init];
		CCTexture2D *enemyTexture = [[CCTextureCache sharedTextureCache] addImage: @"solder_small_2.png"];
		for (int i=0; i<NUMBER_OF_ENEMIES; i++) {
			EnemyItem *enemy = [EnemyItem spriteWithTexture:enemyTexture];

			int side = arc4random()%(int)2;
			int i = arc4random() % (int)(screenSize.width*0.2);
			int j = arc4random() % (int)(screenSize.height);
			
			if (side == 0) {
				enemy.position = ccp(i, j);
			}
			else if(side == 1) {
				enemy.position = ccp(screenSize.width - i, j);
			}
			
			[enemies addObject:enemy];
			[self addChild:enemy z:1];
		}
		
		enemyWeapons = [[NSMutableArray alloc]init];
		CCTexture2D *enemyWeaponTexture = [[CCTextureCache sharedTextureCache] addImage: @"enemy_weapon.png"];
		for (int i=0; i<NUMBER_OF_ENEMIES; i++) {
			EnemyWeaponItem *enemyWeapon = [EnemyWeaponItem spriteWithTexture:enemyWeaponTexture];
			EnemyItem *enemy = [enemies objectAtIndex:i];
			enemyWeapon.position = ccp(enemy.position.x, enemy.position.y);
			[enemyWeapons addObject:enemyWeapon];
			[self addChild:enemyWeapon z:1];
			enemyWeapon.visible = NO;
			enemyWeapon.weaponSpawned = NO;
		}
		
		[self scheduleUpdate];
		screenTouched = NO;
	}
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	[self updateEnemiesAndEnemyWeaponsPosition];
	[self updateBackgroundPosition];
	
	if (player.velocity.y == 0) {
		//CCLOG(@"Update: Player static");
		playerStatic = YES;
	}
	
	deltaTime = delta;
	
	[self powerUpForces];
	[self playerBoostMovement];
//	[self moveEnemiesTowardsPlayer];
	[self spawnEnemyWeaponsTowardsPlayerChasing];
//	[self spawnEnemyWeaponsTowardsPlayer];
	[self frictionalForces];
//	[self enemyForces];
//	[self horizontalForces];
	
	[player update];
	
	if(!CGPointEqualToPoint(player.velocity, CGPointZero)) {
		CGPoint pos = player.position;
		pos.x += lor*2 ;
		player.position = pos;
	}
	
//	[self checkEndGame];
	[self displayLabels];
}

-(void) updateEnemiesAndEnemyWeaponsPosition {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);

	for (int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];
		EnemyWeaponItem *enemyWeapon = [enemyWeapons objectAtIndex:i];

		//CCLOG(@"velocity %f, acceleration %f", player.velocity.y, player.acceleration.y);
		
		//Copy enemy velocity from player velocity. 
		//We use players velocity to move emenies down. Player always is at the center of the screen.
		enemy.velocity = ccp(-player.velocity.x, -player.velocity.y);
		enemyWeapon.velocity = ccp(-player.velocity.x, -player.velocity.y);
		
		//Reset enemy and enemy weapon position
		if (enemy.position.y < -enemy.contentSize.height*0.5 || enemyWeapon.position.y < -enemyWeapon.contentSize.height*0.5) {
			//CCLOG(@"enemy reset");
			int side = arc4random()%(int)2;
			int i = arc4random() % (int)(screenSize.width*0.2);
			int j = screenSize.height + arc4random() % (int)(screenSize.height*0.5);
			
			if (side == 0) {
				enemy.position = ccp(i, j);
			}
			else if(side == 1) {
				enemy.position = ccp(screenSize.width - i, j);
			}
			
			enemyWeapon.position = enemy.position;
			enemyWeapon.weaponSpawned = NO; // Resetting the weapon to be spawned the next time.
			enemyWeapon.reachedMaxLength = NO;
			enemyWeapon.intialEnemyWeaponPlayerInfluenceVelocity = CGPointZero;
			
//			// Acceleration(+) is back enemy cannot pull anymore..
//			player.acceleration = ccp(player.acceleration.x, player.acceleration.y+ENEMY_DECELERATION);			
		}
		[enemy update];
		[enemyWeapon update];
	}
}

-(void) moveEnemiesTowardsPlayer {
	[self getPlayerInfluenceRect];
	
	for (int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];
		EnemyWeaponItem *enemyWeapon = [enemyWeapons objectAtIndex:i];
		
		if (CGRectContainsPoint(playerInfluence, enemy.position)){
			
			CGPoint enemyPositionToPlayer = ccp(enemy.position.x - player.position.x, enemy.position.y - player.position.y);
			CGFloat enemyAngleToPlayer = ccpToAngle(enemyPositionToPlayer);
			
			CGPoint enemyPlayerInfluenceVelocity = ccp(ENEMY_VELOCITY_TOWARDS_PLAYER*cos(enemyAngleToPlayer), 
													   ENEMY_VELOCITY_TOWARDS_PLAYER*sin(enemyAngleToPlayer));
			
//			CCLOG(@"enemyPlayerInfluenceVelocity: (%f, %f)", 
//				  enemyPlayerInfluenceVelocity.x, 
//				  enemyPlayerInfluenceVelocity.y);
			
			enemy.position = ccp(enemy.position.x - enemyPlayerInfluenceVelocity.x, 
								 enemy.position.y - enemyPlayerInfluenceVelocity.y);

			if(!enemyWeapon.weaponSpawned)
			enemyWeapon.position = enemy.position;
		}
	}
}

-(void) spawnEnemyWeaponsTowardsPlayerChasing {
	[self getPlayerInfluenceRect];	
	
	for (int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];
		EnemyWeaponItem *enemyWeapon = [enemyWeapons objectAtIndex:i];
		if (CGRectContainsPoint(playerInfluence, enemy.position)) {
			
			CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
										   player.position.y - player.contentSize.height*0.5, 
										   player.contentSize.width, player.contentSize.height);
			
			CGRect enemyRect = CGRectMake(enemy.position.x - enemy.contentSize.width*0.5, 
										  enemy.position.y - enemy.contentSize.height*0.5, 
										  enemy.contentSize.width, enemy.contentSize.height);
			
			
			CGFloat threadLength = sqrt((enemyWeapon.position.x-enemy.position.x)*(enemyWeapon.position.x-enemy.position.x)+
										(enemyWeapon.position.y-enemy.position.y)*(enemyWeapon.position.y-enemy.position.y));
			
			if (CGRectContainsPoint(playerRect, enemyWeapon.position)) {
				enemyWeapon.visible = NO;
				enemyWeapon.position = player.position;
			}
			else if (CGRectContainsPoint(enemyRect, enemyWeapon.position) && enemyWeapon.reachedMaxLength) {
				enemyWeapon.visible = NO;
				enemyWeapon.position = enemy.position;
			}
			else if(threadLength > MAX_WEAPON_THREAD_LENGTH && !enemyWeapon.reachedMaxLength) {
				enemyWeapon.visible = YES;
				enemyWeapon.reachedMaxLength = YES;
			}
			else if(threadLength <= MAX_WEAPON_THREAD_LENGTH && !enemyWeapon.reachedMaxLength) {
				enemyWeapon.visible = YES;
				CGPoint enemyWeaponPositionToPlayer = ccp(enemyWeapon.position.x - player.position.x, enemyWeapon.position.y - player.position.y);
				CGFloat enemyWeaponAngleToPlayer = ccpToAngle(enemyWeaponPositionToPlayer);
				
				CGPoint enemyWeaponPlayerInfluenceVelocity = ccp(ENEMY_VELOCITY_TOWARDS_PLAYER*5*cos(enemyWeaponAngleToPlayer), 
																 ENEMY_VELOCITY_TOWARDS_PLAYER*5*sin(enemyWeaponAngleToPlayer));
				
				enemyWeapon.position = ccp(enemyWeapon.position.x - enemyWeaponPlayerInfluenceVelocity.x, enemyWeapon.position.y - enemyWeaponPlayerInfluenceVelocity.y);
			}
			else if(enemyWeapon.reachedMaxLength) {
				enemyWeapon.visible = YES;
				CGPoint enemyWeaponPositionToEnemy = ccp(enemyWeapon.position.x - enemy.position.x, enemyWeapon.position.y - enemy.position.y);
				CGFloat enemyWeaponAngleToEnemy = ccpToAngle(enemyWeaponPositionToEnemy);
				
				CGPoint enemyWeaponEnemyInfluenceVelocity = ccp(ENEMY_VELOCITY_TOWARDS_PLAYER*5*cos(enemyWeaponAngleToEnemy), 
																 ENEMY_VELOCITY_TOWARDS_PLAYER*5*sin(enemyWeaponAngleToEnemy));
				
				enemyWeapon.position = ccp(enemyWeapon.position.x - enemyWeaponEnemyInfluenceVelocity.x, enemyWeapon.position.y - enemyWeaponEnemyInfluenceVelocity.y);			}
		}
		else {
			enemyWeapon.visible = NO;
		}
		
	}
}

-(void) spawnEnemyWeaponsTowardsPlayer {
	[self getPlayerInfluenceRect];	
	
	for (int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];
		EnemyWeaponItem *enemyWeapon = [enemyWeapons objectAtIndex:i];
		
		// If enemy entered player region for first time spawn the weapon towards the player.
		if (CGRectContainsPoint(playerInfluence, enemy.position) && !enemyWeapon.weaponSpawned) {
			
			//CCLOG(@"Weapon spawning for first time");
			
			CGPoint enemyWeaponPositionToPlayer = ccp(enemyWeapon.position.x - player.position.x, 
													  enemyWeapon.position.y - player.position.y);
			CGFloat enemyWeaponAngleToPlayer = ccpToAngle(enemyWeaponPositionToPlayer);
			
			enemyWeapon.intialEnemyWeaponPlayerInfluenceVelocity = ccp(ENEMY_WEAPON_VELOCITY_TOWARDS_PLAYER*cos(enemyWeaponAngleToPlayer), 
																	   ENEMY_WEAPON_VELOCITY_TOWARDS_PLAYER*sin(enemyWeaponAngleToPlayer));
			
//			CCLOG(@"enemyWeapon.intialEnemyWeaponPlayerInfluenceVelocity: (%f, %f)", 
//														enemyWeapon.intialEnemyWeaponPlayerInfluenceVelocity.x, 
//														enemyWeapon.intialEnemyWeaponPlayerInfluenceVelocity.y);
			
			enemyWeapon.weaponSpawned = YES;
		}
		
		// If enemy already entered player region weapon is no longer haunting the player. It is moving only in the direction
		// when enemy first shot towards the palyer.
		else if(CGRectContainsPoint(playerInfluence, enemy.position) && enemyWeapon.weaponSpawned) {
			//CCLOG(@"Weapon spawned already");
			
			CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
										   player.position.y - player.contentSize.height*0.5, 
										   player.contentSize.width, player.contentSize.height);
			
			CGRect enemyRect = CGRectMake(enemy.position.x - enemy.contentSize.width*0.5, 
										   enemy.position.y - enemy.contentSize.height*0.5, 
										   enemy.contentSize.width, enemy.contentSize.height);
			
			
			CGFloat threadLength = sqrt((enemyWeapon.position.x-enemy.position.x)*(enemyWeapon.position.x-enemy.position.x)+
										(enemyWeapon.position.y-enemy.position.y)*(enemyWeapon.position.y-enemy.position.y));
			
			
			if (CGRectContainsPoint(playerRect, enemyWeapon.position)) {
				enemyWeapon.visible = NO;
				enemyWeapon.position = player.position;
			}
			else if (CGRectContainsPoint(enemyRect, enemyWeapon.position) && enemyWeapon.reachedMaxLength) {
				enemyWeapon.visible = NO;
				enemyWeapon.position = enemy.position;
			}
			else if(threadLength > MAX_WEAPON_THREAD_LENGTH && !enemyWeapon.reachedMaxLength) {
				enemyWeapon.visible = YES;
				enemyWeapon.reachedMaxLength = YES;
			}
			else if(threadLength <= MAX_WEAPON_THREAD_LENGTH && !enemyWeapon.reachedMaxLength) {
				enemyWeapon.visible = YES;
				enemyWeapon.position = ccp(enemyWeapon.position.x - enemyWeapon.intialEnemyWeaponPlayerInfluenceVelocity.x, 
										   enemyWeapon.position.y - enemyWeapon.intialEnemyWeaponPlayerInfluenceVelocity.y);
			}
			else if(enemyWeapon.reachedMaxLength) {
				enemyWeapon.visible = YES;
				enemyWeapon.position = ccp(enemyWeapon.position.x + enemyWeapon.intialEnemyWeaponPlayerInfluenceVelocity.x, 
										   enemyWeapon.position.y + enemyWeapon.intialEnemyWeaponPlayerInfluenceVelocity.y);
			}
		}
		
		// If enemy not in player influence donot make the weapon visible
		else {
			enemyWeapon.visible = NO;
		}
	}
}

-(void) updateBackgroundPosition{

	background1.position = ccp(background1.position.x, background1.position.y-player.velocity.y);
	background2.position = ccp(background2.position.x, background2.position.y-player.velocity.y);	
	sideWallLeft1.position = ccp(sideWallLeft1.position.x, sideWallLeft1.position.y-player.velocity.y);
	sideWallLeft2.position = ccp(sideWallLeft2.position.x, sideWallLeft2.position.y-player.velocity.y);	
	sideWallRight1.position = ccp(sideWallRight1.position.x, sideWallRight1.position.y-player.velocity.y);
	sideWallRight2.position = ccp(sideWallRight2.position.x, sideWallRight2.position.y-player.velocity.y);	
	
	if (background1.position.y <= -background1.contentSize.height*0.5) {
		background1.position = ccp(screenSize.width*0.5, background2.position.y+
														 background2.contentSize.height*0.5+
														 background1.contentSize.height*0.5-1);
	}

	if (background2.position.y <= -background2.contentSize.height*0.5) {
		background2.position = ccp(screenSize.width*0.5, background1.position.y+
														 background1.contentSize.height*0.5+
														 background2.contentSize.height*0.5-1);
	}

	if (sideWallLeft1.position.y <= -sideWallLeft1.contentSize.height*0.5) {
		sideWallLeft1.position = ccp(sideWallLeft1.contentSize.width*0.5, sideWallLeft2.position.y+
								   sideWallLeft2.contentSize.height*0.5+
								   sideWallLeft1.contentSize.height*0.5-1);
	}
	
	if (sideWallLeft2.position.y <= -sideWallLeft2.contentSize.height*0.5) {
		sideWallLeft2.position = ccp(sideWallLeft2.contentSize.width*0.5, sideWallLeft1.position.y+
								   sideWallLeft1.contentSize.height*0.5+
								   sideWallLeft2.contentSize.height*0.5-1);
	}
	
	if (sideWallRight1.position.y <= -sideWallRight1.contentSize.height*0.5) {
		sideWallRight1.position = ccp(screenSize.width-sideWallRight1.contentSize.width*0.5, sideWallRight2.position.y+
									 sideWallRight2.contentSize.height*0.5+
									 sideWallRight1.contentSize.height*0.5-1);
	}
	
	if (sideWallRight2.position.y <= -sideWallRight2.contentSize.height*0.5) {
		sideWallRight2.position = ccp(screenSize.width-sideWallRight2.contentSize.width*0.5, sideWallRight1.position.y+
									 sideWallRight1.contentSize.height*0.5+
									 sideWallRight2.contentSize.height*0.5-1);
	}

}

-(void) powerUpForces {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);

	if (player.fuel > 0) {
		// Acceleration due to player pressing the screen..
		player.acceleration = ccp(player.acceleration.x, player.acceleration.y + TOUCH_ACCELERATION);
	}
}

-(void) playerBoostMovement {
	
	if (playerBoost) {
		player.position = ccp(player.position.x, player.position.y+(TOUCH_PLAYER_VER_BOOST_ACCELERATION*deltaTime));
		if(player.position.y >= MAX_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT){
			playerBoost = NO;
			player.position = ccp(player.position.x, MAX_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		}
	}
	else if(!playerBoost) {
		player.position = ccp(player.position.x, player.position.y-(TOUCH_PLAYER_VER_BOOST_ACCELERATION*0.25*deltaTime));
		if(player.position.y <= MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT) {
			player.position = ccp(player.position.x, MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		}
	}
}

-(void) frictionalForces {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
//	// Deceleration(-) due to static friction..
//	if (playerStatic) {
//		if(player.velocity.y < PLAYER_MAX_VELOCITY *0.2){
//			player.decelaration = ccp(player.decelaration.x, player.decelaration.y+STATIC_FRICTIONAL_DECELERATION);
//		}
//		else if(player.velocity.y >= PLAYER_MAX_VELOCITY *0.2){
//			playerStatic = NO;
//			player.decelaration = ccp(player.decelaration.x, player.decelaration.y+DYNAMIC_FRICTIONAL_DECELERATION);
//		}
//	}
//			
//	// Deceleration due to dynamic friction..
//	else {
		player.decelaration = ccp(player.decelaration.x, player.decelaration.y+DYNAMIC_FRICTIONAL_DECELERATION);
//	}
}

-(void) enemyForces {
		// Deceleration(-) due to enemy trowing rope at the player..
	[self getPlayerInfluenceRect];	
	
	for (int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];
		if (CGRectContainsPoint(playerInfluence, enemy.position) && enemy.position.y < player.position.y) {
			player.decelaration = ccp(player.decelaration.x, player.decelaration.y+ENEMY_DECELERATION);
		}
	}
}

-(void) horizontalForces {
	
	// Player horizontal movement due to enemies.
	[self getPlayerInfluenceRect];
	for (int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];
		if (CGRectContainsPoint(playerInfluence, enemy.position)  && 
			enemy.position.y < player.position.y && 
			!CGPointEqualToPoint(player.velocity, CGPointZero)){
			if(enemy.position.x < player.position.x)
				player.position = ccp(player.position.x-ENEMY_HOR_DECELERATION, player.position.y);
			else if(enemy.position.x > player.position.x){
				player.position = ccp(player.position.x+ENEMY_HOR_DECELERATION, player.position.y);
			}
		}
	}
}

-(void) checkEndGame {
	if (CGPointEqualToPoint(player.velocity, CGPointZero) || 
							player.position.x < sideWallLeft1.contentSize.width || 
							player.position.x > screenSize.width - sideWallRight1.contentSize.width) {
		[[CCDirector sharedDirector] replaceScene: [GameOverScene scene]];
	}
}

-(void) draw {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[super draw];
	
	// draw a simple line
	// The default state is:
	// Line Width: 1
	// color: 255,255,255,255 (white, non-transparent)
	// Aliased
	glEnable(GL_LINE_SMOOTH);
	//glColor4ub(150, 100, 75, 255);  
	glColor4ub(0, 0, 0, 255);  
    glLineWidth(1.0f);
	
	[self getPlayerInfluenceRect];
	
	for (int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];
		EnemyWeaponItem *enemyWeapon = [enemyWeapons objectAtIndex:i];
		//CCLOG(@"enemy %i position: (%f, %f)", i, enemy.position.x, enemy.position.y);
		if (CGRectContainsPoint(playerInfluence, enemy.position)){
			ccDrawLine(enemy.position, enemyWeapon.position);
		}
	}
	//ccDrawCircle(player.position, PLAYER_INFLUENCE_RADIUS, CC_DEGREES_TO_RADIANS(360), 100, NO);
}

-(void) getPlayerInfluenceRect {
//	playerInfluence = CGRectMake(player.position.x - player.contentSize.width*PLAYER_INFLUENCE_SIZE*0.5, 
//								 player.position.y - player.contentSize.height*PLAYER_INFLUENCE_SIZE*0.5,
//								 player.contentSize.width*PLAYER_INFLUENCE_SIZE, player.contentSize.height*PLAYER_INFLUENCE_SIZE);

	playerInfluence = CGRectMake(0, player.position.y - player.contentSize.height*PLAYER_INFLUENCE_SIZE*0.5,
								 screenSize.width, player.contentSize.height*PLAYER_INFLUENCE_SIZE);
	
//	CCLOG(@"player		   : origin: (%f, %f), width: %f, height: %f", player.position.x, player.position.y, player.contentSize.width, player.contentSize.height);
//	CCLOG(@"playerInfluence: origin: (%f, %f), width: %f, height: %f", playerInfluence.origin.x, playerInfluence.origin.y, playerInfluence.size.width, playerInfluence.size.height);
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	
	player.fuel = PLAYER_MAX_FUEL;
	player.decelaration = CGPointZero;
	playerBoost = YES;
	
	return YES;
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	screenTouched = NO;
}

- (void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {

//	CGFloat acceX = (CGFloat)acceleration.x/20.0f;
//	particles[0]->_pos.x += dx;
	
	lor = 0;
	if(acceleration.x > 0)
		lor = 1;
	else if(acceleration.x < 0)
		lor = -1;
	
	//CCLOG(@"acceX: %i, acceleration.x: %f", acceX, acceleration.x);
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) displayLabels {
	velocityLabelText = @"vel: (";
	NSString *s1 = [NSString stringWithFormat:@"%i", (int)player.velocity.x];
	velocityLabelText = [velocityLabelText stringByAppendingString:s1];
	velocityLabelText = [velocityLabelText stringByAppendingString:@", "];
	NSString *s2 = [NSString stringWithFormat:@"%i", (int)player.velocity.y];
	velocityLabelText = [velocityLabelText stringByAppendingString:s2];
	velocityLabelText = [velocityLabelText stringByAppendingString:@")"];
	[velocityLabel setString:velocityLabelText];
	
	accelerationLabelText = @"acc: (";
	NSString *s3 = [NSString stringWithFormat:@"%f", (float)player.acceleration.x];
	accelerationLabelText = [accelerationLabelText stringByAppendingString:s3];
	accelerationLabelText = [accelerationLabelText stringByAppendingString:@", "];
	NSString *s4 = [NSString stringWithFormat:@"%f", (float)player.acceleration.y];
	accelerationLabelText = [accelerationLabelText stringByAppendingString:s4];
	accelerationLabelText = [accelerationLabelText stringByAppendingString:@")"];
	[accelerationLabel setString:accelerationLabelText];
	
	decelerationLabelText = @"dec: (";
	NSString *s5 = [NSString stringWithFormat:@"%f", (float)player.decelaration.x];
	decelerationLabelText = [decelerationLabelText stringByAppendingString:s5];
	decelerationLabelText = [decelerationLabelText stringByAppendingString:@", "];
	NSString *s6 = [NSString stringWithFormat:@"%f", (float)player.decelaration.y];
	decelerationLabelText = [decelerationLabelText stringByAppendingString:s6];
	decelerationLabelText = [decelerationLabelText stringByAppendingString:@")"];
	[decelerationLabel setString:decelerationLabelText];
	
	fuelLabelText = @"Fuel: ";
	NSString *s7 = [NSString stringWithFormat:@"%f", (CGFloat)player.fuel];
	fuelLabelText = [fuelLabelText stringByAppendingString:s7];
	[fuelLabel setString:fuelLabelText];
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[enemyWeapons release];
	[enemies release];
	[super dealloc];
}

@end
