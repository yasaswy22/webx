//
//  FlyingMountainItem.m
//  WebX
//
//  Created by Sunny's Mac on 8/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FlyingMountainItem.h"
#import "GlobalVars.h"
#define VEL_FLYINGMOUNTAINS_LAYER1 0.5
#define VEL_FLYINGMOUNTAINS_LAYER2 0.1

@implementation FlyingMountainItem

@synthesize velocity;

-(void) update{
    self.position = ccp(self.position.x+self.velocity.x, self.position.y+self.velocity.y);
    if (self.position.y <= -self.contentSize.height*0.5) {
        //[self setPositionInAnyQuadrant];
        [self setPositioning];
    }
}

-(void) setPositionInAnyQuadrant {
    int quadrant = arc4random() % 4;
    int j, k;
    
    if (quadrant == 0) {
        j = arc4random() % (int)(screenSize.width*0.5);
        k = (int)(screenSize.height)+(int)self.contentSize.height+arc4random() % (int)(screenSize.height*0.5);
    }
    else if (quadrant == 1) {
        j = (int)(screenSize.width*0.5) + arc4random() % (int)(screenSize.width*0.5);
        k = (int)(screenSize.height)+(int)self.contentSize.height+arc4random() % (int)(screenSize.height*0.5);
    }
    else if (quadrant == 2) {
        j = (int)(screenSize.width*0.5) + arc4random() % (int)(screenSize.width*0.5);
        k = (int)(screenSize.height)+(int)self.contentSize.height+(int)(screenSize.height*0.5) + arc4random() % (int)(screenSize.height*0.5);
    }
    else if (quadrant == 3) {
        j = arc4random() % (int)(screenSize.width*0.5);
        k = (int)(screenSize.height)+(int)self.contentSize.height+(int)(screenSize.height*0.5) + arc4random() % (int)(screenSize.height*0.5);
    }
    
    self.position = ccp(j, k);
}

-(void) setPositioning {
    int j, k;
        
    j = arc4random() % (int)(screenSize.width);
    k = (int)screenSize.height+(int)self.contentSize.height+arc4random() % (int)(screenSize.height);
    
    self.position = ccp(j, k);
}

-(void) setPositioningInitiallyLayer1Rocks {
    int j, k;
    
    j = arc4random() % (int)(screenSize.width);
    k = (int)self.contentSize.height+arc4random() % (int)(screenSize.height);
    [self setScaling];
    self.position = ccp(j, k);
    self.velocity = ccp(self.velocity.x, -VEL_FLYINGMOUNTAINS_LAYER1);
}

-(void) setPositioningInitiallyLayer2Rocks {
    int j, k;
    
    j = arc4random() % (int)(screenSize.width);
    k = (int)self.contentSize.height+arc4random() % (int)(screenSize.height);
    [self setScaling];
    self.position = ccp(j, k);
    self.velocity = ccp(self.velocity.x, -VEL_FLYINGMOUNTAINS_LAYER2);
}

-(void) setScaling{
    int randomScaleFactor;
    randomScaleFactor = 50 + arc4random() %  50;
    [self setScale:randomScaleFactor * 0.01];
}

-(void) setPositionInAnyQuadrantInitially {
    int quadrant = arc4random() % 4;
    int j, k;
    
    if (quadrant == 0) {
        j = arc4random() % (int)(screenSize.width*0.5);
        k = arc4random() % (int)(screenSize.height*0.5);
    }
    else if (quadrant == 1) {
        j = (int)(screenSize.width*0.5) + arc4random() % (int)(screenSize.width*0.5);
        k = arc4random() % (int)(screenSize.height*0.5);
    }
    else if (quadrant == 2) {
        j = (int)(screenSize.width*0.5) + arc4random() % (int)(screenSize.width*0.5);
        k = (int)(screenSize.height*0.5) + arc4random() % (int)(screenSize.height*0.5);
    }
    else if (quadrant == 3) {
        j = arc4random() % (int)(screenSize.width*0.5);
        k = (int)(screenSize.height*0.5) + arc4random() % (int)(screenSize.height*0.5);
    }
    
    self.position = ccp(j, k);
}

@end
