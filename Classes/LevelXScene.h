//
//  LevelXScene.h
//  WebX
//
//  Created by Sunny's Mac on 1/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "GamePlayMethods.h"
#import "ChaserItemShortGamePlayGrid.h"

@interface LevelXScene : CCLayer {

@private
	GamePlayMethods *gamePlayMethods;
	ChaserItemShortGamePlayGrid *chaserItemShortGamePlayGrid;
	PlayerItem *player;
	CCMenuItemFont *pauseButton;
	NSMutableArray *fuelItems1;
	NSMutableArray *staticEnemies;
	NSMutableArray *chasingEnemies;
	NSMutableArray *chasingEnemiesInShortGameplay;
	NSMutableArray *nonStaticCollectables;
	NSMutableArray *nonStaticWeapons;
	NSMutableArray *nonStaticCollectedWeapons;
	NSMutableArray *flyingCollectables;
	NSMutableArray *flyingWeapons;
	NSMutableArray *flyingCollectedWeapons;
	NSMutableArray *blocks;
	NSMutableArray *chasingEnemiesPlacementGrid;
	BOOL playerTouched;
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat acceX;
	ccTime deltaTime;
	ccTime accumulatedTime;
	ccTime totalTime;
	CGPoint touchedLocation;
	CGPoint previousPlayerPosition;
	CGFloat barLength;
	CCLabelTTF *scoreLabel;
	NSString *scoreLabelText;
    NSArray *currentChaserItemGrid;
    NSMutableArray *currentChaserItemGridRects;
    CGPoint touchStartLocation;
    CGPoint touchMovedLocation;
}

+(id) scene;
-(void) displayScore: (CGFloat) score;
-(void) returnMainMenu: (id) sender;
-(void) returnMainGamePlay;

@end
