//
//  FacebookTable.m
//  WebX
//
//  Created by Sunny's Mac on 7/5/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FacebookTable.h"
#import "GameCenterFacebookTable.h"


@implementation FacebookTable
@dynamic facebookPic;
@dynamic facebookName;
@dynamic facebookUUID;
@dynamic playingThisGame;
@dynamic facebookGCRelation;

+(FacebookTable *) entryWithFacebookData:(NSDictionary *)facebookData inManagedObjectContext:(NSManagedObjectContext *)context{
    
    FacebookTable *facebookEntry = nil;
//    NSLog(@"Facebook Data: %@", [facebookData description]);
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"FacebookTable" inManagedObjectContext:context]];
    [request setPredicate:[NSPredicate predicateWithFormat:@"facebookUUID = %@", 
                           [facebookData objectForKey:@"uid"]]];
    NSError *error = nil;
    facebookEntry = [[context executeFetchRequest:request error:&error] lastObject];
    
    if (!error && !facebookEntry) {
//        NSLog(@"New Facebook Entry");
        facebookEntry = [NSEntityDescription insertNewObjectForEntityForName:@"FacebookTable" inManagedObjectContext:context];
        
        facebookEntry.facebookName = [NSString stringWithString:[facebookData valueForKey:@"name"]];
        facebookEntry.facebookUUID = (NSNumber *)[facebookData valueForKey:@"uid"];
        facebookEntry.facebookPic = [NSData dataWithContentsOfURL:
                                     [NSURL URLWithString:[facebookData valueForKey:@"pic_square"]]];
        if ([[facebookData valueForKey:@"is_app_user"] intValue] == 1) {
//            NSLog(@"New Facebook Entry, Playing this game");
            facebookEntry.playingThisGame = [NSNumber numberWithBool:YES];
        }
        else{
//            NSLog(@"New Facebook Entry, Not playing this game");
            facebookEntry.playingThisGame = [NSNumber numberWithBool:NO];
        }
    }
    else if (!error && 
             facebookEntry && 
             !facebookEntry.playingThisGame && 
             [[facebookData valueForKey:@"is_app_user"] intValue] == 1) {
//        NSLog(@"Old Facebook Entry, jut updating his playing status");
        [facebookEntry setValue:[NSNumber numberWithBool:YES] forKey:@"playingThisGame"];
    }
    
    // Commit the change.
    if (![context save:&error]) {
        // Handle the error.
        NSLog(@"Saving changes failed: %@", error);
        
    }
    
    return facebookEntry;
}

@end
