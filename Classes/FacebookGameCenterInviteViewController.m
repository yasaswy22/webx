//
//  FacebookGameCenterInviteViewController.m
//  WebX
//
//  Created by Sunny's Mac on 7/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FacebookGameCenterInviteViewController.h"
#import "FacebookTable.h"
#import "PersonalDetailsTable.h"
#import "WebXAppDelegate.h"
#define FRIENDS_PAGE_MULTIPLE 0.5
#define SIZE_OF_ROW_IN_TABLE_VIEW 64*FRIENDS_PAGE_MULTIPLE
#define FONT_OF_NAME_IN_TABLE_VIEW 10*FRIENDS_PAGE_MULTIPLE

@implementation FacebookGameCenterInviteViewController

@synthesize freindsList;
@synthesize fetchedFacebookEntries;
@synthesize selectGameCenterIDView;
@synthesize managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.managedObjectContext = [(WebXAppDelegate*)[UIApplication sharedApplication].delegate managedObjectContext];
        
        [NSTimer scheduledTimerWithTimeInterval: 0.2
                                         target: self
                                       selector: @selector(resetTime:) 
                                       userInfo: nil repeats:YES];
        
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinner.center = CGPointMake(self.view.bounds.size.width*0.5, self.view.bounds.size.height*0.5);
        [self.view addSubview:spinner];
        [self showingloadingActivity];
        
        [NSTimer scheduledTimerWithTimeInterval: 1
                                         target: self
                                       selector: @selector(loadFriendsFacebookDetailsFromCoreData) 
                                       userInfo: nil repeats:YES];
        
//        [self loadFriendsFacebookDetailsFromCoreData];
    }
    
    return self;
}

- (void)dealloc
{
    [self.freindsList release];
    [self.managedObjectContext release];
    [self.fetchedFacebookEntries release];    
    [spinner release];
    [selectGameCenterIDView release];
    [super dealloc];
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Activity Indicator

-(void) resetTime:(NSTimer *)t{
    if (timerForSpinning > 10) {
        [self notShowingloadingActivity];
    }
    else{
        timerForSpinning += 1;
        [self showingloadingActivity];
    }
}

-(void) showingloadingActivity{
    [spinner startAnimating];
    self.view.userInteractionEnabled = NO;
}

-(void) notShowingloadingActivity{
    [spinner stopAnimating];
    self.view.userInteractionEnabled = YES;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self.freindsList release];
    self.freindsList = nil;
    [self.managedObjectContext release];
    self.managedObjectContext = nil;
    [self.fetchedFacebookEntries release];
    self.fetchedFacebookEntries = nil;
    [spinner release];
    [selectGameCenterIDView release];
    selectGameCenterIDView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark UITableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    if ([fetchedFacebookEntries count] == 0) {
        return 1;
    }
    else {
        return [fetchedFacebookEntries count];            
    }
    
    return 0;
}

-(NSString *) tableView:(UITableView *)tableView nameAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.fetchedFacebookEntries lastObject] != nil) {
        FacebookTable *facebookData = [self.fetchedFacebookEntries objectAtIndex:indexPath.row];
        return facebookData.facebookName;
    }
    else{
        return @"You have no friends...";
    }
    return nil;
}

- (UIImage*)image: (UIImage *)image scaleToSize:(CGSize)size  {
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, size.width, size.height), [image CGImage]);
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

-(UIImage *) tableView:(UITableView *)tableView pictureAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.fetchedFacebookEntries lastObject] != nil) {
        FacebookTable *facebookData = [self.fetchedFacebookEntries objectAtIndex:indexPath.row];
        UIImage *tempImg = [UIImage imageWithData:facebookData.facebookPic];
        return [self image:tempImg scaleToSize:CGSizeMake(tempImg.size.width*FRIENDS_PAGE_MULTIPLE, 
                                                          tempImg.size.height*FRIENDS_PAGE_MULTIPLE)];     
    }
    UIImage *lodingImage = [UIImage imageNamed:@"Question_Mark_Icon_BlueGlow"];
    return [self image:lodingImage scaleToSize:CGSizeMake(lodingImage.size.width*FRIENDS_PAGE_MULTIPLE, 
                                                          lodingImage.size.height*FRIENDS_PAGE_MULTIPLE)];   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SIZE_OF_ROW_IN_TABLE_VIEW;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FacebookTableViewCellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:YES animated:YES];
    cell.textLabel.font = [UIFont fontWithName:nil size:FONT_OF_NAME_IN_TABLE_VIEW];
    cell.textLabel.text = [self tableView:tableView nameAtIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellEditingStyleNone;
    
    [cell setAccessoryType:UITableViewCellAccessoryDetailDisclosureButton];
    
    return cell;
}


//-(void) referenceTime
//{
//    UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"hi" message:@"hi" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
//    [al show];
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{ 
//    [self postOnFaceBookFriends];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{ 
    [[NSBundle mainBundle] loadNibNamed:@"FacebookGameCenterSelectionView" owner:self options:nil];
    [self.view addSubview:self.selectGameCenterIDView];
}

#pragma mark Facebook methods

- (IBAction)FacebookViewFinished:(id)sender {
    
    // Saves changes in the application's managed object context before the application terminates.
    
    [self dismissModalViewController];
}

-(void) loadFriendsFacebookDetailsFromCoreData {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"FacebookTable" 
                                   inManagedObjectContext:self.managedObjectContext]];
    [request setPredicate:nil];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"facebookName" ascending:YES];
    [request setPredicate:[NSPredicate predicateWithFormat:@"playingThisGame = %@", 
                           [NSNumber numberWithBool:YES]]];
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
    
    NSError *error = nil;
    [self.fetchedFacebookEntries release];
    self.fetchedFacebookEntries  = [[self.managedObjectContext executeFetchRequest:request error:&error] retain];
    [request release];
    
    [self.freindsList reloadData];
}

#pragma mark Views (Facebook View)

// Helper methods

-(UIViewController*) getRootViewController
{
	return [UIApplication sharedApplication].keyWindow.rootViewController;
}

-(void) presentViewController:(UIViewController*)vc
{
	UIViewController* rootVC = [self getRootViewController];
	[rootVC presentModalViewController:vc animated:YES];
}

-(void) dismissModalViewController
{
	UIViewController* rootVC = [self getRootViewController];
	[rootVC dismissModalViewControllerAnimated:YES];
}

@end
