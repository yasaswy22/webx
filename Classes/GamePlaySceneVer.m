//
//  GamePlaySceneVer.m
//  WebX
//
//  Created by Sunny's Mac on 12/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GamePlaySceneVer.h"
#import "MainMenuScene.h"

@implementation GamePlaySceneVer

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* gamePlayScene = [CCScene node];
	GamePlaySceneVer* gamePlayLayer = [GamePlaySceneVer node];
	[gamePlayScene addChild:gamePlayLayer];
	return gamePlayScene;
}

-(id) init {
	if((self = [super init])) {
		CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
		screenSize = [[CCDirector sharedDirector] winSize];
		
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		
		CCMenuItemImage* exitButton = [CCMenuItemImage itemFromNormalImage:@"exit.png" 
															 selectedImage:@"exit.png"
																	target:self
																  selector:@selector(returnMainMenu:)];
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5,
								  screenSize.height - exitButton.contentSize.height * 0.5);
		CCMenu* menu = [CCMenu menuWithItems:exitButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu z:3];
		
		velocityLabel= [CCLabelTTF labelWithString:@"Nothing touched" fontName:@"Marker Felt" fontSize:13];
		velocityLabel.anchorPoint = ccp(0,1);
		velocityLabel.position = ccp(0, screenSize.height);
		[self addChild:velocityLabel z:2];
		
		accelerationLabel= [CCLabelTTF labelWithString:@"Nothing touched" fontName:@"Marker Felt" fontSize:13];
		accelerationLabel.anchorPoint = ccp(0,1);
		accelerationLabel.position = ccp(0, screenSize.height - (velocityLabel.contentSize.height+5));
		[self addChild:accelerationLabel z:2];
		
		player = [PlayerItem spriteWithFile:@"monkey.png"];
		player.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
		player.velocity = CGPointZero;
		player.acceleration = CGPointZero;
		//		player.velocity = ccp(0, PLAYER_INITIAL_VELOCITY);
		//		player.acceleration = ccp(0, PLAYER_INITIAL_ACCELERATION);
		[self addChild:player z:2];
		
		enemies = [[NSMutableArray alloc]init];
		CCTexture2D *enemyTexture = [[CCTextureCache sharedTextureCache] addImage: @"enemy.png"];
		for (int i=0; i<NUMBER_OF_ENEMIES; i++) {
			EnemyItem *enemy = [EnemyItem spriteWithTexture:enemyTexture];
			int i = arc4random() % (int)screenSize.width;
			int j = arc4random() % (int)screenSize.height;
			enemy.position = ccp(i, j);
			[enemies addObject:enemy];
			[self addChild:enemy z:1];
		}
		
		[self scheduleUpdate];
		screenTouched = NO;
	}
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	if (player.velocity.y == 0) {
		//CCLOG(@"Update: Player static");
		playerStatic = YES;
	}
	
	if (screenTouched) {
		[self appliedForces];
	}
	
	[self otherForces];
	[player update];
	[self updateEnemiesPosition];
	[self scaleEnemies];
	[self displayLabels];
}

-(void) updateEnemiesPosition {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	for (int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];
		
		//CCLOG(@"velocity %f, acceleration %f", player.velocity.y, player.acceleration.y);
		
		//Copy enemy velocity from player velocity. 
		//We use players velocity to move emenies down. Player always is at the center of the screen.
		
		CGFloat playerVelocity = player.velocity.y;
		CGPoint screenCenter = ccp(screenSize.width*0.5, screenSize.height*0.5);
		CGPoint enemyPositionCentered = ccp(enemy.position.x - screenCenter.x, enemy.position.y - screenCenter.y);
		
		CGFloat enemyAngleCentered = ccpToAngle(enemyPositionCentered);
		//CCLOG(@"Angle centered: %f", enemyAngleCentered);
		
		enemy.velocity = ccp(playerVelocity*cos(enemyAngleCentered), playerVelocity*sin(enemyAngleCentered));
		
		//Reset enemy position if it crosses any of the borders
		if (enemy.position.y < -enemy.contentSize.height*0.5 ||
			enemy.position.y > screenSize.height+enemy.contentSize.height*0.5 ||
			enemy.position.x < -enemy.contentSize.width*0.5 ||
			enemy.position.x > screenSize.width+enemy.contentSize.width*0.5) {
			//CCLOG(@"enemy reset");
			int i = screenSize.width*0.4 + arc4random() % (int)(screenSize.width*0.2); 
			int j = screenSize.height*0.4 + arc4random() % (int)(screenSize.height*0.2);
			enemy.position = ccp(i, j);
			//			// Acceleration(+) is back enemy cannot pull anymore..
			//			player.acceleration = ccp(player.acceleration.x, player.acceleration.y+ENEMY_DECELERATION);			
		}
		[enemy update];
	}
}

-(void) scaleEnemies {
	for (int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];

		CGFloat distanceFromCenterX = fabsf(screenSize.width*0.5 - enemy.position.x);
		CGFloat distanceFromCenterY = fabsf(screenSize.height*0.5 - enemy.position.y);
		//CCLOG(@"Enemy %i distance: %f screenSize.height*0.5: %f", i, distanceFromCenter, screenSize.height*0.5);
		
		CGFloat distanceFromCenter = sqrt(distanceFromCenterX*distanceFromCenterX + distanceFromCenterY*distanceFromCenterY);
		
		CGFloat scaleValue = distanceFromCenter / (screenSize.height*0.5);
		//CCLOG(@"Enemy %i scale: %f", i, scaleValue);
		[enemy setScale:scaleValue];
		
	}	
}

-(void) appliedForces {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	if(player.velocity.y <= PLAYER_MAX_VELOCITY_VER && player.acceleration.y <=PLAYER_MAX_ACCELERATION_VER) {
		// Acceleration(+) due to player pressing the screen..
		player.acceleration = ccp(player.acceleration.x, player.acceleration.y+PLAYER_ACCELERATION_VER);
	}
	else if (player.velocity.y > PLAYER_MAX_VELOCITY_VER) {
		player.velocity = ccp(player.velocity.x, PLAYER_MAX_VELOCITY_VER);
	}
}

-(void) otherForces {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	//Player reached top speed
	if (player.velocity.y > PLAYER_MAX_VELOCITY_VER) {
		player.velocity = ccp(player.velocity.x, PLAYER_MAX_VELOCITY_VER);
	}
	
	if(player.velocity.y > 0 && player.velocity.y <= PLAYER_MAX_VELOCITY_VER){
		
		// Deceleration(-) due to friction..
		if (playerStatic) {
			if(player.velocity.y < PLAYER_MAX_VELOCITY_VER *0.2){
				player.acceleration = ccp(player.acceleration.x, player.acceleration.y-STATIC_FRICTIONAL_DECELERATION_VER);
			}
			else if(player.velocity.y > PLAYER_MAX_VELOCITY_VER *0.2){
				playerStatic = NO;
				player.acceleration = ccp(player.acceleration.x, player.acceleration.y-DYNAMIC_FRICTIONAL_DECELERATION_VER);
			}
		}
		else {
			player.acceleration = ccp(player.acceleration.x, player.acceleration.y-DYNAMIC_FRICTIONAL_DECELERATION_VER);
		}
		
		// Deceleration(-) due to enemy trowing rope at the player..
		for (int i=0; i<enemies.count; i++) {
			EnemyItem *enemy = [enemies objectAtIndex:i];
			if(enemy.position.y <= player.position.y){
				player.acceleration = ccp(player.acceleration.x, player.acceleration.y-ENEMY_DECELERATION_VER);
			}
		}
	}
	
	//Player has stopped
	else if(player.velocity.y < 0) {
		player.velocity = CGPointZero;
		player.acceleration = CGPointZero;
	}
	
	//player.velocity = ccp(player.velocity.x+player.acceleration.x, player.velocity.y+player.acceleration.y);
}

-(void) draw {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[super draw];
	
	// draw a simple line
	// The default state is:
	// Line Width: 1
	// color: 255,255,255,255 (white, non-transparent)
	// Aliased
	glEnable(GL_LINE_SMOOTH);
	glColor4ub(125, 100, 100, 255);  
    glLineWidth(1.0f);
	
	for (int i=0; i<enemies.count; i++) {
		EnemyItem *enemy = [enemies objectAtIndex:i];
		//CCLOG(@"enemy %i position: (%f, %f)", i, enemy.position.x, enemy.position.y);
		if(enemy.position.y < screenSize.height*0.2 ||
		   enemy.position.y > screenSize.height*0.8 ||
		   enemy.position.x < screenSize.width*0.2 ||
		   enemy.position.x > screenSize.width*0.8){
			ccDrawLine(player.position, enemy.position);
		}
	}
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	screenTouched = YES;
	return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	screenTouched = NO;
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) displayLabels {
	velocityLabelText = @"vel: (";
	NSString *s1 = [NSString stringWithFormat:@"%i", (int)player.velocity.x];
	velocityLabelText = [velocityLabelText stringByAppendingString:s1];
	velocityLabelText = [velocityLabelText stringByAppendingString:@", "];
	NSString *s2 = [NSString stringWithFormat:@"%i", (int)player.velocity.y];
	velocityLabelText = [velocityLabelText stringByAppendingString:s2];
	velocityLabelText = [velocityLabelText stringByAppendingString:@")"];
	[velocityLabel setString:velocityLabelText];
	
	accelerationLabelText = @"acc: (";
	NSString *s3 = [NSString stringWithFormat:@"%0.2f", (float)player.acceleration.x];
	accelerationLabelText = [accelerationLabelText stringByAppendingString:s3];
	accelerationLabelText = [accelerationLabelText stringByAppendingString:@", "];
	NSString *s4 = [NSString stringWithFormat:@"%0.2f", (float)player.acceleration.y];
	accelerationLabelText = [accelerationLabelText stringByAppendingString:s4];
	accelerationLabelText = [accelerationLabelText stringByAppendingString:@")"];
	[accelerationLabel setString:accelerationLabelText];
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[enemies release];
	[super dealloc];
}

@end
