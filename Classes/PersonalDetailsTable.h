//
//  PersonalDetailsTable.h
//  WebX
//
//  Created by Sunny's Mac on 6/29/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PersonalDetailsTable : NSManagedObject {
@private
}
@property (nonatomic, retain) NSString * facebookName;
@property (nonatomic, retain) NSData * facebookPic;
@property (nonatomic, retain) NSNumber * facebookUUID;
@property (nonatomic, retain) NSString * gameCenterAlias;
@property (nonatomic, retain) NSString * gameCenterEmail;
@property (nonatomic, retain) NSNumber * gameCenterScore;
@property (nonatomic, retain) NSString * gameCenterID;

+(PersonalDetailsTable *) entryOrUpdateWithFacebookData:(NSDictionary *)facebookData inManagedObjectContext:(NSManagedObjectContext *)context;

+(PersonalDetailsTable *) entryOrUpdateWithGameCenterAlias:(NSString *)gameCenterAlias entryWithGameCenterID:(NSString *)gameCenterID inManagedObjectContext:(NSManagedObjectContext *)context;

+(PersonalDetailsTable *) updateGameCenterScore:(NSNumber *)gameCenterScore 
                                forGameCenterID:(NSString *)gameCenterID 
                             forGameCenterAlias:(NSString *)gameCenterAlias 
                         inManagedObjectContext:(NSManagedObjectContext *)context;

@end
