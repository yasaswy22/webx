//
//  FacebookGameCenterInviteViewController.h
//  WebX
//
//  Created by Sunny's Mac on 7/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"

@interface FacebookGameCenterInviteViewController : UIViewController {
    IBOutlet UITableView *freindsList;
    NSArray *fetchedFacebookEntries;
    
    IBOutlet UIPickerView *selectGameCenterIDView;

    UIActivityIndicatorView *spinner;
    int timerForSpinning;
    
    NSManagedObjectContext *managedObjectContext;
    
}

@property (nonatomic, retain) UITableView *freindsList;
@property (nonatomic, retain) NSArray *fetchedFacebookEntries;
@property (nonatomic, retain) UIPickerView *selectGameCenterIDView;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

- (IBAction)FacebookViewFinished:(id)sender;
-(void) resetTime:(NSTimer *)t;
-(void) showingloadingActivity;
-(void) notShowingloadingActivity;

-(void) loadFriendsFacebookDetailsFromCoreData;

-(UIViewController*) getRootViewController;
-(void) presentViewController:(UIViewController*)vc;
-(void) dismissModalViewController;



@end
