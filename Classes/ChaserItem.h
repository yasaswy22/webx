//
//  ChaserItem.h
//  WebX
//
//  Created by Sunny's Mac on 1/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"

@class PlayerItem;

@interface ChaserItem : CCSprite {

@private
	CGPoint velocity;
	BOOL direction; //NO is down, YES is up.
	CGFloat maxAllowedHeightFromBottom;
	BOOL leftRight;
	ccTime vibratingTime;
	BOOL active;
}

@property CGPoint velocity;
@property BOOL direction;
@property CGFloat maxAllowedHeightFromBottom;
@property BOOL leftRight;
@property ccTime vibratingTime;
@property BOOL active;

-(void) update;
-(void) updateChasingEnemyMovement: (CGPoint) playerVelocity;
-(void) moveChasingEnemiesTowardsPlayer: (PlayerItem *) player;
-(void) chasingEnemyBackingMovement: (PlayerItem *) player:(ccTime) deltaTime;
-(void) vibrateChasingEnemies: (ccTime) deltaTime;
-(void) playerBoostChasingEnemiesMovement: (CGPoint) playerVelocity;
-(void) makeChaserEnemyInactive;
//-(void) checkEndGame: (PlayerItem *) player;

@end
