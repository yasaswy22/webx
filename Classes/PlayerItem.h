//
//  PlayerItem.h
//  WebProject
//
//  Created by Sunny's Mac on 12/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GlobalVars.h"

@interface PlayerItem : CCSprite {

@private
	CGFloat fuel;
	CGPoint acceleration;
	CGPoint decelaration;
	CGPoint velocity;
	CGPoint collisionForce;
	CGPoint collisionFrictionalForce;
	playerState currentState;
	BOOL playerBoost;
	ccTime playerBoostTime;
	int numNonStaticCollectables;
	int numFlyingCollectables;
	playerWeapon selectedWeapon;
	BOOL killed;
    int barLength;
}
@property CGFloat fuel;
@property CGPoint acceleration;
@property CGPoint decelaration;
@property CGPoint velocity;
@property CGPoint collisionForce;
@property CGPoint collisionFrictionalForce;
@property playerState currentState;
@property BOOL playerBoost;
@property ccTime playerBoostTime;
@property int numNonStaticCollectables;
@property int numFlyingCollectables;
@property playerWeapon selectedWeapon;
@property BOOL killed;
@property int barLength;


-(void) updateDuringShortGamePlay;
-(void) updateDuringMainGamePlay;
-(void) limitPlayerBoundaries;
-(void) updatePosition;
-(void) playerBoostMovement: (ccTime) deltaTime;
-(void) powerUpForces;
-(void) maintainPlayerAtMinHeight: (ccTime) deltaTime;
-(void) playerFrictionalForces;
-(void) playerFrictionalForcesEnemyCollisionMovement;
-(void) updateAccelerometerInput: (CGFloat) acceX;
-(BOOL) touchedPlayer: (CGPoint) touchedLocation;
-(void) changePlayerSelectedWeapon;
-(void) changePlayerWeaponDefault;
-(void) updateDirectionShortGamePlay;

@end
