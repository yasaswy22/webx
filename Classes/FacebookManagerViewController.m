//
//  FacebookManagerViewController.m
//  WebX
//
//  Created by Sunny's Mac on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FacebookManagerViewController.h"
#import "FacebookViewController.h"
#import "FacebookFriendsViewController.h"
#import "FacebookGameCenterInviteViewController.h"
#import "FacebookGameCenterMappingViewController.h"

@implementation FacebookManagerViewController

@synthesize facebookTabBarManager;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        facebookAppId = [NSString stringWithString:@"168636726530184"];
        facebook = [[Facebook alloc] initWithAppId:facebookAppId];
        facebook.accessToken    = [[NSUserDefaults standardUserDefaults] stringForKey:@"AccessToken"];
        facebook.expirationDate = (NSDate *) [[NSUserDefaults standardUserDefaults] objectForKey:@"ExpirationDate"];
        
        if (![facebook isSessionValid]) {
            [self authorizeFacebook];           
        }
        else if([facebook isSessionValid]){
            [self showFacebookTabBar];
        }
    }
    return self;
}

-(void) showFacebookTabBar{
    
    FacebookViewController *facebookViewController = [[FacebookViewController alloc] 
                                                      initWithNibName:@"FacebookViewController" 
                                                      bundle:nil];
    facebookViewController.title = @"Post";
    FacebookFriendsViewController *facebookFriendsViewController = [[FacebookFriendsViewController alloc] 
                                                                    initWithNibName:@"FacebookFriendsViewController" 
                                                                    bundle:nil];
    facebookFriendsViewController.title = @"Facebook";
    FacebookGameCenterInviteViewController *facebookGameCenterInviteViewController = [[FacebookGameCenterInviteViewController alloc] 
                                              initWithNibName:@"FacebookGameCenterInviteViewController" 
                                              bundle:nil];
    facebookGameCenterInviteViewController.title = @"Game Center";
    FacebookGameCenterMappingViewController *facebookGameCenterMappingViewController = [[FacebookGameCenterMappingViewController alloc] 
                                                                                      initWithNibName:@"FacebookGameCenterMappingViewController" 
                                                                                      bundle:nil];
    facebookGameCenterMappingViewController.title = @"Map";
    
    self.facebookTabBarManager = [[UITabBarController alloc] init];
    self.facebookTabBarManager.viewControllers = [NSArray arrayWithObjects: facebookViewController, facebookFriendsViewController, facebookGameCenterInviteViewController, facebookGameCenterMappingViewController, nil];

//    self.facebookTabBarManager.viewControllers = [NSArray arrayWithObjects: facebookGameCenterInviteViewController, nil];
    

//    CGRect frame = CGRectMake(0.0, 0.0, self.facebookTabBarManager.view.bounds.size.width, 
//                              self.facebookTabBarManager.view.bounds.size.height);
//    UIView *tabBarTint = [[UIView alloc] initWithFrame:frame];
//    [tabBarTint setBackgroundColor:[UIColor colorWithRed:59.0/255.0 green:89/255.0 blue:152/255.0 alpha:0.7]];
//    [self.facebookTabBarManager.tabBar insertSubview:tabBarTint atIndex:0];
//    [tabBarTint release];

    self.view = self.facebookTabBarManager.view;
 
    [facebookViewController release];
    [facebookFriendsViewController release];
    [facebookGameCenterInviteViewController release];
    [facebookGameCenterMappingViewController release];
}

- (void)dealloc
{
    [self.facebookTabBarManager release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self.facebookTabBarManager release];
    self.facebookTabBarManager = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark Facebook delegate methods

-(void) fbDidLogin{
    [self synchronizeFacebook];
    [self showFacebookTabBar];
}   

-(void) synchronizeFacebook{
    [[NSUserDefaults standardUserDefaults] setObject:facebook.accessToken forKey:@"AccessToken"];
    [[NSUserDefaults standardUserDefaults] setObject:facebook.expirationDate forKey:@"ExpirationDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) authorizeFacebook{
    NSArray *facebookPermissions = [NSArray arrayWithObjects:@"offline_access", 
                                    @"user_about_me", @"read_stream",@"publish_stream", nil];
    [facebook authorize:facebookPermissions delegate:self];
}

- (IBAction)loginIntoFacebook:(id)sender {
    [self authorizeFacebook];
}

- (IBAction)cancelLoginIntoFacebook:(id)sender {
        [self dismissModalViewController];
}

#pragma mark Views (Facebook View)

// Helper methods

-(UIViewController*) getRootViewController
{
	return [UIApplication sharedApplication].keyWindow.rootViewController;
}

-(void) presentViewController:(UIViewController*)vc
{
	UIViewController* rootVC = [self getRootViewController];
	[rootVC presentModalViewController:vc animated:YES];
}

-(void) dismissModalViewController
{
	UIViewController* rootVC = [self getRootViewController];
	[rootVC dismissModalViewControllerAnimated:YES];
}

@end
