//
//  CollectableItem.m
//  WebX
//
//  Created by Sunny's Mac on 1/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CollectableItem.h"
#import "GlobalVars.h"

@implementation CollectableItem

@synthesize velocity;
@synthesize acceleration;
@synthesize collisionForce;
@synthesize frictionalForce;
@synthesize nonStatic;
@synthesize flying;

-(void) update {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	self.position = ccp(self.position.x+velocity.x, self.position.y+velocity.y);
}

-(void) updateCollectablesPosition: (CGPoint) targetVelocity {
	
	if (nonStatic) {
		velocity = ccp(-targetVelocity.x, -targetVelocity.y);
		
		if (self.position.y < -self.contentSize.height*0.5 || 
			self.position.x < -self.contentSize.width*0.5 ||
			self.position.x > screenSize.width+self.contentSize.width*0.5) {
			
			[self resetCollectablesPosition];
		}
		[self update];
	}
	else if(flying) {
		self.position = ccp(self.position.x, self.position.y-targetVelocity.y);
		
		if (self.velocity.x > 0) {
			if(self.position.x > screenSize.width+self.contentSize.width*0.5 ||
			   self.position.y < -self.contentSize.height*0.5) {
				
				int side = arc4random()%2;
				int j = arc4random() % (int)(screenSize.width*0.5);
				int k = arc4random() % (int)(screenSize.height*1.5);
				
				if (side == 0) {
					self.position = ccp(-j, k);
					self.velocity = ccp(FLYING_ENEMY_HOR_ACCELERATION, 0);
				}
				else if (side == 1) {
					self.position = ccp(screenSize.width + j, k);
					self.velocity = ccp(-FLYING_ENEMY_HOR_ACCELERATION, 0);
				}
			}
		}
		else if (self.velocity.x < 0) {
			if(self.position.x < -self.contentSize.width*0.5 ||
			   self.position.y < -self.contentSize.height*0.5) {
				
				int side = arc4random()%2;
				int j = arc4random() % (int)(screenSize.width*0.5);
				int k = arc4random() % (int)(screenSize.height*1.5);
				
				if (side == 0) {
					self.position = ccp(-j, k);
					self.velocity = ccp(FLYING_ENEMY_HOR_ACCELERATION, 0);
				}
				else if (side == 1) {
					self.position = ccp(screenSize.width + j, k);
					self.velocity = ccp(-FLYING_ENEMY_HOR_ACCELERATION, 0);
				}
			}
		}
		else if(self.velocity.x == 0 && self.position.y < -self.contentSize.height*0.5) {
			int side = arc4random()%2;
			int j = arc4random() % (int)(screenSize.width*0.5);
			int k = arc4random() % (int)(screenSize.height*1.5);
			
			if (side == 0) {
				self.position = ccp(-j, k);
				self.velocity = ccp(FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
			else if (side == 1) {
				self.position = ccp(screenSize.width + j, k);
				self.velocity = ccp(-FLYING_ENEMY_HOR_ACCELERATION, 0);
			}
		}
		[self update];
	}
}

-(void) resetCollectablesPosition {
	if (nonStatic) {
		int j = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
		int k = (int)screenSize.height + arc4random() % (int)(screenSize.height*0.5);
		
		self.position = ccp(j, k);
		collisionForce = CGPointZero;
		frictionalForce = CGPointZero;
	}
	else if(flying) {
		int side = arc4random()%2;
		int j = arc4random() % (int)(screenSize.width*0.5);
		int k = (int) screenSize.height * 0.5 + arc4random() % (int)(screenSize.height*0.5);
		
		if (side == 0) {
			self.position = ccp(-j, k);
			self.velocity = ccp(FLYING_ENEMY_HOR_ACCELERATION, 0);
		}
		else if (side == 1) {
			self.position = ccp(screenSize.width + j, k);
			self.velocity = ccp(-FLYING_ENEMY_HOR_ACCELERATION, 0);
		}
	}
}

@end
