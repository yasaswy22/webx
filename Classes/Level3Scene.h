//
//  Level3Scene.h
//  WebX
//
//  Created by Sunny's Mac on 1/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "FuelItem.h"
#import "ScrollingLayer.h"

@interface Level3Scene : CCLayer {
	
@private
	PlayerItem *player;
	FuelItem *fuelTank;
	CCMenuItemFont *pauseButton;
	NSMutableArray *flyingEnemies;
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat lor;
	ccTime deltaTime;
}

@end
