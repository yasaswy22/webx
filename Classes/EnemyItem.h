//
//  EnemyItem.h
//  WebProject
//
//  Created by Sunny's Mac on 12/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface EnemyItem : CCSprite {

@private
	CGPoint velocity;
	CGPoint acceleration;
	CGPoint collisionForce;
	CGPoint frictionalForce;
}

@property CGPoint velocity;
@property CGPoint acceleration;
@property CGPoint collisionForce;
@property CGPoint frictionalForce;

-(void) update;
-(void) updateEnemiesPosition: (CGPoint) targetVelocity;
-(void) staticEnemyFrictionalForce;
-(void) resetEnemyPosition;
-(void) resetEnemyPositionForBoost;

@end
