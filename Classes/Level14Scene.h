//
//  Level14Scene.h
//  WebX
//
//  Created by Sunny's Mac on 2/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "FuelItem.h"

@interface Level14Scene : CCLayer {
	
@private
	PlayerItem *player;
	FuelItem *fuelTank;
	CCMenuItemFont *pauseButton;
	NSMutableArray *chasingEnemies;
	BOOL playerTouched;
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat acceX;
	ccTime deltaTime;
	ccTime accumulatedTime;
	ccTime totalTime;
	CGPoint touchedLocation;
	CGPoint previousPlayerPosition;
}

-(void) moveWeaponsTowardsPlayer;
-(void) getPlayerInfluenceRect;
-(void) returnMainMenu: (id) sender;
@end
