//
//  AshItem.h
//  WebX
//
//  Created by Sunny's Mac on 8/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface AshItem : CCSprite {
    
@private
    CGPoint velocity;
}

@property CGPoint velocity;

-(void) update;
-(void) updateAll;
-(void) updateScaling;
-(void) updateRotating;
-(void) setPositioning;
-(void) setPositioningInitially;
-(void) randomizeVelocity;

@end
