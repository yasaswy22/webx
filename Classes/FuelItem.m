//
//  FuelItem.m
//  WebX
//
//  Created by Sunny's Mac on 1/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FuelItem.h"
#import "GlobalVars.h"

@implementation FuelItem

@synthesize velocity;
@synthesize active;

-(void) update {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	self.position = ccp(self.position.x+velocity.x, self.position.y+velocity.y);
}

-(void) updateFuelTankPosition: (CGPoint) targetVelocity {
	
	if (self.active) {
		
		velocity = ccp(-targetVelocity.x, -targetVelocity.y);
		
		if (self.position.y < screenSize.height*0.8 && 
			self.position.y > MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT - screenSize.height*0.05) {
			self.visible = YES;
		}
		else {
			self.visible = NO;
		}

		
		if (self.position.y < -self.contentSize.height*0.5 || 
			self.position.x < -self.contentSize.width*0.5 ||
			self.position.x > screenSize.width+self.contentSize.width*0.5) {
			
			//		[self resetFuelTankPosition];
			[self makeFuelItemInactive];
		}
	}
	
	[self update];
}

-(void) resetFuelTankPosition {
	self.active = NO;
	int i = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
	int j = screenSize.height + arc4random() % (int)(screenSize.height*0.5);
	self.position = ccp(i, j);
}

-(void) makeFuelItemInactive {
	self.position = ccp(0, -screenSize.height*0.5);
	self.active = NO;
}

@end
