//
//  LevelSelectScene.h
//  WebX
//
//  Created by Sunny's Mac on 1/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface LevelSelectScene : CCLayer {

@private
	NSMutableArray *levels;
}

+(id) scene;

@end
