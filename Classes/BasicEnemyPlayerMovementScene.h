//
//  BasicEnemyPlayerMovementScene.h
//  WebX
//
//  Created by Sunny's Mac on 1/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"

@interface BasicEnemyPlayerMovementScene : CCLayer {

@private
	PlayerItem *player;
	NSMutableArray *staticEnemies;
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat lor;
	ccTime deltaTime;
}

@end
