//
//  AshItem.m
//  WebX
//
//  Created by Sunny's Mac on 8/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AshItem.h"
#import "GlobalVars.h"
#define VEL_ASH_X  1
#define VEL_ASH_Y  2

@implementation AshItem

@synthesize velocity;

-(void) update{
    if (self.scale > 0.1) {        
        self.position = ccp(self.position.x+self.velocity.x, self.position.y+self.velocity.y);
    }
    else
        [self setPositioning];
}

-(void) updateAll{
    if (self.scale > 0.1 || self.position.y < -self.contentSize.height *0.5) {        
        [self updateScaling];
        [self randomizeVelocity];
        [self updateRotating];
        self.position = ccp(self.position.x+self.velocity.x, self.position.y+self.velocity.y);
    }
    else
        [self setPositioning];
}

-(void) updateRotating{
    [self runAction:[CCRotateBy actionWithDuration:1.0f angle:60]];
}


-(void) updateScaling{
    int randomScaleDecreaseFactor = 1 + arc4random() % 5;
    [self setScale:(self.scale - 0.01*randomScaleDecreaseFactor)];
}

-(void) setPositioning{
    // For initial position
    int j = arc4random() % (int) screenSize.width;
    int k = (int) screenSize.height + arc4random() % (int) screenSize.height*0.5;
    self.position = ccp(j, k);
    
    // For initial random velocity
    [self randomizeVelocity];
    
    // For initial Size
    int randomScaleFactor = 50 + arc4random() %  40;
    [self setScale:randomScaleFactor * 0.01 ];
}

-(void) setPositioningInitially{
    // For initial position
    int j = arc4random() % (int) screenSize.width;
    int k = (int) screenSize.height + arc4random() % (int) screenSize.height;
    self.position = ccp(j, k);
    
    // For initial random velocity
    [self randomizeVelocity];
    
    // For initial Size
    int randomScaleFactor = 50 + arc4random() %  40;
    [self setScale:randomScaleFactor * 0.01 ];
}

-(void) randomizeVelocity{
    int randomVelocityFactorX = 10 + arc4random() %  90;
    int randomVelocityFactorY = 10 + arc4random() %  90;
    int randomSignedVelocity = arc4random() % 2;
    if (randomSignedVelocity == 0) {
        self.velocity = ccp(VEL_ASH_X*randomVelocityFactorX*0.01, -VEL_ASH_Y*randomVelocityFactorY*0.01);
    }
    if (randomSignedVelocity == 1) {
        self.velocity = ccp(-VEL_ASH_X*randomVelocityFactorX*0.01, -VEL_ASH_Y*randomVelocityFactorY*0.01);
    }
}

@end
