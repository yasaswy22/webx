//
//  Level17Scene.h
//  WebX
//
//  Created by Sunny's Mac on 2/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "GamePlayMethods.h"

@interface Level17Scene : CCLayer {

@private
	GamePlayMethods *gamePlayMethods;
	PlayerItem *player;
	CCMenuItemFont *pauseButton;
	NSMutableArray *fuelItems1;
	NSMutableArray *staticEnemies;
	NSMutableArray *chasingEnemies;
	NSMutableArray *nonStaticCollectables;
	NSMutableArray *nonStaticWeapons;
	NSMutableArray *nonStaticCollectedWeapons;
	NSMutableArray *flyingCollectables;
	NSMutableArray *flyingWeapons;
	NSMutableArray *flyingCollectedWeapons;
	NSMutableArray *blocks;
	NSMutableArray *chasingEnemiesPlacementGrid;
	BOOL playerTouched;
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat acceX;
	ccTime deltaTime;
	ccTime accumulatedTime;
	ccTime totalTime;
	CGPoint touchedLocation;
	CGPoint previousPlayerPosition;
	CCLabelTTF *scoreLabel;
	NSString *scoreLabelText;
}

+(id) scene;
-(void) displayScore: (CGFloat) score;
-(void) returnMainMenu: (id) sender;
-(void) startShortGamePlay;

@end
