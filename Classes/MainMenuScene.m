//
//  MainMenuScene.m
//  WebX
//
//  Created by Sunny's Mac on 12/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MainMenuScene.h"
#import "GamePlayMainScene.h"
#import "GlobalVars.h"
#import "FacebookManagerViewController.h"
#import "WebXAppDelegate.h"
#import "GameCenterTable.h"
#import "PersonalDetailsTable.h"

#define SPRITES_IN_FIRE_PLACE 24
#define SPRITES_IN_CERBERUS 4
@implementation MainMenuScene

@synthesize fetchedGameCenterEntries;
@synthesize personalDetails;
@synthesize managedObjectContext;

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* mainMenuScene = [CCScene node];
	MainMenuScene* mainMenuLayer = [MainMenuScene node];
	[mainMenuScene addChild:mainMenuLayer];
	return mainMenuScene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		
        self.managedObjectContext = [(WebXAppDelegate*)[UIApplication sharedApplication].delegate managedObjectContext];
        [self loadPersonalGameCenterDetailsFromCoreData];
        sharedGlobalSingletonVarsInstance = [GlobalSingletonVars sharedGlobalSingletonVars];
        screenSize = [[CCDirector sharedDirector] winSize];
        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES && [[UIScreen mainScreen] scale] == 2.00) {
            retinaDisplay = YES;
        }
        
        GameKitHelper* gkHelper = [GameKitHelper sharedGameKitHelper];
		gkHelper.delegate = self;
		[gkHelper authenticateLocalPlayer];
        if (sharedGlobalSingletonVarsInstance.justPlayed) {
            NSLog(@"Just played");
            CGFloat scoreFromCurrentGame = sharedGlobalSingletonVarsInstance.score;
            CGFloat scoreFromCodeData = [self.personalDetails.gameCenterScore floatValue];
            
            if (scoreFromCodeData < scoreFromCurrentGame) {
                
                NSNumber *currentScore = [NSNumber numberWithFloat:scoreFromCurrentGame];
                
                [PersonalDetailsTable updateGameCenterScore:currentScore 
                                            forGameCenterID:self.personalDetails.gameCenterID 
                                         forGameCenterAlias:self.personalDetails.gameCenterAlias  
                                     inManagedObjectContext:self.managedObjectContext];
                [self loadPersonalGameCenterDetailsFromCoreData];
                GKLocalPlayer* localPlayer = [GKLocalPlayer localPlayer];
                
                if (localPlayer.authenticated) {
                    int currLongScore = [self.personalDetails.gameCenterScore intValue];
                    [gkHelper submitScore:currLongScore category:@"Score"];
                    NSLog(@"Updated Game center score:%d", currLongScore);
                }
            }
        }
        [sharedGlobalSingletonVarsInstance resetVars];
        
        screenSize = [[CCDirector sharedDirector] winSize];
		CCSprite *gate = [CCSprite spriteWithFile:@"gate.png"];
		gate.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
		[self addChild:gate z:0];
      
        // Cerberus
        
        if (retinaDisplay) {
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"cerberus-hd.plist"];
            CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:@"cerberus-hd.png"];
            [self addChild:spriteBatch z:9];
            NSMutableArray *rotateAnimFrames = [NSMutableArray array];
            for(int i = 1; i <= SPRITES_IN_CERBERUS; ++i) {
                if (i<10) {
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image0000%d.png", i]]];
                }
                else{
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image000%d.png", i]]];
                }
                burningAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.25f];
            }
            
            burningFirePlace = [CCSprite spriteWithSpriteFrameName:@"Image00001.png"];
            burningFirePlace.position = ccp(gate.position.x, 
                                            gate.position.y -
                                            gate.contentSize.height*0.5);
            CCAction *burningAction = [CCRepeatForever actionWithAction:
                                       [CCAnimate actionWithAnimation:burningAnim restoreOriginalFrame:NO]];
            [burningFirePlace runAction:burningAction];
            [spriteBatch addChild:burningFirePlace];
        }
        else{
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"cerberus.plist"];
            CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:@"cerberus.png"];
            [self addChild:spriteBatch z:9];
            NSMutableArray *rotateAnimFrames = [NSMutableArray array];
            for(int i = 1; i <= SPRITES_IN_CERBERUS; ++i) {
                if (i<10) {
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image0000%d.png", i]]];
                }
                else{
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image000%d.png", i]]];
                }
                burningAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.25f];
            }
            
            burningFirePlace = [CCSprite spriteWithSpriteFrameName:@"Image00001.png"];
            burningFirePlace.position = ccp(gate.position.x, 
                                            gate.position.y -
                                            gate.contentSize.height*0.5);
            CCAction *burningAction = [CCRepeatForever actionWithAction:
                                       [CCAnimate actionWithAnimation:burningAnim restoreOriginalFrame:NO]];
            [burningFirePlace runAction:burningAction];
            [spriteBatch addChild:burningFirePlace];
        }
        
        // Burning Fire place 1
        
        if (retinaDisplay) {
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"fire_place-hd.plist"];
            CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:@"fire_place-hd.png"];
            [self addChild:spriteBatch z:9];
            NSMutableArray *rotateAnimFrames = [NSMutableArray array];
            for(int i = 1; i <= SPRITES_IN_FIRE_PLACE; ++i) {
                if (i<10) {
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image0000%d.png", i]]];
                }
                else{
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image000%d.png", i]]];
                }
                burningAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.05f];
            }
            
            burningFirePlace = [CCSprite spriteWithSpriteFrameName:@"Image00001.png"];
            burningFirePlace.position = ccp(gate.position.x - 
                                            gate.contentSize.width*0.5 + 
                                            burningFirePlace.contentSize.width*0.5, 
                                            gate.position.y - 
                                            gate.contentSize.height*0.5 + 
                                            burningFirePlace.contentSize.height*0.5);
            CCAction *burningAction = [CCRepeatForever actionWithAction:
                                      [CCAnimate actionWithAnimation:burningAnim restoreOriginalFrame:NO]];
            [burningFirePlace runAction:burningAction];
            [spriteBatch addChild:burningFirePlace];
        }
        else{
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"fire_place.plist"];
            CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:@"fire_place.png"];
            [self addChild:spriteBatch z:9];
            NSMutableArray *rotateAnimFrames = [NSMutableArray array];
            for(int i = 1; i <= SPRITES_IN_FIRE_PLACE; ++i) {
                if (i<10) {
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image0000%d.png", i]]];
                }
                else{
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image000%d.png", i]]];
                }
                burningAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.05f];
            }
            
            burningFirePlace = [CCSprite spriteWithSpriteFrameName:@"Image00001.png"];
            burningFirePlace.position = ccp(gate.position.x - 
                                            gate.contentSize.width*0.5 + 
                                            burningFirePlace.contentSize.width*0.5, 
                                            gate.position.y - 
                                            gate.contentSize.height*0.5 + 
                                            burningFirePlace.contentSize.height*0.5);
            CCAction *burningAction = [CCRepeatForever actionWithAction:
                                       [CCAnimate actionWithAnimation:burningAnim restoreOriginalFrame:NO]];
            [burningFirePlace runAction:burningAction];
            [spriteBatch addChild:burningFirePlace];
        }
        
        // Burning fire place 2
        
        if (retinaDisplay) {
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"fire_place-hd.plist"];
            CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:@"fire_place-hd.png"];
            [self addChild:spriteBatch z:9];
            NSMutableArray *rotateAnimFrames = [NSMutableArray array];
            for(int i = 1; i <= SPRITES_IN_FIRE_PLACE; ++i) {
                if (i<10) {
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image0000%d.png", i]]];
                }
                else{
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image000%d.png", i]]];
                }
                burningAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.05f];
            }
            
            burningFirePlace = [CCSprite spriteWithSpriteFrameName:@"Image00001.png"];
            burningFirePlace.position = ccp(gate.position.x + 
                                            gate.contentSize.width*0.5 - 
                                            burningFirePlace.contentSize.width*0.5, 
                                            gate.position.y - 
                                            gate.contentSize.height*0.5 + 
                                            burningFirePlace.contentSize.height*0.5);
            CCAction *burningAction = [CCRepeatForever actionWithAction:
                                       [CCAnimate actionWithAnimation:burningAnim restoreOriginalFrame:NO]];
            [burningFirePlace runAction:burningAction];
            [spriteBatch addChild:burningFirePlace];
        }
        else{
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"fire_place.plist"];
            CCSpriteBatchNode *spriteBatch = [CCSpriteBatchNode batchNodeWithFile:@"fire_place.png"];
            [self addChild:spriteBatch z:9];
            NSMutableArray *rotateAnimFrames = [NSMutableArray array];
            for(int i = 1; i <= SPRITES_IN_FIRE_PLACE; ++i) {
                if (i<10) {
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image0000%d.png", i]]];
                }
                else{
                    [rotateAnimFrames addObject:
                     [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                      [NSString stringWithFormat:@"Image000%d.png", i]]];
                }
                burningAnim = [CCAnimation animationWithFrames:rotateAnimFrames delay:0.05f];
            }
            
            burningFirePlace = [CCSprite spriteWithSpriteFrameName:@"Image00001.png"];
            //            burningFirePlace.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
            burningFirePlace.position = ccp(gate.position.x + 
                                            gate.contentSize.width*0.5 - 
                                            burningFirePlace.contentSize.width*0.5, 
                                            gate.position.y - 
                                            gate.contentSize.height*0.5 + 
                                            burningFirePlace.contentSize.height*0.5);
            CCAction *burningAction = [CCRepeatForever actionWithAction:
                                       [CCAnimate actionWithAnimation:burningAnim restoreOriginalFrame:NO]];
            [burningFirePlace runAction:burningAction];
            [spriteBatch addChild:burningFirePlace];
        }
        
//		CCLabelTTF *title = [CCLabelTTF labelWithString:@"Main Menu" fontName:@"Marker Felt" fontSize:32];
//		title.position =  ccp(screenSize.width * 0.5, screenSize.height*0.8 - title.contentSize.height * 0.5);
//		title.color = ccc3(TEXT_COLOR);
//		[self addChild: title z:10];
		
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:32];
		
        CCMenuItemFont *startButton = [CCMenuItemFont itemFromString:@"Start" target:self selector:@selector(startGamePlayMain:)];
		startButton.color = ccc3(TEXT_COLOR);
		startButton.position = ccp(screenSize.width*0.5, screenSize.height*0.5);

//        CCMenuItemFont *leaderboardButton = [CCMenuItemFont itemFromString:@"Leaderboard" 
//                                                                    target:self 
//                                                                  selector:@selector(showLeaderboard:)];
//		leaderboardButton.color = ccc3(TEXT_COLOR);
//		leaderboardButton.position = ccp(screenSize.width*0.5, screenSize.height*0.3);
//        
//        facebookButtonLogin = [CCMenuItemFont itemFromString:@"Shout in Facebook" 
//                                                                    target:self 
//                                                                  selector:@selector(postOnFaceBook:)];
//		facebookButtonLogin.color = ccc3(TEXT_COLOR);
//		facebookButtonLogin.position = ccp(screenSize.width*0.5, screenSize.height*0.1);
        
//		CCMenu* menu = [CCMenu menuWithItems:   startButton, 
//                                                leaderboardButton, 
//                                                facebookButtonLogin, 
//                                                nil];
        
        CCMenu* menu = [CCMenu menuWithItems:startButton, nil];
        
		menu.position = CGPointZero;
		[self addChild:menu z:10];
	}
	return self;
}

-(void) postOnFaceBook: (id) sender{
    if ([self isConnectedToInternet]) {
        
    FacebookManagerViewController *facebookManagerViewController = [[[FacebookManagerViewController alloc] 
                                                       initWithNibName:@"FacebookManagerViewController" 
                                                       bundle:nil] autorelease];
        
        
    UIViewController *presentViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [presentViewController presentModalViewController:facebookManagerViewController animated:YES];
    }
}

#pragma mark Code Data methods

-(void) loadPersonalGameCenterDetailsFromCoreData {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"PersonalDetailsTable" 
                                   inManagedObjectContext:self.managedObjectContext]];
    [request setPredicate:nil];
    NSError *error = nil;
    [self.personalDetails release];
    self.personalDetails  = [[[self.managedObjectContext executeFetchRequest:request error:&error] lastObject]retain] ;
    [request release];
}

-(void) loadFriendsGameCenterDetailsFromCoreData {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"GameCenterTable" 
                                   inManagedObjectContext:self.managedObjectContext]];
    [request setPredicate:nil];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"gameCenterAlias" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
    
    NSError *error = nil;
    [self.fetchedGameCenterEntries release];
    self.fetchedGameCenterEntries  = [[self.managedObjectContext executeFetchRequest:request error:&error] retain];
    [request release];
    
    //    CCLOG(@"onPlayerInfoReceived from Core Data: %@", [self.fetchedGameCenterEntries description]);
}

#pragma mark GameKitHelper delegate methods

-(void) onLocalPlayerAuthenticationChanged
{
	GKLocalPlayer* localPlayer = [GKLocalPlayer localPlayer];
//	CCLOG(@"LocalPlayer isAuthenticated changed to: %@", localPlayer.authenticated ? @"YES" : @"NO");
	
	if (localPlayer.authenticated)
	{
		GameKitHelper* gkHelper = [GameKitHelper sharedGameKitHelper];
        GKLocalPlayer* localPlayer = [GKLocalPlayer localPlayer];
        GKLeaderboard *leaderboard = [[GKLeaderboard alloc] init];
        GKScore *gKScore= leaderboard.localPlayerScore;
         
        //For successive entries and user ID changes data is updated in the core data
        if (self.personalDetails.gameCenterID) {
            NSLog(@"Updating score from Game Center to Core data");
            NSNumber *currentScore = [NSNumber numberWithFloat:gKScore.value];
            
            [PersonalDetailsTable updateGameCenterScore:currentScore
                                        forGameCenterID:localPlayer.playerID
                                     forGameCenterAlias:localPlayer.alias 
                                 inManagedObjectContext:self.managedObjectContext];
        }
        // For first time entry
        else if(!self.personalDetails.gameCenterID){
        [PersonalDetailsTable entryOrUpdateWithGameCenterAlias:localPlayer.alias entryWithGameCenterID:localPlayer.playerID inManagedObjectContext:self.managedObjectContext];
        }
		[gkHelper getLocalPlayerFriends];
		//[gkHelper resetAchievements];
	}	
}

-(void) onFriendListReceived:(NSArray*)friends
{
//	CCLOG(@"onFriendListReceived: %@", [friends description]);
	GameKitHelper* gkHelper = [GameKitHelper sharedGameKitHelper];
	[gkHelper getPlayerInfo:friends];
    [gkHelper retrieveScoresForFriends:friends];
}

-(void) onPlayerInfoReceived:(NSArray*)players
{
//	CCLOG(@"onPlayerInfoReceived: %@", [players description]);
    
    for (int i=0; i<[players count]; i++) {
        
        GKPlayer *gameCenterData = [players objectAtIndex:i];
        [GameCenterTable entryWithGameCenterAlias:gameCenterData.alias entryWithGameCenterID:gameCenterData.playerID inManagedObjectContext:self.managedObjectContext];
    }
    
    [self loadFriendsGameCenterDetailsFromCoreData];
}

-(void) onScoresSubmitted:(bool)success
{
	CCLOG(@"onScoresSubmitted: %@", success ? @"YES" : @"NO");
}

-(void) onScoresReceived:(NSArray*)scores
{
    for (int i=0; i<[scores count]; i++) {
    
        GKScore *gameCenterData = [scores objectAtIndex:i];
        NSNumber *currentScore = [NSNumber numberWithFloat:gameCenterData.value];
        [GameCenterTable updateGameCenterScore:currentScore withGameCenterID:gameCenterData.playerID inManagedObjectContext:self.managedObjectContext];
    }
    [self loadFriendsGameCenterDetailsFromCoreData];
}

-(void) onAchievementReported:(GKAchievement*)achievement
{
//	CCLOG(@"onAchievementReported: %@", achievement);
}

-(void) onAchievementsLoaded:(NSDictionary*)achievements
{
//	CCLOG(@"onLocalPlayerAchievementsLoaded: %@", [achievements description]);
}

-(void) onResetAchievements:(bool)success
{
//	CCLOG(@"onResetAchievements: %@", success ? @"YES" : @"NO");
}

-(void) onLeaderboardViewDismissed
{
//	CCLOG(@"onLeaderboardViewDismissed");
//	
//	GameKitHelper* gkHelper = [GameKitHelper sharedGameKitHelper];
//	[gkHelper retrieveTopTenAllTimeGlobalScores];
}

-(void) onAchievementsViewDismissed
{
//	CCLOG(@"onAchievementsViewDismissed");
}

-(void) onReceivedMatchmakingActivity:(NSInteger)activity
{
//	CCLOG(@"receivedMatchmakingActivity: %i", activity);
}

-(void) onMatchFound:(GKMatch*)match
{
//	CCLOG(@"onMatchFound: %@", match);
}

-(void) onPlayersAddedToMatch:(bool)success
{
//	CCLOG(@"onPlayersAddedToMatch: %@", success ? @"YES" : @"NO");
}

-(void) onMatchmakingViewDismissed
{
//	CCLOG(@"onMatchmakingViewDismissed");
}
-(void) onMatchmakingViewError
{
//	CCLOG(@"onMatchmakingViewError");
}

-(void) onPlayerConnected:(NSString*)playerID
{
//	CCLOG(@"onPlayerConnected: %@", playerID);
}

-(void) onPlayerDisconnected:(NSString*)playerID
{
//	CCLOG(@"onPlayerDisconnected: %@", playerID);
}

-(void) onStartMatch
{
//	CCLOG(@"onStartMatch");
}

-(void) onReceivedData:(NSData*)data fromPlayer:(NSString*)playerID
{
//	CCLOG(@"onReceivedData: %@ fromPlayer: %@", data, playerID);
}

-(void) showLeaderboard: (id) sender{
    if ([self isConnectedToInternet]) {  
    GameKitHelper* gkHelper = [GameKitHelper sharedGameKitHelper];
//    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
//    [gkHelper retrieveTopTenAllTimeGlobalScores];
//    //        if (!localPlayer.isAuthenticated) {
//    //            CCLOG(@"localPlayer.isAuthenticated");
//    //            [gkHelper authenticateLocalPlayer];
//    //            [gkHelper submitScore:1234567 category:@"Score"];
//    //        }
        [gkHelper retrieveTopTenAllTimeGlobalScores];
        [gkHelper showLeaderboard];
    }
}

# pragma mark normal methods

-(void) startGamePlayMain: (id) sender{
	[[CCDirector sharedDirector]replaceScene: [GamePlayMainScene scene]];
}

-(BOOL) isConnectedToInternet{
    NSError *error = nil;
    NSString *internetTestString = [[[NSString alloc] 
                                     initWithContentsOfURL:[NSURL URLWithString: @"http://www.google.com"] 
                                     encoding:NSASCIIStringEncoding 
                                     error:&error] autorelease];
    
    if ([internetTestString length] == 0) {
        UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"Login Failed" 
                                                             message:@"Unable to connect. \n Please try again later." delegate:self 
                                                   cancelButtonTitle:@"Ok" 
                                                   otherButtonTitles:nil, nil] autorelease];
        [alertView show];
        return NO;
    }
    else
    return YES;
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
    [self.fetchedGameCenterEntries release];
	[super dealloc];
}

@end
