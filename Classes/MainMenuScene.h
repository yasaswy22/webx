//
//  MainMenuScene.h
//  WebX
//
//  Created by Sunny's Mac on 12/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GlobalVars.h"
#import "GlobalSingletonVars.h"
#import "GameKitHelper.h"
#import <GameKit/GameKit.h>

@class PersonalDetailsTable;

@interface MainMenuScene : CCLayer <GameKitHelperProtocol>{
    GlobalSingletonVars *sharedGlobalSingletonVarsInstance;
    CCMenuItemFont *facebookButtonLogin;
    PersonalDetailsTable *personalDetails;
    NSArray *fetchedGameCenterEntries;
    
    NSManagedObjectContext *managedObjectContext;
    
    BOOL retinaDisplay;
    CCAnimation *burningAnim;
    CCSprite *burningFirePlace;
}

@property (nonatomic, retain) NSArray *fetchedGameCenterEntries;
@property (nonatomic, retain) PersonalDetailsTable *personalDetails;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;


+(id) scene;
-(BOOL) isConnectedToInternet;
-(void) loadPersonalGameCenterDetailsFromCoreData;
-(void) loadFriendsGameCenterDetailsFromCoreData;
@end
