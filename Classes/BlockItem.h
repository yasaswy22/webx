//
//  BlockItem.h
//  WebX
//
//  Created by Sunny's Mac on 1/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GlobalVars.h"

@interface BlockItem : CCSprite {

@private
	BOOL active;
	BOOL willKill;
	CGPoint velocity;
}

@property BOOL active;
@property BOOL willKill;
@property CGPoint velocity;

-(void) update;
-(void) updateBlockPosition;
-(void) checkCollectedWeaponsBlockCollision: (NSMutableArray *) collectedWeapons;
-(void) checkEnemyItemBlockCollision: (NSMutableArray *) enemies;
-(void) checkChaserEnemyItemBlockCollision: (NSMutableArray *) chasingEnemies;
-(void) resetBlockPosition;
-(void) activateBlock;

@end
