//
//  GameCenterTable.h
//  WebX
//
//  Created by Sunny's Mac on 7/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GameCenterFacebookTable;

@interface GameCenterTable : NSManagedObject {
@private
}
@property (nonatomic, retain) NSNumber * gameCenterScore;
@property (nonatomic, retain) NSString * gameCenterID;
@property (nonatomic, retain) NSString * gameCenterEmail;
@property (nonatomic, retain) NSString * gameCenterAlias;
@property (nonatomic, retain) GameCenterFacebookTable * facebookGCRelation;

+(GameCenterTable *) entryWithGameCenterAlias:(NSString *)gameCenterAlias entryWithGameCenterID:(NSString *)gameCenterID inManagedObjectContext:(NSManagedObjectContext *)context;
+(GameCenterTable *) updateGameCenterScore:(NSNumber *)gameCenterScore withGameCenterID:(NSString *)gameCenterID inManagedObjectContext:(NSManagedObjectContext *)context;
@end
