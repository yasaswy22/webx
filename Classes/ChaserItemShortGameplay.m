//
//  ChaserItemShortGameplay.m
//  WebX
//
//  Created by Sunny's Mac on 2/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ChaserItemShortGameplay.h"
#import "GlobalVars.h"

@implementation ChaserItemShortGameplay

@synthesize active;

-(void) resetChaserItemShortGameplay {
	self.active = NO;
	self.position = ccp(-screenSize.width, -screenSize.height);
}

@end
