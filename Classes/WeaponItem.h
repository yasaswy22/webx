//
//  WeaponItem.h
//  WebX
//
//  Created by Sunny's Mac on 1/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface WeaponItem : CCSprite {

@private
	BOOL active;
}

@property BOOL active;

-(void) weaponsPositionPlayerBoostMovement: (CGPoint) playerVelocity: (BOOL) playerBoost: (ccTime) dTime;
-(void) rotateWeapons: (CGPoint) playerPosition :(CGPoint) previousPlayerPosition;

@end
