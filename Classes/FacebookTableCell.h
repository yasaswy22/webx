//
//  FacebookTableCell.h
//  WebX
//
//  Created by Sunny's Mac on 7/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FacebookTableCell : UITableViewCell {
    
    IBOutlet UIImageView *profilePicture;
    IBOutlet UILabel *profileName;
    IBOutlet UILabel *score;
    IBOutlet UIView *backgroundCellView;
    
}

@property (nonatomic, retain) UIImageView *profilePicture;
@property (nonatomic, retain) UILabel *profileName;
@property (nonatomic, retain) UILabel *score;
@property (nonatomic, retain) UIView *backgroundCellView;

@end
