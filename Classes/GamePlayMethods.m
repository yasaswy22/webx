//
//  GamePlayMethods.m
//  WebX
//
//  Created by Sunny's Mac on 2/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GamePlayMethods.h"
#import "GlobalVars.h"
#import "ChaserItem.h"

@implementation GamePlayMethods

-(void) updateMaxFuelItemsThatCanBeTaken: (GlobalSingletonVars*) sharedGlobalVarsInstance {
	if (sharedGlobalVarsInstance.score <= 2000) {
		sharedGlobalVarsInstance.maxFuelItemsThatCanBeTaken = MAX_FUEL_ITEMS_INITIALLY;
	}
	else if(sharedGlobalVarsInstance.score > 2000 && sharedGlobalVarsInstance.score <= 4000) {
		sharedGlobalVarsInstance.maxFuelItemsThatCanBeTaken = MAX_FUEL_ITEMS_INITIALLY*1.1;
	}
	else if(sharedGlobalVarsInstance.score > 4000 && sharedGlobalVarsInstance.score <= 6000) {
		sharedGlobalVarsInstance.maxFuelItemsThatCanBeTaken = MAX_FUEL_ITEMS_INITIALLY*1.2;
	}
	else if(sharedGlobalVarsInstance.score > 6000 && sharedGlobalVarsInstance.score <= 8000) {
		sharedGlobalVarsInstance.maxFuelItemsThatCanBeTaken = MAX_FUEL_ITEMS_INITIALLY*1.3;
	}
}

-(void) checkPlayerTakesFuel: (PlayerItem*) player :(FuelItem*) fuelItem :(NSMutableArray*) enemies :(GlobalSingletonVars*) sharedGlobalVarsInstance{
	
	CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
								   player.position.y - player.contentSize.height*0.5, 
								   player.contentSize.width, player.contentSize.height);
	
	CGRect fuelRect = CGRectMake(fuelItem.position.x - fuelItem.contentSize.width*0.5, 
								 fuelItem.position.y - fuelItem.contentSize.height*0.5, 
								 fuelItem.contentSize.width, fuelItem.contentSize.height);
	
	if(CGRectIntersectsRect(playerRect, fuelRect)) {
		
		sharedGlobalVarsInstance.fuelItemsTaken++;
		[fuelItem makeFuelItemInactive];
		if (sharedGlobalVarsInstance.fuelItemsTaken >= sharedGlobalVarsInstance.maxFuelItemsThatCanBeTaken) {
			//CCLOG(@"Collected 100 foot prints");
			player.fuel = PLAYER_MAX_FUEL;
			player.decelaration = CGPointZero;
			player.playerBoost = YES;
			sharedGlobalVarsInstance.fuelItemsTaken = 0;
			[self updateMaxFuelItemsThatCanBeTaken: sharedGlobalVarsInstance];
			for (int i=0; i<enemies.count; i++) {
				EnemyItem *enemy = [enemies objectAtIndex:i];
				[enemy resetEnemyPositionForBoost];
			}
		}
	}
}

-(void) updateFuelItems :(PlayerItem*) player :(NSMutableArray *)fuelItems :(NSMutableArray*) enemies :(GlobalSingletonVars*) sharedGlobalVarsInstance{
	for (int i=0; i<fuelItems.count; i++) {
		FuelItem *fuelItem = [fuelItems objectAtIndex:i];
		[fuelItem updateFuelTankPosition: player.velocity];
		[self checkPlayerTakesFuel :player :fuelItem :enemies :sharedGlobalVarsInstance ];
	}
}

-(void) placeFuelItemsPattern1: (NSMutableArray *) fuelItems1 {

	CGFloat offset = screenSize.width*0.5;
	CGFloat peakDistance = 100;
	CGFloat flatness = 1 + arc4random() % 7;
	//CCLOG(@"flatness1: %f", flatness);
	int numFuelItemsToBeUsed = (int)(fuelItems1.count *0.1) + arc4random() % (int)(fuelItems1.count);
	
	for (int i=0; i<numFuelItemsToBeUsed; i++) {
		FuelItem *fuelItem = [fuelItems1 objectAtIndex:i];

		if (!fuelItem.active) {
			CGFloat xPos = peakDistance * sin((CGFloat)i / flatness)+offset;
			CGFloat yPos = screenSize.height+UNIT_GRID_HEIGHT*0.5+i*UNIT_GRID_HEIGHT*2.0;
			fuelItem.position = ccp(xPos, yPos);
			fuelItem.active = YES;
		}
	}
}

-(BOOL) checkInactivePattern1: (NSMutableArray *) fuelItems1 {
	for (int i=0; i<fuelItems1.count; i++) {
		FuelItem *fuelItem = [fuelItems1 objectAtIndex:i];
		if (fuelItem.active) {
			return NO;
		}
	}
	
	return YES;
}

-(BOOL) checkChasingEnemiesNearPlayer: (PlayerItem*) player :(NSMutableArray*) chasingEnemies {
    
    CGRect playerRectMeasuringNearness = CGRectMake(player.position.x - player.contentSize.width*0.5*PLAYER_RECT_NEARNESS_FACTOR, 
                                                    player.position.y - player.contentSize.height*0.5*PLAYER_RECT_NEARNESS_FACTOR, 
                                                    player.contentSize.width*PLAYER_RECT_NEARNESS_FACTOR, 
                                                    player.contentSize.height*PLAYER_RECT_NEARNESS_FACTOR);
    
    for (int i=0; i<[chasingEnemies count]; i++) {
        ChaserItem *chasingEnemy = [chasingEnemies objectAtIndex:i];
        
        CGRect chasingEnemyRect = CGRectMake(chasingEnemy.position.x - chasingEnemy.contentSize.width*0.5, 
                                             chasingEnemy.position.y - chasingEnemy.contentSize.height*0.5, 
                                             chasingEnemy.contentSize.width, 
                                             chasingEnemy.contentSize.height);
        
        if (CGRectIntersectsRect(chasingEnemyRect, playerRectMeasuringNearness)) {
            return YES;
        }
    }
    return NO;
}

-(void) checkAndActivateBlock: (NSMutableArray *) blocks {
    //	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	BOOL aBlockActive = NO;
	
	for (int i =0; i<blocks.count; i++) {
		BlockItem *block = [blocks objectAtIndex:i];
		if (block.active) {
			aBlockActive = YES;
			break;
		}
	}
	
	if (!aBlockActive) {
		int randomBlock = arc4random() %  blocks.count;
		BlockItem *block=[blocks objectAtIndex:randomBlock];
		
		while (!block.active) {	
            CGFloat currentVelocityY = 1 + arc4random() % (int)(MAX_ROCK_FALLING_VELOCITY);
            block.velocity = ccp(0, -currentVelocityY);
			[block activateBlock];
		}
	}
}

-(void) checkAndActivateBlocksInBackgroundLayer1: (NSMutableArray *)blocks {
//    CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	int numOfActiveBlocks = 0;
    int blocksToBeActivated = 0;
    int numOfAllowedActiveBlocks = NUMBER_OF_ROCKS_ALLOWED_ACTIVE;
    
	for (int i =0; i<blocks.count; i++) {
		BlockItem *block = [blocks objectAtIndex:i];
		if (block.active) {
            numOfActiveBlocks++;
		}
	}

    if (numOfActiveBlocks < numOfAllowedActiveBlocks) {
        
        blocksToBeActivated = numOfAllowedActiveBlocks - numOfActiveBlocks;
        
        while (blocksToBeActivated > 0) {
            int randomBlock = arc4random() %  blocks.count;
            BlockItem *block=[blocks objectAtIndex:randomBlock];
            
            while (!block.active) {	
                CGFloat currentVelocityY = 1 + arc4random() % (int)(MAX_ROCK_FALLING_VELOCITY);
                block.velocity = ccp(0, -currentVelocityY);
                [block activateBlock];
                
                blocksToBeActivated--;
            }
        }
    }
}

-(void) playerMovementPlayerBlockCollision: (PlayerItem*)player :(BlockItem*)block {
	
	CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
								   player.position.y - player.contentSize.height*0.5, 
								   player.contentSize.width, player.contentSize.height);
	
	CGRect blockRect = CGRectMake(block.position.x - block.contentSize.width*0.5, 
								  block.position.y - block.contentSize.height*0.5, 
								  block.contentSize.width, block.contentSize.height);
	
	CGPoint playerTopHead = ccp(player.position.x, player.position.y+player.contentSize.height*0.5);
	CGPoint playerBottomHead = ccp(player.position.x, player.position.y-player.contentSize.height*0.5);
	CGPoint playerLeftHead = ccp(player.position.x-player.contentSize.width*0.5, player.position.y);
	CGPoint playerRightHead = ccp(player.position.x+player.contentSize.width*0.5, player.position.y);
	
	CGPoint blockTopHead = ccp(block.position.x, block.position.y+block.contentSize.height*0.5);
	CGPoint blockBottomHead = ccp(block.position.x, block.position.y-block.contentSize.height*0.5);
	CGPoint blockLeftHead = ccp(block.position.x-block.contentSize.width*0.5, block.position.y);
	CGPoint blockRightHead = ccp(block.position.x+block.contentSize.width*0.5, block.position.y);
	
	
	if (CGRectIntersectsRect(blockRect, playerRect)) {
		
		if (block.willKill) {
			player.velocity = CGPointZero;
			player.acceleration = CGPointZero;
			player.decelaration = CGPointZero;
			player.fuel = 0;
			player.playerBoost = NO;
			player.killed = YES;
		}
		else {
			//Player collides block from bottom
			if (playerTopHead.y > blockBottomHead.y &&
                playerBottomHead.y < blockBottomHead.y &&
				playerLeftHead.x > blockLeftHead.x-player.contentSize.width*0.5 && 
				playerRightHead.x < blockRightHead.x+player.contentSize.width*0.5) {
				//CCLOG(@"Player collides block from bottom");
				player.position = ccp(player.position.x, blockRect.origin.y-player.contentSize.height*0.5);
			}
			//Player collides block from top
			else if (playerBottomHead.y < blockTopHead.y && 
                     playerTopHead.y > blockTopHead.y && 
                     playerLeftHead.x > blockLeftHead.x-player.contentSize.width*0.5 && 
                     playerRightHead.x < blockRightHead.x+player.contentSize.width*0.5) {
				//CCLOG(@"Player collides block from top");
				player.position = ccp(player.position.x, blockRect.origin.y+blockRect.size.height+player.contentSize.height*0.5);
			}
			//Player collides block from right
			else if(playerLeftHead.x < blockRightHead.x &&
                    playerRightHead.x > blockRightHead.x &&
					playerBottomHead.y > blockBottomHead.y-player.contentSize.height*0.5 &&
					playerTopHead.y < blockTopHead.y+player.contentSize.height*0.5) {
				//CCLOG(@"Player collides block from right");
				player.position = ccp(blockRect.origin.x+blockRect.size.width+player.contentSize.width*0.5, player.position.y);
			}
			//Player collides block from Left
			else if(playerRightHead.x > blockLeftHead.x &&
                    playerLeftHead.x < blockLeftHead.x &&
					playerBottomHead.y > blockBottomHead.y-player.contentSize.height*0.5 &&
					playerTopHead.y < blockTopHead.y+player.contentSize.height*0.5) {
				//CCLOG(@"Player collides block from Left");
				player.position = ccp(blockRect.origin.x-player.contentSize.width*0.5, player.position.y);
			}
		}
	}
}

-(void) playerMovementPlayerEnemiesCollision: (PlayerItem*)player :(EnemyItem *)enemy {
	
	CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
								   player.position.y - player.contentSize.height*0.5, 
								   player.contentSize.width, player.contentSize.height);
	
	CGRect enemyRect = CGRectMake(enemy.position.x - enemy.contentSize.width*0.5, 
								  enemy.position.y - enemy.contentSize.height*0.5, 
								  enemy.contentSize.width, enemy.contentSize.height);
	
	if(CGRectIntersectsRect(playerRect, enemyRect) && enemy.position.y > player.position.y) {
		
		CGPoint playerPositionToEnemy = ccp(player.position.x - enemy.position.x, player.position.y - enemy.position.y);
		CGFloat playerAngleToEnemy = ccpToAngle(playerPositionToEnemy);
		
		CGFloat playerVelocity = sqrt(player.velocity.x*player.velocity.x + player.velocity.y*player.velocity.y);
		CGFloat collisionForceMagnitude = PLAYER_COLLISION_FORCE_PLAYER_ENEMY_COLLISION * playerVelocity / PLAYER_MAX_VELOCITY;
		CGFloat frictionalForceMagnitude = PLAYER_FRICTIONAL_DECELERATION_PLAYER_ENEMY_COLLISION * playerVelocity / PLAYER_MAX_VELOCITY;
		
		player.collisionForce = ccp(collisionForceMagnitude*cos(playerAngleToEnemy), 
							 collisionForceMagnitude*sin(playerAngleToEnemy));
		player.collisionFrictionalForce = ccp(frictionalForceMagnitude*cos(playerAngleToEnemy),
									   frictionalForceMagnitude*sin(playerAngleToEnemy));
	}
}

-(void) moveCollectablesTowardsPlayer: (PlayerItem*)player :(CollectableItem *)collectable {
	CGRect	playerInfluence = CGRectMake(0, player.position.y - player.contentSize.height*PLAYER_INFLUENCE_SIZE*0.5,
										 screenSize.width, player.contentSize.height*PLAYER_INFLUENCE_SIZE);
    
    if (CGRectContainsPoint(playerInfluence, collectable.position)){
        
        CGPoint collectableItemPositionToPlayer = ccp(collectable.position.x - player.position.x, 
                                                      collectable.position.y - player.position.y);
        CGFloat collectableItemAngleToPlayer = ccpToAngle(collectableItemPositionToPlayer);
        
        CGPoint collectableItemPlayerInfluenceVelocity = ccp(COLLECTABLE_ITEM_VELOCITY_TOWARDS_PLAYER*cos(collectableItemAngleToPlayer), 
                                                             COLLECTABLE_ITEM_VELOCITY_TOWARDS_PLAYER*sin(collectableItemAngleToPlayer));
        
        collectable.position = ccp(collectable.position.x - collectableItemPlayerInfluenceVelocity.x, 
                                   collectable.position.y - collectableItemPlayerInfluenceVelocity.y);
    }
}

-(void) checkPlayerTakesCollectable: (PlayerItem*)player :(CollectableItem *) collectable {
	
	CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
								   player.position.y - player.contentSize.height*0.5, 
								   player.contentSize.width, player.contentSize.height);
	
	CGRect collectableRect = CGRectMake(collectable.position.x - collectable.contentSize.width*0.5,
										collectable.position.y - collectable.contentSize.height*0.5,
										collectable.contentSize.width, collectable.contentSize.height);
	
	if (CGRectIntersectsRect(collectableRect, playerRect)) {
		
		if (collectable.nonStatic) {
			if (player.numNonStaticCollectables < MAX_NON_STATIC_WEAPONS ) {
				player.numNonStaticCollectables ++;
				//				CCLOG(@"Collected non static weapon, total: % i", numNonStaticCollectables);
			}
			player.selectedWeapon = nonStaticWeapon;
		}
		else if(collectable.flying) {
			if (player.numFlyingCollectables < MAX_FLYING_WEAPONS ) {
				player.numFlyingCollectables ++;
				//				CCLOG(@"Collected flying weapon, total: % i", numFlyingCollectables);
			}
			player.selectedWeapon = flyingWeapon;
		}
		
		[collectable resetCollectablesPosition];
	}
}

-(void) checkPlayerTakesCollectable: (PlayerItem*)player :(CollectableItem *) collectable: (NSMutableArray *) nonStaticWeapons: (NSMutableArray *) flyingWeapons {
	
	CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
								   player.position.y - player.contentSize.height*0.5, 
								   player.contentSize.width, player.contentSize.height);
	
	CGRect collectableRect = CGRectMake(collectable.position.x - collectable.contentSize.width*0.5,
										collectable.position.y - collectable.contentSize.height*0.5,
										collectable.contentSize.width, collectable.contentSize.height);
	
	if (CGRectIntersectsRect(collectableRect, playerRect)) {
		
		if (collectable.nonStatic) {
			if (player.numNonStaticCollectables < MAX_NON_STATIC_WEAPONS ) {
				player.numNonStaticCollectables ++;
				WeaponItem *nonStaticWeapon = [nonStaticWeapons objectAtIndex:player.numNonStaticCollectables-1];
				nonStaticWeapon.active = YES;
				[self resetNonStaticWeaponsForRotationAroundPlayer: player :nonStaticWeapons];
			}
			player.selectedWeapon = nonStaticWeapon;
		}
		else if(collectable.flying) {
			if (player.numFlyingCollectables < MAX_FLYING_WEAPONS ) {
				player.numFlyingCollectables ++;
				WeaponItem *flyingWeapon = [flyingWeapons objectAtIndex:player.numFlyingCollectables-1];
				flyingWeapon.active = YES;
				[self resetFlyingWeaponsForRotationAroundPlayer: player :flyingWeapons];
			}
			player.selectedWeapon = flyingWeapon;
		}
		
		[collectable resetCollectablesPosition];
	}
}

-(void) resetNonStaticWeaponsForRotationAroundPlayer: (PlayerItem*)player :(NSMutableArray *) nonStaticWeapons {
	
	CGPoint firstNonStaticWeaponPosition = ccp(player.position.x+WEAPON_MAGNITUDE_FROM_PLAYER, player.position.y);
	CGPoint newRotatedPoint;
	CGFloat weaponAngleToPlayerRadians;	
	
	for (int i=0; i<nonStaticWeapons.count; i++) {
        WeaponItem *nonStaticWeapon = [nonStaticWeapons objectAtIndex:i];
        if (nonStaticWeapon.active) {
            CGFloat degreesOfSeperation = 360.0/(CGFloat)player.numNonStaticCollectables;
            newRotatedPoint = ccpRotateByAngle(firstNonStaticWeaponPosition, player.position, CC_DEGREES_TO_RADIANS(degreesOfSeperation*i));
            weaponAngleToPlayerRadians = ccpToAngle(ccpSub(newRotatedPoint, player.position));
            nonStaticWeapon.position = ccp(player.position.x+WEAPON_MAGNITUDE_FROM_PLAYER*cos(weaponAngleToPlayerRadians), 
                                           player.position.y+WEAPON_MAGNITUDE_FROM_PLAYER*sin(weaponAngleToPlayerRadians));
        }
        else if(!nonStaticWeapon.active) {
            nonStaticWeapon.position = WEAPON_ITEM_RESET_POSITION;
        }
    }
}

-(void) deactivateNonStaticWeaponsForRotationAroundPlayer: (PlayerItem*)player :(NSMutableArray *) nonStaticWeapons {
	
	if (player.numNonStaticCollectables > 0 && player.selectedWeapon == nonStaticWeapon) {
        WeaponItem *nonStaticWeapon = [nonStaticWeapons objectAtIndex:player.numNonStaticCollectables-1];
        nonStaticWeapon.active = NO;
        player.numNonStaticCollectables --;
    }
    [self resetNonStaticWeaponsForRotationAroundPlayer: player :nonStaticWeapons];
}

-(void) resetFlyingWeaponsForRotationAroundPlayer: (PlayerItem*)player :(NSMutableArray *) flyingWeapons {
	
    CGPoint firstFlyingWeaponPosition = ccp(player.position.x+WEAPON_MAGNITUDE_FROM_PLAYER, player.position.y);
    CGPoint newRotatedPoint;
    CGFloat weaponAngleToPlayerRadians;
    
    for (int i=0; i<flyingWeapons.count; i++) {
        WeaponItem *flyingWeapon = [flyingWeapons objectAtIndex:i];
        if (flyingWeapon.active) {
            CGFloat degreesOfSeperation = 360.0/(CGFloat)player.numFlyingCollectables;
            newRotatedPoint = ccpRotateByAngle(firstFlyingWeaponPosition, player.position, CC_DEGREES_TO_RADIANS(degreesOfSeperation*i));
            weaponAngleToPlayerRadians = ccpToAngle(ccpSub(newRotatedPoint, player.position));
            flyingWeapon.position = ccp(player.position.x+WEAPON_MAGNITUDE_FROM_PLAYER*cos(weaponAngleToPlayerRadians), 
                                        player.position.y+WEAPON_MAGNITUDE_FROM_PLAYER*sin(weaponAngleToPlayerRadians));
        }
        else if(!flyingWeapon.active) {
            flyingWeapon.position = WEAPON_ITEM_RESET_POSITION;
        }
    }
}

-(void) deactivateFlyingWeaponsForRotationAroundPlayer: (PlayerItem*)player :(NSMutableArray *) flyingWeapons {
	
	if (player.numFlyingCollectables > 0 && player.selectedWeapon == flyingWeapon) {
        WeaponItem *flyingWeapon = [flyingWeapons objectAtIndex:player.numFlyingCollectables-1];
        flyingWeapon.active = NO;
        player.numFlyingCollectables --;
    }
    [self resetFlyingWeaponsForRotationAroundPlayer: player:flyingWeapons];
}

-(void) activateNonStaticCollectedWeapons: (PlayerItem*)player :(NSMutableArray *) nonStaticCollectedWeapons: (CGPoint) clickedLocation {
	
	if (player.numNonStaticCollectables > 0 && player.selectedWeapon == nonStaticWeapon) {
		CollectedWeaponItem *nonStaticCollectedWeapon = [nonStaticCollectedWeapons objectAtIndex: player.numNonStaticCollectables -1];
		
		[nonStaticCollectedWeapon activateCollectedWeapon: clickedLocation];
	}
}

-(void) activateFlyingCollectedWeapons: (PlayerItem*)player :(NSMutableArray *) flyingCollectedWeapons: (CGPoint) clickedLocation {
	
	if (player.numFlyingCollectables > 0 && player.selectedWeapon == flyingWeapon) {
		CollectedWeaponItem *flyingCollectedWeapon = [flyingCollectedWeapons objectAtIndex: player.numFlyingCollectables -1];
		
		[flyingCollectedWeapon activateCollectedWeapon: clickedLocation];
	}
}

-(void) updateVacantGrids: (NSMutableArray *) chasingEnemiesPlacementGrid: (NSMutableArray *) chasingEnemies {
	
	for (int j=0; j<chasingEnemiesPlacementGrid.count; j++) {
		GridItem *tempChasingEnemyPlacementGrid = [chasingEnemiesPlacementGrid objectAtIndex:j];
		BOOL enemyInGrid = NO;
		
		//CCLOG(@"Grid origin:(%f, %f)", tempChasingEnemyPlacementGrid.gridRect.origin.x, tempChasingEnemyPlacementGrid.gridRect.origin.y);
		
		for (int i=0; i<chasingEnemies.count; i++) {
			ChaserItem *chasingEnemy = [chasingEnemies objectAtIndex:i];
			CGRect gridRectan = tempChasingEnemyPlacementGrid.gridRect;
			CGRect chasingEnemyRect = CGRectMake(chasingEnemy.position.x - chasingEnemy.contentSize.width*0.5, 
                                                 chasingEnemy.position.y - chasingEnemy.contentSize.height*0.5, 
                                                 chasingEnemy.contentSize.width, chasingEnemy.contentSize.height);
			if (CGRectIntersectsRect(gridRectan, chasingEnemyRect) && chasingEnemy.active) {
				enemyInGrid = YES;
				break;
			}
		}
		
		if (enemyInGrid) {
			tempChasingEnemyPlacementGrid.isFree = NO;
			//			CCLOG(@"Grid(%i) occupied", i);
		}
		else {
			tempChasingEnemyPlacementGrid.isFree = YES;
			//			CCLOG(@"Grid(%i) free", i);
		}
	}
}

-(void) placeChasingEnemiesInTheGrid: (NSMutableArray *) chasingEnemiesPlacementGrid: (NSMutableArray *) chasingEnemies {
	[self updateVacantGrids:chasingEnemiesPlacementGrid :chasingEnemies];
    
	int i = arc4random() % chasingEnemiesPlacementGrid.count;
	GridItem *chasingEnemyPlacementGrid = [chasingEnemiesPlacementGrid objectAtIndex:i];
	
	if (chasingEnemyPlacementGrid.isFree) {
		int j = arc4random() % chasingEnemies.count;					
		ChaserItem *chasingEnemy = [chasingEnemies objectAtIndex:j];
		if (!chasingEnemy.active) {
			chasingEnemy.position = ccp(chasingEnemyPlacementGrid.gridRect.origin.x+chasingEnemyPlacementGrid.gridRect.size.width*0.5,
										chasingEnemyPlacementGrid.gridRect.origin.y+chasingEnemyPlacementGrid.gridRect.size.height*0.5);
			chasingEnemy.active = YES;
			chasingEnemy.direction = YES;
			chasingEnemyPlacementGrid.isFree = NO;
            //			CCLOG(@"enemy placed: (%f, %f)", chasingEnemy.position.x, chasingEnemy.position.y);
		}		
	}
}

-(void) showSelectedWeapons: (PlayerItem*)player :(NSMutableArray *) nonStaticWeapons: (NSMutableArray *) flyingWeapons {
	
	if (player.selectedWeapon == nonStaticWeapon) {
		for (int i=0; i<nonStaticWeapons.count; i++) {
			WeaponItem *weapon = [nonStaticWeapons objectAtIndex:i];
			weapon.visible = YES;
		}
		
		for (int i=0; i<flyingWeapons.count; i++) {
			WeaponItem *weapon = [flyingWeapons objectAtIndex:i];
			weapon.visible = NO;
		}
	}
	else if(player.selectedWeapon == flyingWeapon) {
		for (int i=0; i<nonStaticWeapons.count; i++) {
			WeaponItem *weapon = [nonStaticWeapons objectAtIndex:i];
			weapon.visible = NO;
		}
		
		for (int i=0; i<flyingWeapons.count; i++) {
			WeaponItem *weapon = [flyingWeapons objectAtIndex:i];
			weapon.visible = YES;
		}
	}
}

-(void) checkPlayerChaserItemGridCollisionShortGamePlay: (PlayerItem*)player :(NSMutableArray*) currentChaserItemGridRects{
    
    CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
                                   player.position.y - player.contentSize.height*0.5, 
                                   player.contentSize.width, player.contentSize.height);
    
    CGPoint playerTopHead = ccp(player.position.x, player.position.y+player.contentSize.height*0.5);
	CGPoint playerBottomHead = ccp(player.position.x, player.position.y-player.contentSize.height*0.5);
	CGPoint playerRightHead = ccp(player.position.x+player.contentSize.width*0.5, player.position.y);
	CGPoint playerLeftHead = ccp(player.position.x-player.contentSize.width*0.5, player.position.y);   
    
    for (int i=0; i<[currentChaserItemGridRects count]; i++) {
        CGRect currentGrid = [[currentChaserItemGridRects objectAtIndex:i] CGRectValue];
        
        CGPoint blockTopHead = ccp(currentGrid.origin.x+currentGrid.size.width*0.5, currentGrid.origin.y+currentGrid.size.height);
        CGPoint blockBottomHead = ccp(currentGrid.origin.x+currentGrid.size.width*0.5, currentGrid.origin.y);
        CGPoint blockRightHead = ccp(currentGrid.origin.x+currentGrid.size.width, currentGrid.origin.y+currentGrid.size.height*0.5);
        CGPoint blockLeftHead = ccp(currentGrid.origin.x, currentGrid.origin.y+currentGrid.size.height*0.5);
        
        if (CGRectIntersectsRect(currentGrid, playerRect)) {
            //CCLOG(@"Collision");
            //Bottom contact of the block and player
            if (playerTopHead.y >= blockBottomHead.y && player.velocity.y > 0 && player.velocity.x == 0) {
				player.position = ccp(player.position.x, currentGrid.origin.y-player.contentSize.height*0.5);
                player.currentState = notMoving;
			}
			//Top contact of the block and player
			else if (playerBottomHead.y <= blockTopHead.y && player.velocity.y < 0 && player.velocity.x == 0) {
				player.position = ccp(player.position.x, currentGrid.origin.y+currentGrid.size.height+player.contentSize.height*0.5);
                player.currentState = notMoving;
			}
			//Left contact of the block and player
			else if(playerRightHead.x >= blockLeftHead.x && player.velocity.x > 0 && player.velocity.y == 0) {
				player.position = ccp(currentGrid.origin.x-player.contentSize.width*0.5, player.position.y);
                player.currentState = notMoving;
			}
			//Right contact of the block and player
			else if(playerLeftHead.x <= blockRightHead.x && player.velocity.x < 0 && player.velocity.y == 0) {
				player.position = ccp(currentGrid.origin.x+currentGrid.size.width+player.contentSize.width*0.5, player.position.y);
                player.currentState = notMoving;
			}
        }
        
    }
}

-(void) updateScore: (PlayerItem*)player :(GlobalSingletonVars*) sharedGlobalVarsInstance {
    
    sharedGlobalVarsInstance.score += (CGFloat)(player.velocity.y) / (CGFloat)(SCORE_PLAYER_VELOCITY_FACTOR);
}

-(void) updateFuelItemsShortGameplay: (GlobalSingletonVars*) sharedGlobalVarsInstance {
    sharedGlobalVarsInstance.fuelItemsTaken --;
}

-(void) updateBarLength: (PlayerItem*)player :(GlobalSingletonVars*) sharedGlobalVarsInstance {
    player.barLength = screenSize.width*
    ((CGFloat)sharedGlobalVarsInstance.fuelItemsTaken/(CGFloat)sharedGlobalVarsInstance.maxFuelItemsThatCanBeTaken);
}

-(NSString*) displayScore:(GlobalSingletonVars*) sharedGlobalVarsInstance {
    
	NSString *scoreLabelText = @"score: ";
	NSString *s1 = [NSString stringWithFormat:@"%i", (int)sharedGlobalVarsInstance.score];
	scoreLabelText = [scoreLabelText stringByAppendingString:s1];
	scoreLabelText = [scoreLabelText stringByAppendingString: @"   "];
	NSString *s2 = [NSString stringWithFormat:@"%i", (int)sharedGlobalVarsInstance.maxFuelItemsThatCanBeTaken];
	scoreLabelText = [scoreLabelText stringByAppendingString:s2];
	scoreLabelText = [scoreLabelText stringByAppendingString: @"   "];
	NSString *s3 = [NSString stringWithFormat:@"%i", (int)sharedGlobalVarsInstance.fuelItemsTaken];
	scoreLabelText = [scoreLabelText stringByAppendingString:s3];
	return scoreLabelText;
}

@end
