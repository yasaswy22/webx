//
//  FuelItem.h
//  WebX
//
//  Created by Sunny's Mac on 1/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface FuelItem : CCSprite {

@private
	CGPoint velocity;
	BOOL active;
}

@property CGPoint velocity;
@property BOOL active;

-(void) update;
-(void) updateFuelTankPosition: (CGPoint) targetVelocity;
-(void) resetFuelTankPosition;
-(void) makeFuelItemInactive;

@end
