//
//  GlobalSingletonVars.m
//  WebX
//
//  Created by Sunny's Mac on 3/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GlobalSingletonVars.h"
#import "GlobalVars.h"
//#import "SynthesizeSingleton.h"

@implementation GlobalSingletonVars

@synthesize score;
@synthesize fuelItemsTaken;
@synthesize maxFuelItemsThatCanBeTaken;
@synthesize justPlayed;

//SYNTHESIZE_SINGLETON_FOR_CLASS(GlobalSingletonVars);

+ (GlobalSingletonVars *)sharedGlobalSingletonVars {
    
    CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
    
    static  GlobalSingletonVars* sharedGlobalSingletonVars;
    
    @synchronized(self) {
        if (!sharedGlobalSingletonVars) {
            sharedGlobalSingletonVars = [[GlobalSingletonVars alloc] init];
        }
    }
    
    return sharedGlobalSingletonVars;
}

-(void) dealloc {
    CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
    [super dealloc];
}

-(void) resetVars {
    score = 0;
    fuelItemsTaken = 0;
    maxFuelItemsThatCanBeTaken = MAX_FUEL_ITEMS_INITIALLY;
    justPlayed = NO;
}

@end

//Once your class is a singleton, you can access the instance of it using the line:
//[GlobalSingletonVars sharedGlobalSingletonVars];