//
//  GameCenterTable.m
//  WebX
//
//  Created by Sunny's Mac on 7/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GameCenterTable.h"
#import "GameCenterFacebookTable.h"


@implementation GameCenterTable
@dynamic gameCenterScore;
@dynamic gameCenterID;
@dynamic gameCenterEmail;
@dynamic gameCenterAlias;
@dynamic facebookGCRelation;

+(GameCenterTable *) entryWithGameCenterAlias:(NSString *)gameCenterAlias entryWithGameCenterID:(NSString *)gameCenterID inManagedObjectContext:(NSManagedObjectContext *)context{
    
    GameCenterTable *gameCenterEntry = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"GameCenterTable" inManagedObjectContext:context]];
    [request setPredicate:[NSPredicate predicateWithFormat:@"gameCenterID = %@", gameCenterID]];
    NSError *error = nil;
    gameCenterEntry = [[context executeFetchRequest:request error:&error] lastObject];
    
    if (!error && !gameCenterEntry) {
        gameCenterEntry = [NSEntityDescription insertNewObjectForEntityForName:@"GameCenterTable" inManagedObjectContext:context];
        
        gameCenterEntry.gameCenterAlias = gameCenterAlias;
        gameCenterEntry.gameCenterID = gameCenterID;
    }
    
    // Commit the change.
    if (![context save:&error]) {
        // Handle the error.
        NSLog(@"Saving changes failed: %@", error);
        
    }
    
    return gameCenterEntry;
}

+(GameCenterTable *) updateGameCenterScore:(NSNumber *)gameCenterScore withGameCenterID:(NSString *)gameCenterID inManagedObjectContext:(NSManagedObjectContext *)context{
    
    GameCenterTable *gameCenterEntry = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"GameCenterTable" inManagedObjectContext:context]];
    [request setPredicate:[NSPredicate predicateWithFormat:@"gameCenterID = %@", gameCenterID]];
    NSError *error = nil;
    gameCenterEntry = [[context executeFetchRequest:request error:&error] lastObject];
    
    if (!error && gameCenterEntry) {
        [gameCenterEntry setValue:gameCenterScore forKey:@"gameCenterScore"];
    }
    
    // Commit the change.
    if (![context save:&error]) {
        // Handle the error.
        NSLog(@"Saving changes failed: %@", error);
    }
    
    return gameCenterEntry;
}

@end
