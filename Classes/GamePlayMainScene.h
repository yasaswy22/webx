//
//  GamePlayMainScene.h
//  WebX
//
//  Created by Sunny's Mac on 2/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerItem.h"
#import "GamePlayMethods.h"
#import "GlobalSingletonVars.h"

@interface GamePlayMainScene : CCLayer {

@private
	GamePlayMethods *gamePlayMethods;
	PlayerItem *player;
	CCMenuItemFont *pauseButton;
	NSMutableArray *fuelItems1;
	NSMutableArray *staticEnemies;
	NSMutableArray *chasingEnemies;
	NSMutableArray *nonStaticCollectables;
	NSMutableArray *nonStaticWeapons;
	NSMutableArray *nonStaticCollectedWeapons;
	NSMutableArray *flyingCollectables;
	NSMutableArray *flyingWeapons;
	NSMutableArray *flyingCollectedWeapons;
	NSMutableArray *rocks;
	NSMutableArray *rocksBackgroundLayer1;
	NSMutableArray *burningRocks;
	NSMutableArray *burningRocksBackgroundLayer1;
	NSMutableArray *chasingEnemiesPlacementGrid;
	BOOL playerTouched;
	BOOL screenTouched;
	BOOL playerBoost;
	CGFloat acceX;
	ccTime deltaTime;
	ccTime accumulatedTime;
	ccTime totalTime;
	CGPoint touchedLocation;
	CGPoint previousPlayerPosition;
	CCLabelTTF *scoreLabel;
    GlobalSingletonVars *sharedGlobalSingletonVarsInstance;
    NSMutableArray *flyingMountains1;
    NSMutableArray *flyingMountains2;
    NSMutableArray *ashes;
    
    BOOL retinaDisplay;
    CCAnimation *rotateAnim;
    CCAnimation *skeletonAnim;
}

+(id) scene;
-(void) returnMainMenu: (id) sender;
-(void) startShortGamePlay;
-(void) endGamePlay;

@end
