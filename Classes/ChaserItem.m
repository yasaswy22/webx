//
//  ChaserItem.m
//  WebX
//
//  Created by Sunny's Mac on 1/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ChaserItem.h"
#import "GlobalVars.h"
//#import "GameOverScene.h"

@implementation ChaserItem

@synthesize velocity;
@synthesize direction;
@synthesize maxAllowedHeightFromBottom;
@synthesize leftRight;
@synthesize vibratingTime;
@synthesize active;

-(void) update {
	self.position = ccpAdd(self.velocity, self.position);
}

-(void) updateChasingEnemyMovement: (CGPoint) playerVelocity {
		
	[self playerBoostChasingEnemiesMovement: playerVelocity];
		
	if(self.position.y > self.maxAllowedHeightFromBottom){
		self.direction = NO;
	}
	
	if (self.direction == NO && self.position.y < -screenSize.height*0.5){
		[self makeChaserEnemyInactive];
	}
		
	CGFloat plvel = playerVelocity.y;
	CGFloat mxvel = PLAYER_MAX_VELOCITY;
	CGFloat chasingEnemyVelocity = CHASING_ENEMY_VELOCITY*(1 - (plvel / mxvel)*0.5);
	
	if (self.active) {
	
		if (self.direction) {
			self.velocity = ccp(0, chasingEnemyVelocity);
		}
		else {
			self.velocity = ccp(0, -chasingEnemyVelocity*5);
		}
	}
	[self update];
}

-(void) moveChasingEnemiesTowardsPlayer: (PlayerItem *) player {
	
	CGRect playerInfluenceRect = CGRectMake(player.position.x - CHASING_ENEMY_INFLUENCE_CONSTANT*player.contentSize.width*0.5, 
											player.position.y - CHASING_ENEMY_INFLUENCE_CONSTANT*player.contentSize.height*0.5, 
											CHASING_ENEMY_INFLUENCE_CONSTANT*player.contentSize.width, 
											CHASING_ENEMY_INFLUENCE_CONSTANT*player.contentSize.height);
	
	if (CGRectContainsPoint(playerInfluenceRect, self.position)) {

		CGPoint enemyPositionToPlayer = ccp(self.position.x - player.position.x, self.position.y - player.position.y);
		CGFloat enemyAngleToPlayer = ccpToAngle(enemyPositionToPlayer);
		
		CGFloat plvel = player.velocity.y;
		CGFloat mxvel = PLAYER_MAX_VELOCITY;
		CGFloat chasingEnemyVelocity = CHASING_ENEMY_VELOCITY*(1 - (plvel / mxvel)*0.5);
		
		CGPoint enemyPlayerInfluenceVelocity = ccp(chasingEnemyVelocity*cos(enemyAngleToPlayer), 
												   chasingEnemyVelocity*sin(enemyAngleToPlayer));
		self.position = ccp(self.position.x - enemyPlayerInfluenceVelocity.x, self.position.y);
		
	}
}

-(void) chasingEnemyBackingMovement: (PlayerItem *) player:(ccTime) deltaTime {
	
	if (!CGPointEqualToPoint(velocity, CGPointZero)) {	
		if (player.playerBoost) {
			if (self.maxAllowedHeightFromBottom > MIN_CHASING_ENEMY_VERTICAL_MOVE_WHEN_BOOST_HEIGHT) {
				self.position = ccp(self.position.x, self.position.y+(CHASING_ENEMY_VER_BOOST_DECELERATION*deltaTime));
			}
			else {
				player.playerBoost = NO;
			}
		}
	}
}

-(void) vibrateChasingEnemies: (ccTime) deltaTime {
	
	if (vibratingTime < 3*deltaTime) {
		vibratingTime += deltaTime;
	}
	else {
		if (self.leftRight) {
			self.leftRight = NO;
			self.position = ccp(self.position.x - self.contentSize.width*0.05, self.position.y);
		}
		else {
			self.leftRight = YES;
			self.position = ccp(self.position.x + self.contentSize.width*0.05, self.position.y);
		}
		
		vibratingTime = 0;
	}
}

-(void) playerBoostChasingEnemiesMovement: (CGPoint) playerVelocity {
	
	CGFloat chasingEnemyMovementDistance = MAX_CHASING_ENEMY_VERTICAL_MOVE_WHEN_BOOST_HEIGHT - 
	MIN_CHASING_ENEMY_VERTICAL_MOVE_WHEN_BOOST_HEIGHT;
	
	CGFloat plvel = playerVelocity.y;
	CGFloat mxvel = PLAYER_MAX_VELOCITY;
	
	// player can reach upto 0 speed before getting caught to chasing enemies
	self.maxAllowedHeightFromBottom = MIN_CHASING_ENEMY_VERTICAL_MOVE_WHEN_BOOST_HEIGHT +
	chasingEnemyMovementDistance *(1 - (plvel / mxvel));

//	// player can reach upto 10% of max speed before getting caught to chasing enemies
//	if (1.5*(1 - (plvel / mxvel)) <=1) {
//		self.maxAllowedHeightFromBottom = MIN_CHASING_ENEMY_VERTICAL_MOVE_WHEN_BOOST_HEIGHT +
//		chasingEnemyMovementDistance *(1.1*(1 - (plvel / mxvel)));
//	}
}

-(void) makeChaserEnemyInactive {

	self.position = ccp(0, -screenSize.height*0.5);
	self.active = NO;
	
	int i = arc4random() % 2;
	if (i == 0) {
		self.leftRight = NO;
	}
	else if(i == 1) {
		self.leftRight = YES;
	}
}

//-(void) checkEndGame: (PlayerItem *) player {
//	
//	CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
//								   player.position.y - player.contentSize.height*0.5, 
//								   player.contentSize.width, player.contentSize.height);
//	
//	CGRect enemyRect = CGRectMake(self.position.x - self.contentSize.width*0.5, 
//								  self.position.y - self.contentSize.height*0.5, 
//								  self.contentSize.width, self.contentSize.height);
//	
//	if (CGRectIntersectsRect(playerRect, enemyRect) || player.killed) {
//		[[CCDirector sharedDirector] replaceScene: [GameOverScene scene]];
//	}
//}

@end