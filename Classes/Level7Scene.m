//
//  Level7Scene.m
//  WebX
//
//  Created by Sunny's Mac on 1/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Level7Scene.h"
#import "MainMenuScene.h"
#import "GlobalVars.h"
#import "BlockItem.h"

@implementation Level7Scene

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* level7Scene = [CCScene node];
	Level7Scene* level7Layer = [Level7Scene node];
	[level7Scene addChild:level7Layer];
	return level7Scene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		screenSize = [[CCDirector sharedDirector] winSize];
		//		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		self.isAccelerometerEnabled=YES;
		
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:15];
		
		CCMenuItemFont *exitButton = [CCMenuItemFont itemFromString:@"exit" target:self selector:@selector(returnMainMenu:)];
		exitButton.color = ccc3(TEXT_COLOR);
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5, 
								  screenSize.height - exitButton.contentSize.height * 0.5);
		
		pauseButton = [CCMenuItemFont itemFromString:@"  pause" target:self selector:@selector(pauseGame:)];
		pauseButton.color = ccc3(TEXT_COLOR);
		pauseButton.position = ccp(screenSize.width - pauseButton.contentSize.width * 0.5, 
								   pauseButton.contentSize.height * 0.5);
		
		CCMenu* menu = [CCMenu menuWithItems:exitButton, pauseButton, nil];
		menu.position = CGPointZero;
		[self addChild:menu z:4];
		
		player = [PlayerItem spriteWithFile:@"Volleyball_Ball.png"];
		player.position = ccp(screenSize.width*0.5, MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT);
		player.velocity = CGPointZero;
		player.acceleration = CGPointZero;
		player.decelaration = CGPointZero;
		player.velocity = ccp(0, PLAYER_INITIAL_VELOCITY);
		player.acceleration = ccp(0, PLAYER_INITIAL_ACCELERATION);
		[self addChild:player z:2];
		
		fuelTank = [FuelItem spriteWithFile:@"fuel_tank.png"];
		int i = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
		int j = arc4random() % (int)(screenSize.height + screenSize.height*0.5);
		fuelTank.position = ccp(i, j);
		[self addChild:fuelTank z:2];
		
		blocks = [[NSMutableArray alloc] init];
		for (int i=0; i<NUMBER_OF_BARS; i++) {
			
			NSString *fileName = @"bar";
			NSString *s1 = [NSString stringWithFormat:@"%i", i+1];
			fileName = [fileName stringByAppendingString:s1];
			fileName = [fileName stringByAppendingString:@".png"];
			
			BlockItem *block = [BlockItem spriteWithFile:fileName];
			block.position = ccp(screenSize.width*2.0, block.contentSize.height*0.5);
			block.active = NO;
			if (i+1 ==3 || i+1 == 6) {
				block.willKill = YES;
			}
			else {
				block.willKill = NO;
			}
			
			[blocks addObject:block];
			[self addChild:block z:2];
		}
		
		[self addNewBlock];
		
		[self scheduleUpdate];
		screenTouched = NO;
	}
	return self;
}

-(void) update:(ccTime)delta {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	[self powerUpForces];
	[self playerBoostMovement];
	[self frictionalForces];
	[self updateFuelTankPosition];
	[self updatePlayerPosition];
	[self updateBlocksPosition];
	[self maintainPlayerAtMinHeight]; // should be before player block collision to bring back to min height
	[self playerMovementPlayerBlockCollision];
	[self checkPlayerTakesFuel];
	
	
	deltaTime = delta;
	
	[player update];
	
	if(!CGPointEqualToPoint(player.velocity, CGPointZero)) {
		CGPoint pos = player.position;
		pos.x += lor*2 ;
		player.position = pos;
	}
}

-(void) powerUpForces {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	if (player.fuel > 0) {
		// Acceleration due to player pressing the screen..
		player.acceleration = ccp(player.acceleration.x, player.acceleration.y + TOUCH_ACCELERATION);
	}
}

-(void) playerBoostMovement {
	
	if (!CGPointEqualToPoint(player.velocity, CGPointZero)) {	
		if (playerBoost) {
			if (player.position.y < MAX_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT) {
				player.position = ccp(player.position.x, player.position.y+(TOUCH_PLAYER_VER_BOOST_ACCELERATION*deltaTime));
			}
			else if(player.position.y >= MAX_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT){
				playerBoost = NO;
			}
		}
		else if(!playerBoost) {
			if (player.position.y > MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT) {
				player.position = ccp(player.position.x, player.position.y-(TOUCH_PLAYER_VER_BOOST_ACCELERATION*0.25*deltaTime));
			}
		}
	}
}

-(void) frictionalForces {
	if (!CGPointEqualToPoint(player.velocity, CGPointZero)) {
		player.decelaration = ccp(player.decelaration.x, player.decelaration.y+DYNAMIC_FRICTIONAL_DECELERATION);
	}
}

-(void) updateFuelTankPosition {
	
	fuelTank.velocity = ccp(-player.velocity.x, -player.velocity.y);
	
	if (fuelTank.position.y < -fuelTank.contentSize.height*0.5 || 
		fuelTank.position.x < -fuelTank.contentSize.width*0.5 ||
		fuelTank.position.x > screenSize.width+fuelTank.contentSize.width*0.5) {
		
		int j = (int)(screenSize.width*0.1) + arc4random() % (int)(screenSize.width*0.8);
		int k = (int)screenSize.height + arc4random() % (int)(screenSize.height*0.5);
		
		fuelTank.position = ccp(j, k);
		fuelTank.visible = YES;
	}
	[fuelTank update];
}

-(void) updateBlocksPosition {
	
	for (int i =0; i<blocks.count; i++) {
		BlockItem *block = [blocks objectAtIndex:i];
		
		if (block.active) {
			if (block.position.y < -block.contentSize.height*0.5) {
				block.active = NO;
				block.position = ccp(screenSize.width*2.0, block.contentSize.height*0.5);
				[self addNewBlock];
			}
			
			block.velocity = ccp(block.velocity.x, -player.velocity.y);
			[block update];
		}
	}
}

-(void) addNewBlock {
	
	int randomBlock = arc4random() %  NUMBER_OF_BARS;
	BlockItem *block = [blocks objectAtIndex:randomBlock];
	
	if (!block.active) {	
		int side = arc4random() % 2;
		
		if (side == 0) {
			block.position = ccp(block.contentSize.width*0.5, screenSize.height+block.contentSize.height*0.5);
		}
		else if(side == 1) {
			block.position = ccp(screenSize.width-block.contentSize.width*0.5, screenSize.height+block.contentSize.height*0.5);
		}
		block.active = YES;
	}
}

-(void) maintainPlayerAtMinHeight {
	if (player.position.y < MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT && 
		!CGPointEqualToPoint(player.velocity, CGPointZero)) {
		player.position = ccp(player.position.x, player.position.y+(TOUCH_PLAYER_VER_BOOST_ACCELERATION*0.25*deltaTime));
	}
}

-(void) playerMovementPlayerBlockCollision {
	
	for (int i =0; i<blocks.count; i++) {
		BlockItem *block = [blocks objectAtIndex:i];
		
		CGRect blockRect = CGRectMake(block.position.x - block.contentSize.width*0.5, 
									  block.position.y - block.contentSize.height*0.5, 
									  block.contentSize.width, block.contentSize.height);
		
		CGPoint playerTopHead = ccp(player.position.x, player.position.y+player.contentSize.height*0.5);
		CGPoint playerLeftHead = ccp(player.position.x-player.contentSize.width*0.5, player.position.y);
		CGPoint playerRightHead = ccp(player.position.x+player.contentSize.width*0.5, player.position.y);
		
		if (CGRectContainsPoint(blockRect, playerTopHead)) {
			player.position = ccp(player.position.x, blockRect.origin.y-player.contentSize.height*0.5);
			if (block.willKill) {
				player.velocity = CGPointZero;
				player.acceleration = CGPointZero;
				player.decelaration = CGPointZero;
			}
		}
		else if (CGRectContainsPoint(blockRect, playerLeftHead) && player.position.y > blockRect.origin.y) {
			player.position = ccp(blockRect.origin.x+blockRect.size.width+player.contentSize.width*0.5, player.position.y);
			if (block.willKill) {
				player.velocity = CGPointZero;
				player.acceleration = CGPointZero;
				player.decelaration = CGPointZero;
			}
		}
		else if (CGRectContainsPoint(blockRect, playerRightHead) && player.position.y > blockRect.origin.y) {
			player.position = ccp(blockRect.origin.x-player.contentSize.width*0.5, player.position.y);
			if (block.willKill) {
				player.velocity = CGPointZero;
				player.acceleration = CGPointZero;
				player.decelaration = CGPointZero;
			}
		}
	}
}

-(void) updatePlayerPosition {
	if (player.position.x < player.contentSize.width*0.5) {
		player.position = ccp(player.contentSize.width*0.5, player.position.y);
	}
	else if (player.position.x > screenSize.width-player.contentSize.width*0.5) {
		player.position = ccp(screenSize.width-player.contentSize.width*0.5, player.position.y);
	}
}

-(void) checkPlayerTakesFuel {
	
	CGRect playerRect = CGRectMake(player.position.x - player.contentSize.width*0.5, 
								   player.position.y - player.contentSize.height*0.5, 
								   player.contentSize.width, player.contentSize.height);
	
	CGRect fuelRect = CGRectMake(fuelTank.position.x - fuelTank.contentSize.width*0.5, 
								 fuelTank.position.y - fuelTank.contentSize.height*0.5, 
								 fuelTank.contentSize.width, fuelTank.contentSize.height);
	
	if(CGRectIntersectsRect(playerRect, fuelRect)) {
		
		player.fuel = PLAYER_MAX_FUEL;
		player.decelaration = CGPointZero;
		playerBoost = YES;
		fuelTank.visible = NO;
	}
}

-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	lor = 0;
	if(acceleration.x > ACCELEROMETER_MIN_VALUE)
		lor = ACCELEROMETER__VALUE;
	else if(acceleration.x < -ACCELEROMETER_MIN_VALUE)
		lor = -ACCELEROMETER__VALUE;
}

-(void) pauseGame: (id) sender {
	if ([[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] resume];
		[pauseButton setString:@"  pause"];
	}
	else if (![[CCDirector sharedDirector] isPaused]) {
		[[CCDirector sharedDirector] pause];
		[pauseButton setString:@"resume"];
	}
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[blocks release];
	[super dealloc];
}

@end