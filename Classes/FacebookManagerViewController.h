//
//  FacebookManagerViewController.h
//  WebX
//
//  Created by Sunny's Mac on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"

@interface FacebookManagerViewController : UIViewController <FBDialogDelegate, FBSessionDelegate>{
    Facebook *facebook;
    NSString *facebookAppId;
    UITabBarController *facebookTabBarManager;
}

@property (nonatomic, retain) UITabBarController *facebookTabBarManager;

-(void) showFacebookTabBar;
-(void) synchronizeFacebook;
-(void) authorizeFacebook;
- (IBAction)loginIntoFacebook:(id)sender;
- (IBAction)cancelLoginIntoFacebook:(id)sender;

-(UIViewController*) getRootViewController;
-(void) presentViewController:(UIViewController*)vc;
-(void) dismissModalViewController;

@end
