//
//  GlobalSingletonVars.h
//  WebX
//
//  Created by Sunny's Mac on 3/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface GlobalSingletonVars : NSObject {

@private
    CGFloat score;
    int fuelItemsTaken;
    int maxFuelItemsThatCanBeTaken;
    BOOL justPlayed;
}

@property (nonatomic, assign) CGFloat score;
@property (nonatomic, assign) int fuelItemsTaken;
@property (nonatomic, assign) int maxFuelItemsThatCanBeTaken;
@property (nonatomic, assign) BOOL justPlayed;

+ (GlobalSingletonVars *)sharedGlobalSingletonVars;
-(void) resetVars;
@end
