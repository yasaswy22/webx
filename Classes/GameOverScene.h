//
//  GameOverScene.h
//  WebX
//
//  Created by Sunny's Mac on 12/31/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GlobalSingletonVars.h"

@interface GameOverScene : CCLayer {
    GlobalSingletonVars *sharedGlobalSingletonVarsInstance;
}

+(id) scene;

@end
