//
//  LevelSelectScene.m
//  WebX
//
//  Created by Sunny's Mac on 1/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LevelSelectScene.h"
#import "GamePlayMainScene.h"
#import "MainMenuScene.h"
#import "GlobalVars.h"

#define NUMBER_OF_LEVELS_PER_ROW 6
#define LEVELS_POSITION_WIDTH screenSize.width/(NUMBER_OF_LEVELS_PER_ROW + 1)

@implementation LevelSelectScene

+(id) scene {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	CCScene* levelSelectScene = [CCScene node];
	LevelSelectScene* levelSelectLayer = [LevelSelectScene node];
	[levelSelectScene addChild:levelSelectLayer];
	return levelSelectScene;
}

-(id) init {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	if((self = [super init])) {
		screenSize = [[CCDirector sharedDirector] winSize];
		
		CCSprite *background = [CCSprite spriteWithFile:@"level_select.png"];
		background.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
		[self addChild:background];
		
		CCLabelTTF *title = [CCLabelTTF labelWithString:@"Level Select" fontName:@"Marker Felt" fontSize:32];
		title.position =  ccp(screenSize.width * 0.5, screenSize.height*0.8 - title.contentSize.height * 0.5);
		title.color = ccc3(TEXT_COLOR);
		[self addChild: title];
	
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:20];
		
		CCMenu* menu = [CCMenu menuWithItems: nil];
		menu.position = CGPointZero;
		[self addChild:menu];
		
		levels = [[NSMutableArray alloc]init];
		
//		for (int i=0; i<NUMBER_OF_LEVELS_PER_ROW; i++) {
//			
//			NSString *methodName = @"startLevel";
//			NSString *s1 = [NSString stringWithFormat:@"%i", i+1];
//			methodName = [methodName stringByAppendingString:s1];
//			methodName = [methodName stringByAppendingString:@":"];
//			
//			NSString *itemName = [NSString stringWithFormat:@"%i", i+1];
//			
//			CCLOG(@"Method name: %@", methodName);
//			CCLOG(@"itemFromString: %@", itemName);
//			CCMenuItemFont *level = [CCMenuItemFont itemFromString:itemName
//															target:self 
//														  selector:@selector(methodName)];
//			level.color = ccc3(TEXT_COLOR);
//			level.position = ccp(LEVELS_POSITION_WIDTH*i, screenSize.height*0.5);
//			[levels addObject:level];
//			[menu addItems:level];
//		}

		CCMenuItemFont *gamePlayMain = [CCMenuItemFont itemFromString:@"Sart Game" target:self selector:@selector(startGamePlayMain:)];
		gamePlayMain.color = ccc3(TEXT_COLOR);
		gamePlayMain.position = ccp(screenSize.width*0.5, screenSize.height*0.5);
		[menu addChild:gamePlayMain];
		
		CCMenuItemFont *exitButton = [CCMenuItemFont itemFromString:@"exit" target:self selector:@selector(returnMainMenu:)];
		exitButton.color = ccc3(TEXT_COLOR);
		exitButton.position = ccp(screenSize.width - exitButton.contentSize.width * 0.5, 
								  screenSize.height - exitButton.contentSize.height * 0.5);
		[menu addChild:exitButton];
	}
	return self;
}

-(void) startGamePlayMain: (id) sender{
	[[CCDirector sharedDirector]replaceScene: [GamePlayMainScene scene]];
}

-(void) returnMainMenu: (id) sender {
	[[CCDirector sharedDirector] replaceScene: [MainMenuScene scene]];
}

-(void) dealloc {
	CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	[levels release];
	[super dealloc];
}

@end