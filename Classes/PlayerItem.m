//
//  PlayerItem.m
//  WebProject
//
//  Created by Sunny's Mac on 12/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PlayerItem.h"
#import "GlobalVars.h"
#import "WeaponItem.h"
#import "CollectedWeaponItem.h"
#import "BlockItem.h"
#import "ChaserItem.h"
#import "GridItem.h"

@implementation PlayerItem

@synthesize fuel;
@synthesize acceleration;
@synthesize decelaration;
@synthesize velocity;
@synthesize collisionForce;
@synthesize collisionFrictionalForce;
@synthesize currentState;
@synthesize playerBoost;
@synthesize playerBoostTime;
@synthesize numNonStaticCollectables;
@synthesize numFlyingCollectables;
@synthesize selectedWeapon;
@synthesize killed;
@synthesize barLength;

-(void) updateDuringShortGamePlay {
    self.position = ccp(self.position.x+velocity.x, self.position.y+velocity.y);
    //CCLOG(@"velocity: (%f, %f)", velocity.x, velocity.y);
}

-(void) updateDuringMainGamePlay {
	
	// Handling vertical Velocity
	if(velocity.y > PLAYER_MAX_VELOCITY) {
		velocity = ccp(velocity.x, PLAYER_MAX_VELOCITY);
		acceleration = CGPointZero;
		fuel = 0;
	}
	else if(velocity.y >= 0 && velocity.y <= PLAYER_MAX_VELOCITY) {
		velocity = ccp(velocity.x+acceleration.x-decelaration.x, velocity.y+acceleration.y-decelaration.y);
	}
	else if(velocity.y < 0) {
		velocity = CGPointZero;
		acceleration = CGPointZero;
		decelaration = CGPointZero;
	}
}

-(void) limitPlayerBoundaries {
	if (self.position.x < self.contentSize.width*0.5) {
		self.position = ccp(self.contentSize.width*0.5, self.position.y);
	}
	else if (self.position.x > screenSize.width-self.contentSize.width*0.5) {
		self.position = ccp(screenSize.width-self.contentSize.width*0.5, self.position.y);
	}
}

-(void) updatePosition {
	self.position = ccp(self.position.x+velocity.x, self.position.y+velocity.y);
}

-(void) playerBoostMovement: (ccTime) deltaTime {
	
	if (!CGPointEqualToPoint(velocity, CGPointZero)) {	
		if (self.playerBoost) {
			if (self.position.y < MAX_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT) {
				self.position = ccp(self.position.x, self.position.y+(TOUCH_PLAYER_VER_BOOST_ACCELERATION*deltaTime));
			}
			else if(self.position.y >= MAX_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT){
				self.playerBoost = NO;
			}
		}
		else if(!self.playerBoost) {
			if (self.position.y > MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT) {
				self.position = ccp(self.position.x, self.position.y-(TOUCH_PLAYER_VER_BOOST_ACCELERATION*0.25*deltaTime));
			}
		}
	}
}

-(void) powerUpForces {
	//CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
	
	if (fuel > 0) {
		// Acceleration due to player pressing the screen..
		acceleration = ccp(acceleration.x, acceleration.y + TOUCH_ACCELERATION);
	}
}

-(void) maintainPlayerAtMinHeight: (ccTime) deltaTime {
	if (self.position.y < MIN_PLAYER_VERTICAL_MOVE_WHEN_BOOST_HEIGHT && 
		!CGPointEqualToPoint(velocity, CGPointZero)) {
		self.position = ccp(self.position.x, self.position.y+(TOUCH_PLAYER_VER_BOOST_ACCELERATION*0.25*deltaTime));
	}
}

-(void) playerFrictionalForces {
	if (!CGPointEqualToPoint(velocity, CGPointZero)) {
		decelaration = ccp(decelaration.x, decelaration.y+DYNAMIC_FRICTIONAL_DECELERATION);
	}
}

-(void) playerFrictionalForcesEnemyCollisionMovement {
	if (!CGPointEqualToPoint(velocity, CGPointZero)) {
		
		CGFloat playerCollisionForceValue = sqrt(collisionForce.x*collisionForce.x +
												 collisionForce.y*collisionForce.y);
		CGFloat playerFrictionalForceValue = sqrt(collisionFrictionalForce.x*collisionFrictionalForce.x +
												  collisionFrictionalForce.y*collisionFrictionalForce.y);
		
		if (playerCollisionForceValue > playerFrictionalForceValue) {
			collisionForce = ccp(collisionForce.x - collisionFrictionalForce.x, 
								 collisionForce.y - collisionFrictionalForce.y);
			velocity = ccp(velocity.x + collisionForce.x, 
						   velocity.y + collisionForce.y);
		}
		else if(playerCollisionForceValue <= playerFrictionalForceValue){
			collisionForce = CGPointZero;
			collisionFrictionalForce = CGPointZero;
			velocity = ccp(0, velocity.y);
		}
	}
}

-(void) updateAccelerometerInput: (CGFloat) acceX {
	if(!CGPointEqualToPoint(velocity, CGPointZero)) {
		self.position = ccp(self.position.x+acceX*ACCELEROMETER__VALUE, self.position.y);
	}
}

-(BOOL) touchedPlayer: (CGPoint) touchedLocation {
	
	CGRect playerTouchRect = CGRectMake(self.position.x - 3*self.contentSize.width*0.5, 
								   self.position.y - 3*self.contentSize.height*0.5, 
								   3*self.contentSize.width, 3*self.contentSize.height);
	
	if (CGRectContainsPoint(playerTouchRect, touchedLocation)) {
		return YES;
	}
	else {
		return NO;
	}	
}

-(void) changePlayerSelectedWeapon {
	if (self.selectedWeapon == nonStaticWeapon && self.numFlyingCollectables > 0) {
		self.selectedWeapon = flyingWeapon;
	}
	else if(self.selectedWeapon == flyingWeapon && self.numNonStaticCollectables > 0) {
		self.selectedWeapon = nonStaticWeapon;
	}
}

-(void) changePlayerWeaponDefault {
	
	if(self.numNonStaticCollectables == 0 && self.numFlyingCollectables > 0 ) {
		self.selectedWeapon = flyingWeapon;
	}
	else if(self.numFlyingCollectables == 0 && self.numNonStaticCollectables > 0 ) {
		self.selectedWeapon = nonStaticWeapon;
	}
}

-(void) updateDirectionShortGamePlay {
	
	if(self.currentState == movedLeft) {
		self.velocity = ccp(-PLAYER_SHORT_GAMEPLAY_VELOCITY, 0);
	}
	else if(self.currentState == movedRight) {
		self.velocity = ccp(PLAYER_SHORT_GAMEPLAY_VELOCITY, 0);
	}
	else if(self.currentState == movedTop) {
		self.velocity = ccp(0, PLAYER_SHORT_GAMEPLAY_VELOCITY);
	}
	else if(self.currentState == movedBottom) {
		self.velocity = ccp(0, -PLAYER_SHORT_GAMEPLAY_VELOCITY);
	}
	else if(self.currentState == notMoving) {
		self.velocity = ccp(0, 0);
	}
}

@end
