//
//  GameCenterFacebookTable.h
//  WebX
//
//  Created by Sunny's Mac on 6/28/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FacebookTable, GameCenterTable;

@interface GameCenterFacebookTable : NSManagedObject {
@private
}
@property (nonatomic, retain) NSString * gameCenterEmail;
@property (nonatomic, retain) NSString * UUID;
@property (nonatomic, retain) FacebookTable * facebookID;
@property (nonatomic, retain) GameCenterTable * gameCenterID;

@end
