//
//  CollectableItem.h
//  WebX
//
//  Created by Sunny's Mac on 1/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CollectableItem : CCSprite {

@private
	CGPoint velocity;
	CGPoint acceleration;
	CGPoint collisionForce;
	CGPoint frictionalForce;
	BOOL nonStatic;
	BOOL flying;
}

@property CGPoint velocity;
@property CGPoint acceleration;
@property CGPoint collisionForce;
@property CGPoint frictionalForce;
@property BOOL nonStatic;
@property BOOL flying;

-(void) update;
-(void) updateCollectablesPosition: (CGPoint) targetVelocity;
-(void) resetCollectablesPosition;

@end
