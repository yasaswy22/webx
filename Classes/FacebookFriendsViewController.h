//
//  FacebookFriendsViewController.h
//  WebX
//
//  Created by Sunny's Mac on 7/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"

@interface FacebookFriendsViewController : UIViewController <FBDialogDelegate, FBSessionDelegate, FBRequestDelegate>{
    Facebook *facebook;
    NSString *facebookAppId;
    IBOutlet UITableView *freindsList;
    NSArray *fetchedFacebookEntries;
    
    UIActivityIndicatorView *spinner;
    int timerForSpinning;

    NSManagedObjectContext *managedObjectContext;
}

@property (nonatomic, retain) UITableView *freindsList;
@property (nonatomic, retain) NSArray *fetchedFacebookEntries;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

- (IBAction)FacebookViewFinished:(id)sender;
-(void) resetTime:(NSTimer *)t;
-(void) showingloadingActivity;
-(void) notShowingloadingActivity;

-(void) synchronizeFacebook;
-(void) authorizeFacebook;
-(void) getFriendsListDetails;
- (void) publishStreamFriends;
-(void) postOnFaceBookFriends;
-(void) loadFriendsFacebookDetailsFromCoreData;

-(UIViewController*) getRootViewController;
-(void) presentViewController:(UIViewController*)vc;
-(void) dismissModalViewController;

@end
